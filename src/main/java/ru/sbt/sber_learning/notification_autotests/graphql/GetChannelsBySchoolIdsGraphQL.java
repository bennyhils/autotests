package ru.sbt.sber_learning.notification_autotests.graphql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;
import ru.sbt.sber_learning.notification_autotests.GraphQLUtilMethods;
import ru.sbt.sber_learning.notification_autotests.UtilMethods;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
@SpringBootTest(classes = {GraphQLUtilMethods.class, UtilMethods.class})
public class GetChannelsBySchoolIdsGraphQL extends AbstractTestNGSpringContextTests {

    @Value("${autotests-users.sys.login}")
    private String adminSysLogin;

    @Value("${autotests-users.old-password}")
    private String adminSysPassword;

    private static final String SCHOOL_ID_1 = UUID.randomUUID().toString();
    private static final String SCHOOL_ID_2 = UUID.randomUUID().toString();
    private final String fileQuery = "/graphql/getChannelsBySchoolIds.graphql";
    private final Map<String, Boolean> channelNamesTrue = new HashMap<>();
    private final Map<String, Boolean> channelNamesFalse = new HashMap<>();

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private UtilMethods utilMethods;

    @Autowired
    private GraphQLUtilMethods graphQLUtilMethods;

    @BeforeClass
    public void beforeClass() {
        for (Long i = 1L; i <= 3L; i++) {
            utilMethods.fillTableOrganizationChannels(i, SCHOOL_ID_1, true);
            utilMethods.fillTableOrganizationChannels(i, SCHOOL_ID_2, false);
        }

        channelNamesTrue.put("UI", true);
        channelNamesTrue.put("EMAIL", true);
        channelNamesTrue.put("SMS", true);

        channelNamesFalse.put("UI", false);
        channelNamesFalse.put("EMAIL", false);
        channelNamesFalse.put("SMS", false);
    }

    @AfterClass
    public void afterClass() {
        for (Long i = 1L; i <= 3L; i++) {
            utilMethods.cleanTableOrganizationChannels(i, Collections.singleton(SCHOOL_ID_1));
            utilMethods.cleanTableOrganizationChannels(i, Collections.singleton(SCHOOL_ID_2));
        }
    }

    private JSONArray getJsonSuccessResponse(final CloseableHttpResponse responseGraphQL) throws IOException {
        final JSONObject response = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        return response
                .getJSONObject("data")
                .getJSONObject("notificationConfiguration")
                .getJSONArray("getChannelsBySchoolIds");
    }

    private JSONArray getJsonErrorResponse(final CloseableHttpResponse responseGraphQL) throws IOException {
        final JSONObject response = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        return response
                .getJSONArray("errors");
    }

    private Map<String, Boolean> getSchoolChannels(
            final Map<String, JSONObject> schoolsChannels,
            final String schoolId
    ) {
        final Map<String, Boolean> schoolChannels = new HashMap<>();
        schoolsChannels.get(schoolId).getJSONArray("channels").forEach(obj -> {
            final JSONObject jsonObj = (JSONObject) obj;
            schoolChannels.put(jsonObj.getString("channelName"), jsonObj.getBoolean("channelActive"));
        });

        return schoolChannels;
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetChannelsBySchoolIds")
    @Story(value = "Основная функциональность")
    @Test(description = "Проверка настроек 2 школ по всем каналам")
    @Owner(value = "KovalchukIS")
    public void getChannelsBySchoolIds() throws IOException {

        final ObjectNode variables = objectMapper.createObjectNode();
        variables.putArray("schoolIds").add(SCHOOL_ID_1).add(SCHOOL_ID_2);
        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        adminSysLogin,
                        adminSysPassword,
                        fileQuery,
                        variables,
                        null
                );

        final JSONArray channelsFromResponse = getJsonSuccessResponse(responseGraphQL);

        final Map<String, JSONObject> schoolsChannels = new HashMap<>();
        for (int i = 0; i < channelsFromResponse.length(); i++) {
            schoolsChannels.put(
                    channelsFromResponse.getJSONObject(i).getString("schoolId"),
                    channelsFromResponse.getJSONObject(i)
            );
        }

        final Map<String, Boolean> school_1_Channels = getSchoolChannels(schoolsChannels, SCHOOL_ID_1);
        final Map<String, Boolean> school_2_Channels = getSchoolChannels(schoolsChannels, SCHOOL_ID_2);

        Assert.assertEquals(school_1_Channels, channelNamesTrue);

        Assert.assertEquals(school_2_Channels, channelNamesFalse);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetChannelsBySchoolIds")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда schoolId = null")
    @Owner(value = "KovalchukIS")
    public void getChannelsWithNullSchoolId() throws IOException {

        final ObjectNode variables = objectMapper.createObjectNode();
        variables.putArray("schoolIds").addNull();
        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        adminSysLogin,
                        adminSysPassword,
                        fileQuery,
                        variables,
                        null
                );

        final String errorFromResponse = getJsonErrorResponse(responseGraphQL).toString();
        final String errorExpected = "Variable 'schoolIds' has coerced Null value for NonNull type";

        Assert.assertTrue(
                errorFromResponse.contains(errorExpected),
                "Содержит ли текст ошибки " + "\"" + errorExpected + "\"?"
        );
    }

    @Ignore(value = "Bug. EDU-14352")
    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetChannelsBySchoolIds")
    @Story(value = "Проверка некорректных значений")
    @Test(description = "Когда schoolId = \"incorrectValue\"")
    @Owner(value = "KovalchukIS")
    public void getChannelsWithIncorrectSchoolId() throws IOException {

        final ObjectNode variables = objectMapper.createObjectNode();
        variables.putArray("schoolIds").add("incorrectValue");
        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        adminSysLogin,
                        adminSysPassword,
                        fileQuery,
                        variables,
                        null
                );
        // TODO: возвращается ошибка 400. Bug?
    }

    @Ignore(value = "Bug. EDU-14352")
    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetChannelsBySchoolIds")
    @Story(value = "Проверка пустых значений")
    @Test(description = "Когда schoolId = \"\"")
    @Owner(value = "KovalchukIS")
    public void getChannelsWithEmptySchoolId() throws IOException {

        final ObjectNode variables = objectMapper.createObjectNode();
        variables.putArray("schoolIds").add("");
        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        adminSysLogin,
                        adminSysPassword,
                        fileQuery,
                        variables,
                        null
                );
        // TODO: возвращается ошибка 400. Bug?
    }
}