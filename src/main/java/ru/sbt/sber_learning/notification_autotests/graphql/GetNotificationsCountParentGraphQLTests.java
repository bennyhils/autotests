package ru.sbt.sber_learning.notification_autotests.graphql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.sbt.sber_learning.notification_autotests.GraphQLUtilMethods;
import ru.sbt.sber_learning.notification_autotests.UtilMethods;

import java.io.IOException;

@Component
@SpringBootTest(classes = {GraphQLUtilMethods.class, UtilMethods.class})
public class GetNotificationsCountParentGraphQLTests extends AbstractTestNGSpringContextTests {

    @Autowired
    private GraphQLUtilMethods graphQLUtilMethods;

    @Autowired
    private UtilMethods utilMethods;

    @Value("${autotests-users.student.login}")
    private String studentLogin;

    @Value("${autotests-users.parent.login}")
    private String parentLogin;

    @Value("${autotests-users.teacher.login}")
    private String teacherLogin;

    @Value("${autotests-users.business.login}")
    private String businessLogin;

    @Value("${autotests-users.password}")
    private String password;

    private static final int STATUS_OK = 200;
    private static final int BAD_REQUEST = 400;
    private static final String ANOTHER_STUDENT_ID = "a9b7f0dc-15ec-4b00-8d2a-15b31689a8a6";
    private static final String SCHOOL_ID = "00000000-0000-0000-0001-000000000001";
    private final String fileQuery = "/graphql/getNotificationsCountParent.graphql";
    private String STUDENT_ID;
    private String PARENT_ID;

    @BeforeClass
    private void setStudentAndParentIdAndFillDB() {
        STUDENT_ID = utilMethods.getSelectUserIdFromUsersByLogin(studentLogin);
        PARENT_ID = utilMethods.getSelectUserIdFromUsersByLogin(parentLogin);

        utilMethods.fillDBNotificationsMass(10, STUDENT_ID, SCHOOL_ID, false, false);
        utilMethods.fillDBNotificationsMass(5, STUDENT_ID, SCHOOL_ID, true, false);
    }

    @AfterClass
    private void cleanup() {
        utilMethods.cleanDBNotifications(STUDENT_ID, SCHOOL_ID);
        utilMethods.cleanDBNotifications(PARENT_ID, SCHOOL_ID);
    }

    private String getJsonSuccessResponse(final CloseableHttpResponse responseGraphQL) throws IOException {
        final JSONObject response = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        return response
                .getJSONObject("data")
                .getJSONObject("parent")
                .get("getNotificationsCount").toString();
    }

    private String getJsonErrorResponse(final CloseableHttpResponse responseGraphQL) throws IOException {
        final JSONObject response = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        return response
                .getJSONArray("errors")
                .getJSONObject(0)
                .getJSONObject("extensions")
                .getString("uiErrorCode");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsCountParent")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда wasReadIncluded = true. ")
    @Owner(value = "Anastasiya Krutova")
    public void getNotificationsCount_whenWasReadIncludedTrue_shouldReturnCountFromDB() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("wasReadIncluded", true);
        variables.put("childUserId", STUDENT_ID);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        parentLogin,
                        password,
                        fileQuery,
                        variables,
                        SCHOOL_ID
                );

        final String notificationsCount = getJsonSuccessResponse(responseGraphQL);
        final int countAllOfNotifications =
                utilMethods.selectCountNotifications(STUDENT_ID, SCHOOL_ID, false)
                + utilMethods.selectCountNotifications(STUDENT_ID, SCHOOL_ID, true);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(Integer.parseInt(notificationsCount), countAllOfNotifications);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsCountParent")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда wasReadIncluded = false. ")
    @Owner(value = "Anastasiya Krutova")
    public void getNotificationsCount_whenWasReadIncludedFalse_shouldReturnCountFromDB() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("wasReadIncluded", false);
        variables.put("childUserId", STUDENT_ID);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        parentLogin,
                        password,
                        fileQuery,
                        variables,
                        SCHOOL_ID
                );

        final String notificationsCount = getJsonSuccessResponse(responseGraphQL);
        final int countOfNotifications = utilMethods.selectCountNotifications(STUDENT_ID, SCHOOL_ID, false);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(Integer.parseInt(notificationsCount), countOfNotifications);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsCountParent")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда wasReadIncluded не передается, в БД присутствуют прочитанные и непрочитанные нотификации. ")
    @Owner(value = "Anastasiya Krutova")
    public void getNotificationsCount_shouldReturnCountFromDB_DefaultWasReadIsFalse() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("childUserId", STUDENT_ID);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        parentLogin,
                        password,
                        fileQuery,
                        variables,
                        SCHOOL_ID
                );

        final String notificationsCount = getJsonSuccessResponse(responseGraphQL);
        final int countOfNotifications = utilMethods.selectCountNotifications(STUDENT_ID, SCHOOL_ID, false);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(Integer.parseInt(notificationsCount), countOfNotifications);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsCountParent")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда в базе есть уведомления, в которых userId - отправитель нотификаций. ")
    @Owner(value = "Anastasiya Krutova")
    public void getNotificationsCount_shouldNotCountOwnNotifications() throws IOException {

        utilMethods.fillDataBaseWithUserFromId(2, PARENT_ID, STUDENT_ID, SCHOOL_ID, false, false);

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("childUserId", STUDENT_ID);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        parentLogin,
                        password,
                        fileQuery,
                        variables,
                        SCHOOL_ID
                );

        final String notificationsCount = getJsonSuccessResponse(responseGraphQL);
        final int countOfNotifications = utilMethods.selectCountNotifications(STUDENT_ID, SCHOOL_ID, false);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(Integer.parseInt(notificationsCount), countOfNotifications);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsCountParent")
    @Story(value = "Проверка некорректных значений")
    @Test(description = "Когда schoolId = школе, к которой данный userId не привязан. " +
                        "Должен вернуть Error - Security access denied.")
    @Owner(value = "Anastasiya Krutova")
    public void getNotificationsCount_shouldReturnSecurityAccessDenied_WhenSchoolAnother() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("childUserId", STUDENT_ID);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        parentLogin,
                        password,
                        fileQuery,
                        variables,
                        "312ffa69-d1a8-482e-825c-949114487d88"
                );

        final String errorType = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(errorType, "SECURITY_ACCESS_DENIED");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsCountParent")
    @Story(value = "Проверка некорректных значений")
    @Test(description = "Когда parentId не привязан к данному userId. Должен вернуть Error - Security access denied. ")
    @Owner(value = "Anastasiya Krutova")
    public void getNotificationsCount_shouldReturnSecurityAccessDenied_WhenUserIdAnother() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("childUserId", ANOTHER_STUDENT_ID);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        parentLogin,
                        password,
                        fileQuery,
                        variables,
                        SCHOOL_ID
                );

        final String errorType = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(errorType, "SECURITY_ACCESS_DENIED");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsCountParent")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда childUserID не передается. Должен вернуть Bad Request. ")
    @Owner(value = "Anastasiya Krutova")
    public void getNotificationsCount_shouldReturnSecurityAccessDenied_WhenChildUserIdNotTransmit() throws IOException {

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        parentLogin,
                        password,
                        fileQuery,
                        null,
                        null
                );

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), BAD_REQUEST);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsCountParent")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда childUserId = null. Должен вернуть Bad Request. ")
    @Owner(value = "Anastasiya Krutova")
    public void getNotificationsCount_shouldReturnSecurityAccessDenied_WhenChildUserIdNull() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.putNull("childUserId");

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        parentLogin,
                        password,
                        fileQuery,
                        null,
                        null
                );

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), BAD_REQUEST);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsCountParent")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда schoolId = null. Должен вернуть Error - Security access denied. ")
    @Owner(value = "Anastasiya Krutova")
    public void getNotificationsCount_shouldReturnSecurityAccessDenied_WhenSchoolNull() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("childUserId", STUDENT_ID);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        parentLogin,
                        password,
                        fileQuery,
                        variables,
                        null
                );

        final String errorType = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(errorType, "SECURITY_ACCESS_DENIED");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsCountParent")
    @Story(value = "Проверка некорректных значений")
    @Test(description = "Когда роль пользователя - teacher. Должен вернуть Error - Security access denied. ")
    @Owner(value = "Anastasiya Krutova")
    public void getNotificationsCount_shouldReturnSecurityAccessDenied_WhenTeacherAuth() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("childUserId", STUDENT_ID);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        teacherLogin,
                        password,
                        fileQuery,
                        variables,
                        SCHOOL_ID
                );

        final String errorType = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(errorType, "SECURITY_ACCESS_DENIED");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsCountParent")
    @Story(value = "Проверка некорректных значений")
    @Test(description = "Когда роль пользователя - businessAdmin. Должен вернуть Error - Security access denied. ")
    @Owner(value = "Anastasiya Krutova")
    public void getNotificationsCount_shouldReturnSecurityAccessDenied_WhenBusinessAdminAuth() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("childUserId", STUDENT_ID);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        businessLogin,
                        password,
                        fileQuery,
                        variables,
                        SCHOOL_ID
                );

        final String errorType = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(errorType, "SECURITY_ACCESS_DENIED");
    }
}