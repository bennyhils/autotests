package ru.sbt.sber_learning.notification_autotests.graphql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.sbt.sber_learning.notification_autotests.GraphQLUtilMethods;
import ru.sbt.sber_learning.notification_autotests.UtilMethods;

import java.io.IOException;

@Component
@SpringBootTest(classes = {GraphQLUtilMethods.class, UtilMethods.class})
public class GetNotificationsCountStudentGraphQLTests extends AbstractTestNGSpringContextTests {

    @Value("${autotests-users.student.login}")
    private String studentLogin;

    @Value("${autotests-users.password}")
    private String password;

    @Value("${autotests-users.teacher.login}")
    private String teacherLogin;

    @Autowired
    private GraphQLUtilMethods graphQLUtilMethods;

    @Autowired
    private UtilMethods utilMethods;

    private static final int STATUS_OK = 200;
    private static final String STUDENT_FROM_ID = "00000000-0000-0000-0001-300000000006";
    private static final String SCHOOL_ID = "00000000-0000-0000-0001-000000000001";
    private static String studentId;
    private static String teacherId;

    @BeforeClass
    private void setStudentId() {
        studentId = utilMethods.getSelectUserIdFromUsersByLogin(studentLogin);
        teacherId = utilMethods.getSelectUserIdFromUsersByLogin(teacherLogin);
    }

    @AfterMethod
    private void cleanup() {
        utilMethods.cleanDBNotifications(studentId, SCHOOL_ID);
        utilMethods.cleanDBNotifications(STUDENT_FROM_ID, SCHOOL_ID);
        utilMethods.cleanDBNotifications(teacherId, SCHOOL_ID);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsCountStudent")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда wasReadIncluded = true. ")
    @Owner(value = "Anastasiya Krutova")
    public void getNotificationsCount_whenWasReadIncludedTrue_shouldReturnCountFromDB() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("wasReadIncluded", true);

        utilMethods.fillDBNotificationsMass(10, studentId, SCHOOL_ID, false, false);
        utilMethods.fillDBNotificationsMass(5, studentId, SCHOOL_ID, true, false);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        "/graphql/getNotificationsCountStudent.gql",
                        variables,
                        SCHOOL_ID
                );

        final JSONObject json = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final String notificationsCount = json
                .getJSONObject("data")
                .getJSONObject("student")
                .get("getNotificationsCount").toString();
        final int countAllOfNotifications =
                utilMethods.selectCountNotifications(studentId, SCHOOL_ID, false)
                + utilMethods.selectCountNotifications(studentId, SCHOOL_ID, true);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(Integer.parseInt(notificationsCount), countAllOfNotifications);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsCountStudent")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда wasReadIncluded = false. ")
    @Owner(value = "Anastasiya Krutova")
    public void getNotificationsCount_whenWasReadIncludedFalse_shouldReturnCountFromDB() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("wasReadIncluded", false);

        utilMethods.fillDBNotificationsMass(10, studentId, SCHOOL_ID, false, false);
        utilMethods.fillDBNotificationsMass(5, studentId, SCHOOL_ID, true, false);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        "/graphql/getNotificationsCountStudent.gql",
                        variables,
                        SCHOOL_ID
                );

        final JSONObject json = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final String notificationsCount = json
                .getJSONObject("data")
                .getJSONObject("student")
                .get("getNotificationsCount").toString();
        final int countOfNotifications = utilMethods.selectCountNotifications(studentId, SCHOOL_ID, false);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(Integer.parseInt(notificationsCount), countOfNotifications);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsCountStudent")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда wasReadIncluded не передается и количество нотификаций = 0. ")
    @Owner(value = "Anastasiya Krutova")
    public void getNotificationsCount_shouldReturnCountFromDB() throws IOException {
        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        "/graphql/getNotificationsCountStudentWithoutParams.gql",
                        null,
                        SCHOOL_ID
                );

        final JSONObject json = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final String notificationsCount = json
                .getJSONObject("data")
                .getJSONObject("student")
                .get("getNotificationsCount").toString();

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(notificationsCount, "0");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsCountStudent")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда wasReadIncluded не передается, в БД присутствуют прочитанные и непрочитанные нотификации. ")
    @Owner(value = "Anastasiya Krutova")
    public void getNotificationsCount_shouldReturnCountFromDB_DefaultWasReadIsFalse() throws IOException {

        utilMethods.fillDBNotificationsMass(10, studentId, SCHOOL_ID, false, false);
        utilMethods.fillDBNotificationsMass(5, studentId, SCHOOL_ID, true, false);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        "/graphql/getNotificationsCountStudentWithoutParams.gql",
                        null,
                        SCHOOL_ID
                );

        final JSONObject json = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final String notificationsCount = json
                .getJSONObject("data")
                .getJSONObject("student")
                .get("getNotificationsCount").toString();
        final int countOfNotifications = utilMethods.selectCountNotifications(studentId, SCHOOL_ID, false);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(Integer.parseInt(notificationsCount), countOfNotifications);
    }


    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsCountStudent")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда в базе есть уведомления, в которых userId - отправитель нотификаций. ")
    @Owner(value = "Anastasiya Krutova")
    public void getNotificationsCount_shouldNotCountOwnNotifications() throws IOException {

        utilMethods.fillDataBaseWithUserFromId(10, STUDENT_FROM_ID, studentId, SCHOOL_ID, false, false);
        utilMethods.fillDBNotificationsMass(5, studentId, SCHOOL_ID, false, false);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        "/graphql/getNotificationsCountStudentWithoutParams.gql",
                        null,
                        SCHOOL_ID
                );

        final JSONObject json = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final String notificationsCount = json
                .getJSONObject("data")
                .getJSONObject("student")
                .get("getNotificationsCount").toString();
        final int countOfNotifications = utilMethods.selectCountNotifications(studentId, SCHOOL_ID, false);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(Integer.parseInt(notificationsCount), countOfNotifications);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsCountStudent")
    @Story(value = "Проверка некорректных значений")
    @Test(description = "Когда SchoolId = школе, к которой данный userId не привязан. Должен вернуть Error - Security access denied. ")
    @Owner(value = "Anastasiya Krutova")
    public void getNotificationsCount_shouldReturnSecurityAccessDenied_WhenSchoolAnother() throws IOException {

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        "/graphql/getNotificationsCountStudentWithoutParams.gql",
                        null,
                        "312ffa69-d1a8-482e-825c-949114487d88"
                );

        final JSONObject json = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final String errorType = json
                .getJSONArray("errors")
                .getJSONObject(0)
                .getJSONObject("extensions")
                .getString("uiErrorCode");

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(errorType, "SECURITY_ACCESS_DENIED");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsCountStudent")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда SchoolId = null. Должен вернуть Error - Security access denied. ")
    @Owner(value = "Anastasiya Krutova")
    public void getNotificationsCount_shouldReturnSecurityAccessDenied_WhenSchoolIsNull() throws IOException {

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        "/graphql/getNotificationsCountStudentWithoutParams.gql",
                        null,
                        null
                );

        final JSONObject json = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final String errorType = json
                .getJSONArray("errors")
                .getJSONObject(0)
                .getJSONObject("extensions")
                .getString("uiErrorCode");

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(errorType, "SECURITY_ACCESS_DENIED");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsCountStudent")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда wasReadIncluded = true и роль пользователя - teacher. ")
    @Owner(value = "Anastasiya Krutova")
    public void getNotificationsCount_whenWasReadIncludedTrue_shouldReturnCountFromDB_teacherAuth() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("wasReadIncluded", true);

        utilMethods.fillDBNotificationsMass(10, teacherId, SCHOOL_ID, false, false);
        utilMethods.fillDBNotificationsMass(5, teacherId, SCHOOL_ID, true, false);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        teacherLogin,
                        password,
                        "/graphql/getNotificationsCountStudent.gql",
                        variables,
                        SCHOOL_ID
                );

        final JSONObject json = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final String notificationsCount = json
                .getJSONObject("data")
                .getJSONObject("student")
                .get("getNotificationsCount").toString();
        final int countAllOfNotifications =
                utilMethods.selectCountNotifications(teacherId, SCHOOL_ID, false)
                + utilMethods.selectCountNotifications(teacherId, SCHOOL_ID, true);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(Integer.parseInt(notificationsCount), countAllOfNotifications);
    }
}


