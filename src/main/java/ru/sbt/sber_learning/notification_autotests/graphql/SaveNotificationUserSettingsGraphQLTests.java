package ru.sbt.sber_learning.notification_autotests.graphql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.qameta.allure.*;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import ru.sbt.sber_learning.notification_autotests.GraphQLUtilMethods;
import ru.sbt.sber_learning.notification_autotests.UtilMethods;

import java.io.IOException;

@Component
@SpringBootTest(classes = {GraphQLUtilMethods.class, UtilMethods.class})
public class SaveNotificationUserSettingsGraphQLTests extends AbstractTestNGSpringContextTests {

    @Value("${autotests-users.student.login}")
    private String studentLogin;

    @Value("${autotests-users.password}")
    private String password;

    private static final int STATUS_OK = 200;
    private static final String SCHOOL_ID = "00000000-0000-0000-0001-000000000001";
    final int typeId = 1;
    final int channelId = 1;
    final boolean channelActive = true;

    @Value("${notifications.graphqlURI}")
    private String GRAPHQL_URI;

    @Autowired
    private GraphQLUtilMethods graphQLUtilMethods;

    @Autowired
    private UtilMethods utilMethods;

    @AfterClass
    private void afterClass() {
        utilMethods.cleanDBUserActiveChannels(utilMethods.getSelectUserIdFromUsersByLogin(studentLogin));
    }

    private CloseableHttpResponse graphQLRequest(final String fileGraphQL, final ObjectNode variables) throws IOException{
        return graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        fileGraphQL,
                        variables,
                        SCHOOL_ID);
    }

    private JSONObject saveOneNotificationUserSettings(
            final int typeId,
            final int channelId,
            final boolean channelActive
    ) throws IOException {
        final ObjectNode variables = new ObjectMapper().createObjectNode();
        final ObjectNode settings = variables.putObject("settings");

        settings.put("typeId", typeId)
                .put("channelId", channelId)
                .put("channelActive", channelActive);

        final CloseableHttpResponse responseGraphQL = graphQLRequest("/graphql/saveNotificationUserSettings.gql", variables);

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        return responseGraphQLJson;
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveNotificationUserSettings")
    @Story(value = "Основная функциональность")
    @Test(description = "Сохранение одной настройки уведомлений учеником")
    @Owner(value = "KovalchukIS")
    public void saveOneNotificationUserSettingsTest() throws IOException {

        final JSONObject responseGraphQLJson = saveOneNotificationUserSettings(typeId, channelId, channelActive);

        final Boolean saveNotificationUserSettings = responseGraphQLJson.getJSONObject("data")
                .getJSONObject("notificationConfiguration")
                .getBoolean("saveNotificationUserSettings");
        Assert.assertTrue(saveNotificationUserSettings);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveNotificationUserSettings")
    @Story(value = "Основная функциональность")
    @Test(description = "Сохранение двух настроек уведомлений учеником")
    @Owner(value = "KovalchukIS")
    public void saveTwoNotificationUserSettingsTest() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();

        final ArrayNode settings = variables.putArray("settings");
        settings.addObject().put("typeId", typeId)
                .put("channelId", channelId)
                .put("channelActive", channelActive);
        settings.addObject().put("typeId", 27)
                .put("channelId", channelId)
                .put("channelActive", channelActive);

        final CloseableHttpResponse responseGraphQL = graphQLRequest("/graphql/saveNotificationUserSettings.gql", variables);

        final JSONObject response = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);

        final Boolean saveNotificationUserSettings = response.getJSONObject("data")
                .getJSONObject("notificationConfiguration")
                .getBoolean("saveNotificationUserSettings");
        Assert.assertTrue(saveNotificationUserSettings);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveNotificationUserSettings")
    @Story(value = "Проверка некорректных значений")
    @Test(description = "Сохранение одной настройки уведомлений учеником c некоректным typeId")
    @Owner(value = "KovalchukIS")
    public void saveOneNotificationUserSettingsWithIncorrectTypeIdTest() throws IOException {

        final String expectedErrorRU
                = "javax.validation.ConstraintViolationException: saveNotificationTypeChannelsForUser.arg1[].notificationTypeId: должно быть больше или равно 1";

        final String expectedErrorEN
                = "javax.validation.ConstraintViolationException: saveNotificationTypeChannelsForUser.arg1[].notificationTypeId: must be greater than or equal to 1";

        final JSONObject responseGraphQLJson = saveOneNotificationUserSettings(-1, channelId, channelActive);

        final String errorsString = responseGraphQLJson.getJSONArray("errors").toString();

        Allure.step("Error: " + errorsString);

        Assert.assertTrue(errorsString.contains(expectedErrorRU) || errorsString.contains(expectedErrorEN),
                "Содержит ли текс ошибки " + "\"" + expectedErrorRU + "\"" + " или " + "\"" + expectedErrorEN + "\"?");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveNotificationUserSettings")
    @Story(value = "Проверка некорректных значений")
    @Test(description = "Сохранение одной настройки уведомлений учеником c некоректным channelId")
    @Owner(value = "KovalchukIS")
    public void saveOneNotificationUserSettingsWithIncorrectChannelIdTest() throws IOException {

        final String expectedErrorRU
                = "javax.validation.ConstraintViolationException: saveNotificationTypeChannelsForUser.arg1[].channelId: должно быть больше или равно 1";

        final String expectedErrorEN
                = "javax.validation.ConstraintViolationException: saveNotificationTypeChannelsForUser.arg1[].channelId: must be greater than or equal to 1";

        final JSONObject responseGraphQLJson = saveOneNotificationUserSettings(typeId, -1, channelActive);

        final String errorsString = responseGraphQLJson.getJSONArray("errors").toString();

        Allure.step("Error: " + errorsString);

        Assert.assertTrue(errorsString.contains(expectedErrorRU) || errorsString.contains(expectedErrorEN),
                "Содержит ли текс ошибки " + "\"" + expectedErrorRU + "\"" + " или " + "\"" + expectedErrorEN + "\"?");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveNotificationUserSettings")
    @Story(value = "Проверка некорректных значений")
    @Test(description = "Сохранение одной настройки уведомлений учеником c некоректным channelActive")
    @Owner(value = "KovalchukIS")
    public void saveOneNotificationUserSettingsWithIncorrectChannelActiveTest() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        final ObjectNode settings = variables.putObject("settings");

        settings.put("typeId", typeId)
                .put("channelId", channelActive)
                .put("channelActive", "incorrectValueForBoolean");

        final CloseableHttpResponse responseGraphQL = graphQLRequest("/graphql/saveNotificationUserSettings.gql", variables);

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        Assert.assertTrue(responseGraphQLJson.getJSONArray("errors").toString()
                .contains("Variable 'channelId' has an invalid value"));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveNotificationUserSettings")
    @Story(value = "Проверка null значений")
    @Test(description = "Сохранение одной настройки уведомлений учеником c некоректным typeId = null")
    @Owner(value = "KovalchukIS")
    public void saveOneNotificationUserSettingsWithNullTypeIdTest() throws IOException {

        final Integer nullTypeId = null;

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        final ObjectNode settings = variables.putObject("settings");

        settings.put("typeId", nullTypeId)
                .put("channelId", channelId)
                .put("channelActive", channelActive);

        final CloseableHttpResponse responseGraphQL = graphQLRequest("/graphql/saveNotificationUserSettings.gql", variables);

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        Assert.assertTrue(responseGraphQLJson.getJSONArray("errors").toString()
                .contains("Variable 'settings' has coerced Null value for NonNull type"));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveNotificationUserSettings")
    @Story(value = "Проверка null значений")
    @Test(description = "Сохранение одной настройки уведомлений учеником c некоректным channelId = null")
    @Owner(value = "KovalchukIS")
    public void saveOneNotificationUserSettingsWithNullChannelIdTest() throws IOException {

        final Integer nullChannelId = null;

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        final ObjectNode settings = variables.putObject("settings");

        settings.put("typeId", typeId)
                .put("channelId", nullChannelId)
                .put("channelActive", channelActive);

        final CloseableHttpResponse responseGraphQL = graphQLRequest("/graphql/saveNotificationUserSettings.gql", variables);

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        Assert.assertTrue(responseGraphQLJson.getJSONArray("errors").toString()
                .contains("Variable 'settings' has coerced Null value for NonNull type"));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveNotificationUserSettings")
    @Story(value = "Проверка null значений")
    @Test(description = "Сохранение одной настройки уведомлений учеником c некоректным channelActive = null")
    @Owner(value = "KovalchukIS")
    public void saveOneNotificationUserSettingsWithNullChannelActiveTest() throws IOException {

        final Integer nullChannelActive = null;

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        final ObjectNode settings = variables.putObject("settings");

        settings.put("typeId", typeId)
                .put("channelId", channelActive)
                .put("channelActive", nullChannelActive);

        final CloseableHttpResponse responseGraphQL = graphQLRequest("/graphql/saveNotificationUserSettings.gql", variables);

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        Assert.assertTrue(responseGraphQLJson.getJSONArray("errors").toString()
                .contains("Variable 'channelId' has an invalid value"));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveNotificationUserSettings")
    @Story(value = "Проверка пустых значений")
    @Test(description = "Сохранение одной настройки уведомлений учеником c пустым typeId = \"\" ")
    @Owner(value = "KovalchukIS")
    public void saveOneNotificationUserSettingsWithEmptyTypeIdTest() throws IOException {

        final String expectedErrorRU = "notificationTypeId: должно быть задано";
        final String expectedErrorEN = "notificationTypeId: must not be null";

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        final ObjectNode settings = variables.putObject("settings");

        settings.put("typeId", "")
                .put("channelId", channelId)
                .put("channelActive", channelActive);

        final CloseableHttpResponse responseGraphQL = graphQLRequest("/graphql/saveNotificationUserSettings.gql", variables);

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        final String errorsString = responseGraphQLJson.getJSONArray("errors").toString();

        Allure.step("Error: " + errorsString);

        Assert.assertTrue(errorsString.contains(expectedErrorRU) || errorsString.contains(expectedErrorEN),
                "Содержит ли текс ошибки " + "\"" + expectedErrorRU + "\"" + " или " + "\"" + expectedErrorEN + "\"?");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveNotificationUserSettings")
    @Story(value = "Проверка пустых значений")
    @Test(description = "Сохранение одной настройки уведомлений учеником c пустым channelId = \"\" ")
    @Owner(value = "KovalchukIS")
    public void saveOneNotificationUserSettingsWithEmptyChannelIdTest() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        final ObjectNode settings = variables.putObject("settings");

        settings.put("typeId", typeId)
                .put("channelId", "")
                .put("channelActive", channelActive);

        final CloseableHttpResponse responseGraphQL = graphQLRequest("/graphql/saveNotificationUserSettings.gql", variables);

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        Assert.assertTrue(responseGraphQLJson.getJSONArray("errors").toString()
                .contains("HttpServerErrorException: 500"));
    }
}