package ru.sbt.sber_learning.notification_autotests.graphql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.qameta.allure.*;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;
import ru.sbt.sber_learning.notification_autotests.GraphQLUtilMethods;
import ru.sbt.sber_learning.notification_autotests.UtilMethods;

import java.io.IOException;

@Component
@SpringBootTest(classes = {GraphQLUtilMethods.class, UtilMethods.class})
public class ReadNotificationTeacherGraphQLTests extends AbstractTestNGSpringContextTests {

    @Value("${autotests-users.teacher.login}")
    private String teacherLogin;

    @Value("${autotests-users.password}")
    private String password;

    @Autowired
    private GraphQLUtilMethods graphQLUtilMethods;

    @Autowired
    private UtilMethods utilMethods;

    private static final int STATUS_OK = 200;
    private static final String ORG_UNIT_ID = "00000000-0000-0000-0001-000000000001";
    private static String TEACHER_ID;

    @BeforeClass
    private void setTeacherId() {
        TEACHER_ID = utilMethods.getSelectUserIdFromUsersByLogin(teacherLogin);
    }

    @AfterMethod
    private void cleanup() {
        utilMethods.cleanDBNotifications(TEACHER_ID, ORG_UNIT_ID);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ReadNotificationTeacher")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда указан существующий ID непрочитанной нотификации. Должна стать прочитанной")
    @Owner(value = "Max Kamaev")
    public void readNotification_whenNotificationExistsAndWasNotRead_shouldReturnIDFromDB() throws IOException {

        final Long notificationId = utilMethods.fillDBNotificationsMass(TEACHER_ID, ORG_UNIT_ID, false, false);
        final Long notReadNotificationId = utilMethods.fillDBNotificationsMass(TEACHER_ID, ORG_UNIT_ID, false, false);

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("notificationId", notificationId);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        teacherLogin,
                        password,
                        "/graphql/readNotificationTeacher.graphql",
                        variables,
                        ORG_UNIT_ID
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final String notificationIdFromResponse = responseGraphQLJson
                .getJSONObject("data")
                .getJSONObject("teacher")
                .get("readNotification").toString();

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(notificationIdFromResponse, notificationId.toString());

        Assert.assertTrue(utilMethods.isNotificationRead(notificationId));
        Assert.assertFalse(utilMethods.isNotificationRead(notReadNotificationId));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ReadNotificationTeacher")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда указан существующий ID прочитанной нотификации. Должна остаться прочитанной")
    @Owner(value = "Max Kamaev")
    public void readNotification_whenNotificationExistsAndWasRead_shouldReturnIDFromDB() throws IOException {

        final Long notificationId = utilMethods.fillDBNotificationsMass(TEACHER_ID, ORG_UNIT_ID, true, false);
        final Long notReadNotificationId = utilMethods.fillDBNotificationsMass(TEACHER_ID, ORG_UNIT_ID, false, false);

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("notificationId", notificationId);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        teacherLogin,
                        password,
                        "/graphql/readNotificationTeacher.graphql",
                        variables,
                        ORG_UNIT_ID
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final String notificationIdFromResponse = responseGraphQLJson
                .getJSONObject("data")
                .getJSONObject("teacher")
                .get("readNotification").toString();

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(notificationIdFromResponse, notificationId.toString());

        Assert.assertTrue(utilMethods.isNotificationRead(notificationId));
        Assert.assertFalse(utilMethods.isNotificationRead(notReadNotificationId));
    }

    @Ignore(value = "Bug: EDU-14717. После решения должен возвращать ERROR")
    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ReadNotificationTeacher")
    @Story(value = "Проверка некорректных значений")
    @Test(description = "Когда указан несуществующий ID нотификации.")
    @Owner(value = "Max Kamaev")
    public void readNotification_whenNotificationDoesntExists_shouldReturnError() throws IOException {

        final Long notificationId = utilMethods.fillDBNotificationsMass(TEACHER_ID, ORG_UNIT_ID, false, false);

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("notificationId", notificationId + 1);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        teacherLogin,
                        password,
                        "/graphql/readNotificationTeacher.graphql",
                        variables,
                        ORG_UNIT_ID
                );

        final String errorText = "HttpServerErrorException: 500";
        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        final String errorFromResponse = responseGraphQLJson.getJSONArray("errors").toString();

        Allure.step("Error: " + errorFromResponse);

        Assert.assertTrue(errorFromResponse.contains(errorText), "Содержит ли текст ошибки " + "\"" + errorText + "\"?");

        Assert.assertFalse(utilMethods.isNotificationRead(notificationId));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ReadNotificationTeacher")
    @Story(value = "Проверка некорректных значений")
    @Test(description = "Когда передаем неверный ID школы.")
    @Owner(value = "Max Kamaev")
    public void readNotification_whenWrongSchool_shouldReturnError() throws IOException {

        final Long notificationId = utilMethods.fillDBNotificationsMass(TEACHER_ID, ORG_UNIT_ID, false, false);

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("notificationId", notificationId);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        teacherLogin,
                        password,
                        "/graphql/readNotificationTeacher.graphql",
                        variables,
                        "00000002-0002-0000-0002-000000000002"
                );

        final String errorText = "Нет прав для вызова 'readNotification'. Нужен пермишен: 'studyProcess:notification:read'";
        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertTrue(responseGraphQLJson.getJSONArray("errors").toString().contains(errorText));
        Assert.assertFalse(utilMethods.isNotificationRead(notificationId));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ReadNotificationTeacher")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда notificationId = null")
    @Owner(value = "Max Kamaev")
    public void readNotification_whenNotificationIdIsNull_shouldReturnError() throws IOException {

        final Long notificationId = utilMethods.fillDBNotificationsMass(TEACHER_ID, ORG_UNIT_ID, false, false);

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.putNull("notificationId");

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        teacherLogin,
                        password,
                        "/graphql/readNotificationTeacher.graphql",
                        variables,
                        ORG_UNIT_ID
                );

        final String errorText = "Variable 'notificationId' has coerced Null value for NonNull type 'ID!'";
        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertTrue(responseGraphQLJson.getJSONArray("errors").toString().contains(errorText));
        Assert.assertFalse(utilMethods.isNotificationRead(notificationId));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ReadNotificationTeacher")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда notificationId не передаем")
    @Owner(value = "Max Kamaev")
    public void readNotification_whenNotificationIdNotProvided_shouldReturnError() throws IOException {

        final Long notificationId = utilMethods.fillDBNotificationsMass(TEACHER_ID, ORG_UNIT_ID, false, false);

        final ObjectNode variables = new ObjectMapper().createObjectNode();

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        teacherLogin,
                        password,
                        "/graphql/readNotificationTeacher.graphql",
                        variables,
                        ORG_UNIT_ID
                );

        final String errorText = "Variable 'notificationId' has coerced Null value for NonNull type 'ID!'";
        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertTrue(responseGraphQLJson.getJSONArray("errors").toString().contains(errorText));
        Assert.assertFalse(utilMethods.isNotificationRead(notificationId));
    }
}
