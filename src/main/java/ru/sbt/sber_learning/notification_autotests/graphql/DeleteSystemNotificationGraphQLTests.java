package ru.sbt.sber_learning.notification_autotests.graphql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.sbt.sber_learning.notification_autotests.GraphQLUtilMethods;
import ru.sbt.sber_learning.notification_autotests.UtilMethods;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Component
@SpringBootTest(classes = {GraphQLUtilMethods.class, UtilMethods.class})
public class DeleteSystemNotificationGraphQLTests extends AbstractTestNGSpringContextTests {

    @Value("${autotests-users.sys.login}")
    private String adminSysLogin;

    @Value("${autotests-users.old-password}")
    private String adminSysPassword;

    @Value("${autotests-users.business.login}")
    private String businessLogin;

    @Value("${autotests-users.password}")
    private String password;

    @Autowired
    private GraphQLUtilMethods graphQLUtilMethods;

    @Autowired
    private UtilMethods utilMethods;

    private static final int STATUS_OK = 200;
    private static final String SCHOOL_ID = "00000000-0000-0000-0001-000000000001";
    private static final String FILE_QUERY = "/graphql/deleteSystemNotification.graphql";
    private static final String NOTIFICATION_HEADER = "autotestDeleteHeader";
    private static final String NOTIFICATION_TEXT = "autotestDeleteText";
    private static Long NOTIFICATION_ID_FROM_ADMIN_SYS;
    private static Long NOTIFICATION_ID_FROM_ADMIN_BUSINESS;

    @BeforeMethod
    public void fillDBSystemNotifications() {
        NOTIFICATION_ID_FROM_ADMIN_SYS = utilMethods.fillTableSystemNotifications(
                null,
                NOTIFICATION_HEADER,
                NOTIFICATION_TEXT,
                LocalDateTime.now(ZoneOffset.UTC).toString(),
                LocalDateTime.now(ZoneOffset.UTC).plusMinutes(1).toString(),
                utilMethods.getSelectUserIdFromUsersByLogin(adminSysLogin)
        );

        NOTIFICATION_ID_FROM_ADMIN_BUSINESS = utilMethods.fillTableSystemNotifications(
                SCHOOL_ID,
                NOTIFICATION_HEADER,
                NOTIFICATION_TEXT,
                LocalDateTime.now(ZoneOffset.UTC).toString(),
                LocalDateTime.now(ZoneOffset.UTC).plusMinutes(1).toString(),
                utilMethods.getSelectUserIdFromUsersByLogin(businessLogin)
        );
    }

    @AfterMethod
    private void cleanTableSystemNotifications() {
        utilMethods.cleanTableSystemNotificationsByHeaderAndText(NOTIFICATION_HEADER, NOTIFICATION_TEXT);
        utilMethods.cleanTableNotificationsByTitleAndMessage(NOTIFICATION_HEADER, NOTIFICATION_TEXT);
    }

    private String getJsonSuccessResponse(final CloseableHttpResponse responseGraphQL) throws IOException {
        final JSONObject response = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        return response
                .getJSONObject("data")
                .getJSONObject("systemAdmin")
                .get("deleteSystemNotification").toString();
    }

    private String getJsonErrorResponse(final CloseableHttpResponse responseGraphQL) throws IOException {
        final JSONObject response = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        return response
                .getJSONArray("errors")
                .getJSONObject(0)
                .getJSONObject("extensions")
                .getString("uiErrorCode");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "DeleteSystemNotification")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда значения есть в базе. ")
    @Owner(value = "Anastasiya Krutova")
    public void deleteSystemNotification_everythingExist() throws IOException {

        Assert.assertEquals((int) utilMethods.selectCountSystemNotificationsById(NOTIFICATION_ID_FROM_ADMIN_SYS), 1);

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("id", NOTIFICATION_ID_FROM_ADMIN_SYS);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        adminSysLogin,
                        adminSysPassword,
                        FILE_QUERY,
                        variables,
                        null
                );

        final Long notificationId = Long.valueOf(getJsonSuccessResponse(responseGraphQL));

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(notificationId, NOTIFICATION_ID_FROM_ADMIN_SYS);
        Assert.assertEquals((int) utilMethods.selectCountSystemNotificationsById(NOTIFICATION_ID_FROM_ADMIN_SYS), 0);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "DeleteSystemNotification")
    @Story(value = "Проверка некорректных значений")
    @Test(description = "Попытка удалить уведомление, id которого нет в базе. Должен вернуть Error. ")
    @Owner(value = "Anastasiya Krutova")
    public void deleteSystemNotification_idNotExist() throws IOException {

        final int nonexistentId = 999;
        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("id", nonexistentId);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        adminSysLogin,
                        adminSysPassword,
                        FILE_QUERY,
                        variables,
                        null
                );

        final String response = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(response, "DEFAULT_ERROR_MESSAGE_CODE");
        Assert.assertEquals((int) utilMethods.selectCountSystemNotificationsById(NOTIFICATION_ID_FROM_ADMIN_SYS), 1);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "DeleteSystemNotification")
    @Story(value = "Проверка некорректных значений")
    @Test(description = "Попытка удалить уведомление, которое добавил сис админ, под бизнес админом. ")
    @Owner(value = "Anastasiya Krutova")
    public void deleteSystemNotification_businessAdminAuth() throws IOException {

        Assert.assertEquals((int) utilMethods.selectCountSystemNotificationsById(NOTIFICATION_ID_FROM_ADMIN_SYS), 1);

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("id", NOTIFICATION_ID_FROM_ADMIN_SYS);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        businessLogin,
                        password,
                        FILE_QUERY,
                        variables,
                        SCHOOL_ID
                );

        final Long notificationId = Long.valueOf(getJsonSuccessResponse(responseGraphQL));

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(notificationId, NOTIFICATION_ID_FROM_ADMIN_SYS);
        Assert.assertEquals((int) utilMethods.selectCountSystemNotificationsById(NOTIFICATION_ID_FROM_ADMIN_SYS), 0);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "DeleteSystemNotification")
    @Story(value = "Проверка некорректных значений")
    @Test(description = "Попытка удалить уведомление, которое добавил бизнес админ, под сис админом. ")
    @Owner(value = "Anastasiya Krutova")
    public void deleteSystemNotification_sysAdminAuth() throws IOException {

        Assert.assertEquals((int) utilMethods.selectCountSystemNotificationsById(NOTIFICATION_ID_FROM_ADMIN_BUSINESS), 1);

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("id", NOTIFICATION_ID_FROM_ADMIN_BUSINESS);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        adminSysLogin,
                        adminSysPassword,
                        FILE_QUERY,
                        variables,
                        null
                );

        final Long notificationId = Long.valueOf(getJsonSuccessResponse(responseGraphQL));

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(notificationId, NOTIFICATION_ID_FROM_ADMIN_BUSINESS);
        Assert.assertEquals((int) utilMethods.selectCountSystemNotificationsById(NOTIFICATION_ID_FROM_ADMIN_BUSINESS), 0);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "DeleteSystemNotification")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда id = null. ")
    @Owner(value = "Anastasiya Krutova")
    public void deleteSystemNotification_idNull() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.putNull("id");

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        businessLogin,
                        password,
                        FILE_QUERY,
                        variables,
                        SCHOOL_ID
                );

        final String response = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(response, "DEFAULT_ERROR_MESSAGE_CODE");
        Assert.assertEquals((int) utilMethods.selectCountSystemNotificationsById(NOTIFICATION_ID_FROM_ADMIN_SYS), 1);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "DeleteSystemNotification")
    @Story(value = "Проверка пустых значений")
    @Test(description = "Когда id = \"\". ")
    @Owner(value = "Anastasiya Krutova")
    public void deleteSystemNotification_idNEmpty() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("id", "");

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        businessLogin,
                        password,
                        FILE_QUERY,
                        variables,
                        SCHOOL_ID
                );

        final String response = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(response, "DEFAULT_ERROR_MESSAGE_CODE");
        Assert.assertEquals((int) utilMethods.selectCountSystemNotificationsById(NOTIFICATION_ID_FROM_ADMIN_SYS), 1);
    }
}
