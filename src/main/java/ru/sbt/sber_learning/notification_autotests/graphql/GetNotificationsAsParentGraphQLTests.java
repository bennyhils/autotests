package ru.sbt.sber_learning.notification_autotests.graphql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import ru.sbt.sber_learning.notification_autotests.GraphQLUtilMethods;
import ru.sbt.sber_learning.notification_autotests.UtilMethods;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

@Component
@SpringBootTest(classes = {GraphQLUtilMethods.class, UtilMethods.class})
public class GetNotificationsAsParentGraphQLTests extends AbstractTestNGSpringContextTests {

    private static final int STATUS_OK = 200;
    private static final String ORG_UNIT_ID = "00000000-0000-0000-0001-000000000001";

    @Value("${autotests-users.parent.login}")
    private String parentLogin;

    @Value("${autotests-users.student.login}")
    private String studentLogin;

    @Value("${autotests-users.password}")
    private String password;

    @Autowired
    private GraphQLUtilMethods graphQLUtilMethods;

    @Autowired
    private UtilMethods utilMethods;

    @AfterMethod
    private void cleanup() {
        utilMethods.cleanDBNotifications(utilMethods.getSelectUserIdFromUsersByLogin(studentLogin), ORG_UNIT_ID);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotifications")
    @Story(value = "Parent. Основная функциональность")
    @Test(description = "Когда есть 3 прочитанных и 3 непрочитанных, без пейджинга, wasReadIncluded = true")
    @Owner(value = "Max Kamaev")
    public void getNotificationsAsParent_whenWasReadIncludedTrue_shouldReturnCountFromDB() throws IOException {
        final String childrenId = utilMethods.getSelectUserIdFromUsersByLogin(studentLogin);
        final int count = 3;
        final ArrayList<Long> notificationIds = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            notificationIds.add(i, utilMethods.fillDBNotificationsMass(childrenId, ORG_UNIT_ID, false, false));
        }
        for (int i = 0; i < count; i++){
            notificationIds.add(i, utilMethods.fillDBNotificationsMass(childrenId, ORG_UNIT_ID, true, false));
        }

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("wasReadIncluded", true);
        variables.put("childUserId", childrenId);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        parentLogin,
                        password,
                        "/graphql/getStudentNotificationsAsParent.gql",
                        variables,
                        ORG_UNIT_ID
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final JSONArray notificationsFromResponse = responseGraphQLJson.getJSONObject("data").getJSONObject("parent").getJSONArray("getNotifications");

        final ArrayList<Long> notificationIdsFromResponse = new ArrayList<>();

        for (int i = 0; i < notificationsFromResponse.length(); i++) {
            notificationIdsFromResponse.add(notificationsFromResponse.getJSONObject(i).getLong("id"));
        }

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);

        Collections.sort(notificationIds);
        Collections.sort(notificationIdsFromResponse);

        Assert.assertEquals(notificationIdsFromResponse, notificationIds);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotifications")
    @Story(value = "Parent. Основная функциональность")
    @Test(description = "Когда есть 3 прочитанных и 3 непрочитанных, без пейджинга, wasReadIncluded = false")
    @Owner(value = "Max Kamaev")
    public void getNotificationsAsParent_whenWasReadIncludedFalse_shouldReturnCountFromDB() throws IOException {
        final String childrenId = utilMethods.getSelectUserIdFromUsersByLogin(studentLogin);
        final int count = 3;
        final ArrayList<Long> notificationIds = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            notificationIds.add(i, utilMethods.fillDBNotificationsMass(childrenId, ORG_UNIT_ID, false, false));
        }
        //не считаем прочитанные сообщения
        utilMethods.fillDBNotificationsMass(3, childrenId, ORG_UNIT_ID, true, false);


        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("wasReadIncluded", false);
        variables.put("childUserId", childrenId);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        parentLogin,
                        password,
                        "/graphql/getStudentNotificationsAsParent.gql",
                        variables,
                        ORG_UNIT_ID
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final JSONArray notificationsFromResponse = responseGraphQLJson.getJSONObject("data").getJSONObject("parent").getJSONArray("getNotifications");

        final ArrayList<Long> notificationIdsFromResponse = new ArrayList<>();

        for (int i = 0; i < notificationsFromResponse.length(); i++) {
            notificationIdsFromResponse.add(notificationsFromResponse.getJSONObject(i).getLong("id"));
        }

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);

        Collections.sort(notificationIds);
        Collections.sort(notificationIdsFromResponse);

        Assert.assertEquals(notificationIdsFromResponse, notificationIds);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotifications")
    @Story(value = "Parent. Основная функциональность")
    @Test(description = "Когда есть 3 прочитанных и 3 непрочитанных, c пейджингом последние 2, wasReadIncluded = true")
    @Owner(value = "Max Kamaev")
    public void getNotificationsAsParent_withPaging_shouldReturnCountFromDB() throws IOException {
        final String childrenId = utilMethods.getSelectUserIdFromUsersByLogin(studentLogin);

        final int count = 3;
        final ArrayList<Long> notificationIds = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            notificationIds.add(i, utilMethods.fillDBNotificationsMass(childrenId, ORG_UNIT_ID, false, false));
        }
        for (int i = 0; i < count; i++){
            notificationIds.add(i, utilMethods.fillDBNotificationsMass(childrenId, ORG_UNIT_ID, true, false));
        }

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("wasReadIncluded", true);
        variables.put("childUserId", childrenId);
        final ObjectNode paging = variables.putObject("paging");
        paging.put("offset",4);
        paging.put("limit",4);


        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        parentLogin,
                        password,
                        "/graphql/getStudentNotificationsAsParent.gql",
                        variables,
                        ORG_UNIT_ID
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final JSONArray notificationsFromResponse = responseGraphQLJson.getJSONObject("data").getJSONObject("parent").getJSONArray("getNotifications");

        final ArrayList<Long> notificationIdsFromResponse = new ArrayList<>();

        for (int i = 0; i < notificationsFromResponse.length(); i++) {
            notificationIdsFromResponse.add(notificationsFromResponse.getJSONObject(i).getLong("id"));
        }
        Collections.sort(notificationIds);
        Collections.sort(notificationIdsFromResponse);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        //проверка что получили 2 самых старых уведомления
        for (int i = 0; i < 2; i++) {
            Assert.assertEquals(notificationIds.get(i), notificationIdsFromResponse.get(i));
        }

    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotifications")
    @Story(value = "Parent. Некорректные значения")
    @Test(description = "Когда есть 3 прочитанных и 3 непрочитанных, неправильный ID ребенка")
    @Owner(value = "Max Kamaev")
    public void getNotificationsAsParent_whenWrongChildrenId_shouldReturnError() throws IOException {
        final String childrenId = utilMethods.getSelectUserIdFromUsersByLogin(studentLogin);

        utilMethods.fillDBNotificationsMass(3, childrenId, ORG_UNIT_ID, false, false);
        utilMethods.fillDBNotificationsMass(3, childrenId, ORG_UNIT_ID, true, false);

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("wasReadIncluded", true);
        variables.put("childUserId", "03000000-0000-0000-0001-000000000003");

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        parentLogin,
                        password,
                        "/graphql/getStudentNotificationsAsParent.gql",
                        variables,
                        ORG_UNIT_ID
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);

        Assert.assertTrue(responseGraphQLJson.getJSONArray("errors").toString()
                .contains("AppRuntimeException: Пользователь с идентификатором 03000000-0000-0000-0001-000000000003 не является ребенком Autotests Parent"));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotifications")
    @Story(value = "Parent. Некорректные значения")
    @Test(description = "Когда не передаем обязательный параметр ID ребенка")
    @Owner(value = "Max Kamaev")
    public void getNotificationsAsParent_whenChildrenIdNotProvided_shouldReturnError() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        parentLogin,
                        password,
                        "/graphql/getStudentNotificationsAsParent.gql",
                        variables,
                        ORG_UNIT_ID
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);

        Assert.assertTrue(responseGraphQLJson.getJSONArray("errors").toString()
                .contains("NonNullableValueCoercedAsNullException: Variable 'childUserId' has coerced Null value for NonNull type 'ID!'"));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotifications")
    @Story(value = "Parent. Null значения")
    @Test(description = "Когда childUserId = null")
    @Owner(value = "Max Kamaev")
    public void getNotificationsAsParent_whenChildIdIsNull_shouldReturnError() throws IOException {
        final String childrenId = utilMethods.getSelectUserIdFromUsersByLogin(studentLogin);

        utilMethods.fillDBNotificationsMass(3, childrenId, ORG_UNIT_ID, false, false);
        utilMethods.fillDBNotificationsMass(3, childrenId, ORG_UNIT_ID, true, false);

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("wasReadIncluded", true);
        variables.putNull("childUserId");
        final ObjectNode paging = variables.putObject("paging");
        paging.put("offset",0);
        paging.put("limit", 5);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        parentLogin,
                        password,
                        "/graphql/getStudentNotificationsAsParent.gql",
                        variables,
                        ORG_UNIT_ID
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);

        Assert.assertTrue(responseGraphQLJson.getJSONArray("errors").toString()
                .contains("NonNullableValueCoercedAsNullException: Variable 'childUserId' has coerced Null value for NonNull type 'ID!'"));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotifications")
    @Story(value = "Parent. Null значения")
    @Test(description = "Когда wasReadInluded = null")
    @Owner(value = "Max Kamaev")
    public void getNotificationsAsParent_whenWasReadIncludedIsNull_shouldReturnCountFromDB() throws IOException {
        final String childrenId = utilMethods.getSelectUserIdFromUsersByLogin(studentLogin);
        final int count = 3;
        final ArrayList<Long> notificationIds = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            notificationIds.add(i, utilMethods.fillDBNotificationsMass(childrenId, ORG_UNIT_ID, false, false));
        }
        //не считаем прочитанные сообщения
        utilMethods.fillDBNotificationsMass(3, childrenId, ORG_UNIT_ID, true, false);


        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.putNull("wasReadIncluded");
        variables.put("childUserId", childrenId);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        parentLogin,
                        password,
                        "/graphql/getStudentNotificationsAsParent.gql",
                        variables,
                        ORG_UNIT_ID
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final JSONArray notificationsFromResponse = responseGraphQLJson.getJSONObject("data").getJSONObject("parent").getJSONArray("getNotifications");

        final ArrayList<Long> notificationIdsFromResponse = new ArrayList<>();

        for (int i = 0; i < notificationsFromResponse.length(); i++) {
            notificationIdsFromResponse.add(notificationsFromResponse.getJSONObject(i).getLong("id"));
        }

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);

        Collections.sort(notificationIds);
        Collections.sort(notificationIdsFromResponse);

        Assert.assertEquals(notificationIdsFromResponse, notificationIds);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotifications")
    @Story(value = "Parent. Null значения")
    @Test(description = "Когда paging = null")
    @Owner(value = "Max Kamaev")
    public void getNotificationsAsParent_whenPagingIsNull_shouldCountFromDB() throws IOException {
        final String childrenId = utilMethods.getSelectUserIdFromUsersByLogin(studentLogin);
        final int count = 3;
        final ArrayList<Long> notificationIds = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            notificationIds.add(i, utilMethods.fillDBNotificationsMass(childrenId, ORG_UNIT_ID, false, false));
        }
        for (int i = 0; i < count; i++){
            notificationIds.add(i, utilMethods.fillDBNotificationsMass(childrenId, ORG_UNIT_ID, true, false));
        }

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("wasReadIncluded", true);
        variables.put("childUserId", childrenId);
        variables.putNull("paging");

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        parentLogin,
                        password,
                        "/graphql/getStudentNotificationsAsParent.gql",
                        variables,
                        ORG_UNIT_ID
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final JSONArray notificationsFromResponse = responseGraphQLJson.getJSONObject("data").getJSONObject("parent").getJSONArray("getNotifications");

        final ArrayList<Long> notificationIdsFromResponse = new ArrayList<>();

        for (int i = 0; i < notificationsFromResponse.length(); i++) {
            notificationIdsFromResponse.add(notificationsFromResponse.getJSONObject(i).getLong("id"));
        }

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);

        Collections.sort(notificationIds);
        Collections.sort(notificationIdsFromResponse);

        Assert.assertEquals(notificationIdsFromResponse, notificationIds);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotifications")
    @Story(value = "Parent. Null значения")
    @Test(description = "Когда есть paging, но limit = null")
    @Owner(value = "Max Kamaev")
    public void getNotificationsAsParent_whenLimitIsNull_shouldReturnError() throws IOException {
        final String childrenId = utilMethods.getSelectUserIdFromUsersByLogin(studentLogin);

        utilMethods.fillDBNotificationsMass(3, childrenId, ORG_UNIT_ID, false, false);
        utilMethods.fillDBNotificationsMass(3, childrenId, ORG_UNIT_ID, true, false);

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("wasReadIncluded", true);
        variables.put("childUserId", childrenId);
        final ObjectNode paging = variables.putObject("paging");
        paging.put("offset",4);
        paging.putNull("limit");

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        parentLogin,
                        password,
                        "/graphql/getStudentNotificationsAsParent.gql",
                        variables,
                        ORG_UNIT_ID
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);

        Assert.assertTrue(responseGraphQLJson.getJSONArray("errors").toString()
                .contains("NonNullableValueCoercedAsNullException: Variable 'paging' has coerced Null value for NonNull type 'Int!'"));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotifications")
    @Story(value = "Parent. Null значения")
    @Test(description = "Когда есть paging, но offset = null")
    @Owner(value = "Max Kamaev")
    public void getNotificationsAsParent_whenOffsetIsNull_shouldReturnError() throws IOException {
        final String childrenId = utilMethods.getSelectUserIdFromUsersByLogin(studentLogin);

        utilMethods.fillDBNotificationsMass(3, childrenId, ORG_UNIT_ID, false, false);
        utilMethods.fillDBNotificationsMass(3, childrenId, ORG_UNIT_ID, true, false);

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("wasReadIncluded", true);
        variables.put("childUserId", childrenId);
        final ObjectNode paging = variables.putObject("paging");
        paging.putNull("offset");
        paging.put("limit",4);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        parentLogin,
                        password,
                        "/graphql/getStudentNotificationsAsParent.gql",
                        variables,
                        ORG_UNIT_ID
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);

        Assert.assertTrue(responseGraphQLJson.getJSONArray("errors").toString()
                .contains("NonNullableValueCoercedAsNullException: Variable 'paging' has coerced Null value for NonNull type 'Int!'"));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotifications")
    @Story(value = "Parent. Некорректные значения")
    @Test(description = "Когда есть 3 прочитанных и 3 непрочитанных, неправильный ID школы")
    @Owner(value = "Max Kamaev")
    public void getNotificationsAsParent_whenWrongSchoolId_shouldReturnError() throws IOException {
        final String childrenId = utilMethods.getSelectUserIdFromUsersByLogin(studentLogin);

        utilMethods.fillDBNotificationsMass(3, childrenId, ORG_UNIT_ID, false, false);
        utilMethods.fillDBNotificationsMass(3, childrenId, ORG_UNIT_ID, true, false);

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("wasReadIncluded", true);
        variables.put("childUserId", childrenId);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        parentLogin,
                        password,
                        "/graphql/getStudentNotificationsAsParent.gql",
                        variables,
                        "00000000-0000-0000-0001-000000000002"
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);

        Assert.assertTrue(responseGraphQLJson.getJSONArray("errors").toString()
                .contains("AppRuntimeException: Нет прав для вызова 'getNotifications'. Нужен пермишен: 'studyProcess:notification:getAll'"));
    }
}
