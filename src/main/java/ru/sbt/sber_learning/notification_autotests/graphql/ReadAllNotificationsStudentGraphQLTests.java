package ru.sbt.sber_learning.notification_autotests.graphql;

import io.qameta.allure.*;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.sbt.sber_learning.notification_autotests.GraphQLUtilMethods;
import ru.sbt.sber_learning.notification_autotests.UtilMethods;

import java.io.IOException;

@Component
@SpringBootTest(classes = {GraphQLUtilMethods.class, UtilMethods.class})
public class ReadAllNotificationsStudentGraphQLTests extends AbstractTestNGSpringContextTests {

    @Value("${autotests-users.student.login}")
    private String studentLogin;

    @Value("${autotests-users.password}")
    private String password;

    @Autowired
    private GraphQLUtilMethods graphQLUtilMethods;

    @Autowired
    private UtilMethods utilMethods;

    private static final int STATUS_OK = 200;
    private static final String SCHOOL_ID = "00000000-0000-0000-0001-000000000001";
    private static final String FILE_QUERY = "/graphql/readAllNotificationsStudent.graphql";
    private static final String ERROR_TEXT = "Нет прав для вызова 'readAllNotifications'. " +
                                             "Нужен пермишен: 'studyProcess:notification:read'";
    private static String teacherId;

    @BeforeClass
    private void setTeacherId() {
        teacherId = utilMethods.getSelectUserIdFromUsersByLogin(studentLogin);
    }

    @AfterMethod
    private void cleanup() {
        utilMethods.cleanDBNotifications(utilMethods.getSelectUserIdFromUsersByLogin(studentLogin), SCHOOL_ID);
    }

    private String getJsonSuccessResponse(final CloseableHttpResponse responseGraphQL) throws IOException {
        final JSONObject response = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        return response
                .getJSONObject("data")
                .getJSONObject("student")
                .get("readAllNotifications").toString();
    }

    private JSONObject getJsonErrorResponse(final CloseableHttpResponse responseGraphQL) throws IOException {
        final JSONObject response = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        return response
                .getJSONArray("errors")
                .getJSONObject(0)
                .getJSONObject("extensions");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ReadAllNotificationsStudent")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда есть непрочитанные нотификации. Должен вернуть true. ")
    @Owner(value = "Anastasiya Krutova")
    public void readAllNotifications_whenUnreadExist_shouldReturnTrue() throws IOException {

        final Long readNotificationId = utilMethods.fillDBNotificationsMass(teacherId, SCHOOL_ID, true, false);

        final Long[] notReadNotificationIds = utilMethods.fillDBNotificationsMass(
                2,
                teacherId,
                SCHOOL_ID,
                false,
                false
        );

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        FILE_QUERY,
                        null,
                        SCHOOL_ID
                );

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertTrue(utilMethods.isNotificationRead(readNotificationId));
        Assert.assertTrue(utilMethods.isNotificationRead(notReadNotificationIds[0]));
        Assert.assertTrue(utilMethods.isNotificationRead(notReadNotificationIds[1]));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ReadAllNotificationsStudent")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда нет непрочитанных нотификаций. Должен вернуть true. ")
    @Owner(value = "Anastasiya Krutova")
    public void readAllNotifications_whenUnreadNotExist_shouldReturnTrue() throws IOException {

        final Long[] readNotificationIds = utilMethods.fillDBNotificationsMass(
                2,
                teacherId,
                SCHOOL_ID,
                true,
                false
        );

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        FILE_QUERY,
                        null,
                        SCHOOL_ID
                );

        final String resultFromResponse = getJsonSuccessResponse(responseGraphQL);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertTrue(Boolean.parseBoolean(resultFromResponse), "Все уведомления были прочитаны?");
        Assert.assertTrue(utilMethods.isNotificationRead(readNotificationIds[0]));
        Assert.assertTrue(utilMethods.isNotificationRead(readNotificationIds[1]));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ReadAllNotificationsStudent")
    @Story(value = "Проверка некорректных значений")
    @Test(description = "Когда передаем неверную школу. ")
    @Owner(value = "Anastasiya Krutova")
    public void readAllNotifications_whenSchoolNotExist_shouldReturnError() throws IOException {

        final Long notReadNotificationId = utilMethods.fillDBNotificationsMass(teacherId, SCHOOL_ID, false, false);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        FILE_QUERY,
                        null,
                        "00000999-0999-0999-0999-000000000999"
                );

        final JSONObject errors = getJsonErrorResponse(responseGraphQL);
        final String uiErrorCode = errors.getString("uiErrorCode");

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertFalse(utilMethods.isNotificationRead(notReadNotificationId));
        Assert.assertEquals(uiErrorCode, "SECURITY_ACCESS_DENIED");

        Allure.step("Error: " + errors.toString());
        Assert.assertTrue(errors.toString().contains(ERROR_TEXT),
                "Содержит ли текст ошибки " + "\"" + ERROR_TEXT + "\"?");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ReadAllNotificationsStudent")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда schoolId = null. ")
    @Owner(value = "Anastasiya Krutova")
    public void readAllNotifications_whenSchoolNotProvided_shouldReturnError() throws IOException {
        final String teacherId = utilMethods.getSelectUserIdFromUsersByLogin(studentLogin);

        final Long notReadNotificationId = utilMethods.fillDBNotificationsMass(teacherId, SCHOOL_ID, false, false);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        FILE_QUERY,
                        null,
                        null
                );

        final JSONObject errors = getJsonErrorResponse(responseGraphQL);
        final String uiErrorCode = errors.getString("uiErrorCode");

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertFalse(utilMethods.isNotificationRead(notReadNotificationId));
        Assert.assertEquals(uiErrorCode, "SECURITY_ACCESS_DENIED");

        Allure.step("Error: " + errors.toString());
        Assert.assertTrue(errors.toString().contains(ERROR_TEXT),
                "Содержит ли текст ошибки " + "\"" + ERROR_TEXT + "\"?");
    }
}
