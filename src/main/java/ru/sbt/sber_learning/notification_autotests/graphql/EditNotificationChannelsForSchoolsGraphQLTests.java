package ru.sbt.sber_learning.notification_autotests.graphql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.qameta.allure.*;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import ru.sbt.sber_learning.notification_autotests.GraphQLUtilMethods;
import ru.sbt.sber_learning.notification_autotests.UtilMethods;

import java.io.IOException;
import java.util.Collections;


@Component
@SpringBootTest(classes = {GraphQLUtilMethods.class, UtilMethods.class})
public class EditNotificationChannelsForSchoolsGraphQLTests extends AbstractTestNGSpringContextTests {

    @Value("${autotests-users.sys.login}")
    private String login;

    @Value("${autotests-users.old-password}")
    private String password;

    final static private String ORG_UNIT_ID = "312ffa69-d1a8-482e-825c-949117777777";
    final private Long channelId = 1L;
    final private Long channelId2 = 2L;
    final private Boolean channelActiveFalse = false;

    final ObjectNode objectNode = new ObjectMapper().createObjectNode();

    @Autowired
    UtilMethods utilMethods;

    @Autowired
    GraphQLUtilMethods graphQLUtilMethods;

    @AfterClass
    private void cleanup() {
        //example for logs
        final Integer rowDeleted = utilMethods.cleanTableOrganizationChannels(channelId, Collections.singleton(ORG_UNIT_ID));
        utilMethods.cleanTableOrganizationChannels(channelId2, Collections.singleton(ORG_UNIT_ID));
    }

    private JSONObject executeEditNotificationChannelForSchool(
            final Long channelId,
            final Boolean channelActive,
            final String orgUnitId) throws IOException {

        final ObjectNode variables = objectNode;
        final ObjectNode schoolChannelsList = variables.putObject("schoolChannelsList");
        schoolChannelsList.put("schoolId", orgUnitId);

        final ObjectNode channels = schoolChannelsList.putObject("channels");

        channels
                .put("channelId", channelId)
                .put("channelActive", channelActive);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        login,
                        password,
                        "/graphql/editNotificationChannelsForSchools.gql",
                        variables,
                        null
                );
        return new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
    }

    private JSONObject executeEditNotificationChannelForSchoolWithEmptyChannelId(
            final Boolean channelActive) throws IOException {

        final ObjectNode variables = objectNode;
        final ObjectNode schoolChannelsList = variables.putObject("schoolChannelsList");
        schoolChannelsList.put("schoolId", ORG_UNIT_ID);

        final ObjectNode channels = schoolChannelsList.putObject("channels");

        channels
                .put("channelId", "")
                .put("channelActive", channelActive);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        login,
                        password,
                        "/graphql/editNotificationChannelsForSchools.gql",
                        variables,
                        null
                );
        return new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
    }

    private JSONObject executeEditNotificationChannelForSchoolWithEmptyOrgUnitID(
            final Long channelId,
            final Boolean channelActive) throws IOException {

        final ObjectNode variables = objectNode;
        final ObjectNode schoolChannelsList = variables.putObject("schoolChannelsList");
        schoolChannelsList.put("schoolId", "");

        final ObjectNode channels = schoolChannelsList.putObject("channels");

        channels
                .put("channelId", channelId)
                .put("channelActive", channelActive);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        login,
                        password,
                        "/graphql/editNotificationChannelsForSchools.gql",
                        variables,
                        null
                );
        return new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
    }

    private JSONObject executeEditNotificationTwoChannelsForSchool(
            final Long channelId,
            final Long channelId2,
            final Boolean channelActive) throws IOException {

        final ObjectNode variables = objectNode;
        final ObjectNode schoolChannelsList = variables.putObject("schoolChannelsList");
        schoolChannelsList.put("schoolId", ORG_UNIT_ID);

        final ObjectNode channels = schoolChannelsList.putObject("channels");

        channels
                .put("channelId", channelId)
                .put("channelActive", channelActive)
                .put("channelId", channelId2)
                .put("channelActive", channelActive);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        login,
                        password,
                        "/graphql/editNotificationChannelsForSchools.gql",
                        variables,
                        null
                );
        return new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "EditNotificationChannelsForSchools")
    @Story(value = "Основная функциональность")
    @Test(description = "Выключение и включение канала нотификаций '1' для ненастоящего (тестового) значения orgUnitId школы")
    @Owner(value = "KovalchukIS")
    public void editNotificationChannelsForSchoolsTest() throws IOException {

        executeEditNotificationChannelForSchool(channelId,  channelActiveFalse, ORG_UNIT_ID);
        JSONObject response = executeEditNotificationChannelForSchool(channelId, !channelActiveFalse, ORG_UNIT_ID);
        final Boolean editNotificationChannelsForSchools = response.getJSONObject("data").getJSONObject("notificationConfiguration").getBoolean("editNotificationChannelsForSchools");

        Assert.assertTrue(utilMethods.isChannelActiveInDBOrgUnitActiveChannel(channelId, ORG_UNIT_ID));

        Assert.assertTrue(editNotificationChannelsForSchools);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "EditNotificationChannelsForSchools")
    @Story(value = "Основная функциональность")
    @Test(description = "Выключение и включение канала нотификаций '1' и '2' для ненастоящего (тестового) значения orgUnitId школы")
    @Owner(value = "KovalchukIS")
    public void editNotificationTwoChannelsForSchoolTest() throws IOException {

        executeEditNotificationTwoChannelsForSchool(channelId, channelId2, channelActiveFalse);
        JSONObject response = executeEditNotificationTwoChannelsForSchool(channelId, channelId2, !channelActiveFalse);
        final Boolean editNotificationChannelsForSchools = response.getJSONObject("data").getJSONObject("notificationConfiguration").getBoolean("editNotificationChannelsForSchools");

        Assert.assertTrue(utilMethods.isChannelActiveInDBOrgUnitActiveChannel(channelId, ORG_UNIT_ID));
        Assert.assertTrue(utilMethods.isChannelActiveInDBOrgUnitActiveChannel(channelId2, ORG_UNIT_ID));

        Assert.assertTrue(editNotificationChannelsForSchools);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "EditNotificationChannelsForSchools")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда channelId = null")
    @Owner(value = "KovalchukIS")
    public void editNotificationChannelsForSchoolWithNullChannelIdTest() throws IOException {

        JSONObject response = executeEditNotificationChannelForSchool(null, channelActiveFalse, ORG_UNIT_ID);
        final JSONArray errors = response.getJSONArray("errors");

        Assert.assertTrue(errors.toString()
                .contains("Variable 'schoolChannelsList' has coerced Null value for NonNull type"));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "EditNotificationChannelsForSchools")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда channelActive = null")
    @Owner(value = "KovalchukIS")
    public void editNotificationChannelsForSchoolWithNullChannelActiveTest() throws IOException {

        JSONObject response = executeEditNotificationChannelForSchool(channelId, null, ORG_UNIT_ID);
        final JSONArray errors = response.getJSONArray("errors");

        Assert.assertTrue(errors.toString()
                .contains("Variable 'schoolChannelsList' has coerced Null value for NonNull type"));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "EditNotificationChannelsForSchools")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда orgUnitId = null")
    @Owner(value = "KovalchukIS")
    public void editNotificationChannelsForSchoolWithNullOrgUnitIdTest() throws IOException {

        JSONObject response = executeEditNotificationChannelForSchool(channelId, channelActiveFalse, null);
        final JSONArray errors = response.getJSONArray("errors");

        Assert.assertTrue(errors.toString()
                .contains("Variable 'schoolChannelsList' has coerced Null value for NonNull type"));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "EditNotificationChannelsForSchools")
    @Story(value = "Проверка пустых значений")
    @Test(description = "Когда channelId = ''")
    @Owner(value = "KovalchukIS")
    public void editNotificationChannelsForSchoolWithEmptyChannelIdTest() throws IOException {

        JSONObject response = executeEditNotificationChannelForSchoolWithEmptyChannelId(channelActiveFalse);

        final String expectedErrorRU
                = "javax.validation.ConstraintViolationException: deactivateChannelsForOrganizations.arg0: должно быть больше или равно 1";

        final String expectedErrorEN
                = "javax.validation.ConstraintViolationException: deactivateChannelsForOrganizations.arg0: must be greater than or equal to 1";

        final JSONArray errors = response.getJSONArray("errors");


        final String errorsString = errors.toString();

        Allure.step("Error: " + errorsString);

        Assert.assertTrue(errorsString.contains(expectedErrorEN) || errorsString.contains(expectedErrorRU),
                "Содержит ли текс ошибки " + "\"" + expectedErrorEN + "\"" + " или " + "\"" + expectedErrorRU + "\"" + "?");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "EditNotificationChannelsForSchools")
    @Story(value = "Проверка пустых значений")
    @Test(description = "Когда orgUnitId = ''")
    @Owner(value = "KovalchukIS")
    public void editNotificationChannelsForSchoolWithEmptyOrgUnitIdTest() throws IOException {

        final String expectedError = "Запрещено передавать пустое значение orgUnitId";

        JSONObject response = executeEditNotificationChannelForSchoolWithEmptyOrgUnitID(channelId, channelActiveFalse);


        final JSONArray errors = response.getJSONArray("errors");
        final String errorsString = errors.toString();

        Allure.step("Error: " + errorsString);

        Assert.assertTrue(errorsString.contains(expectedError), "Содержит ли текст ошибки " + "\"" + expectedError + "\"?");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "EditNotificationChannelsForSchools")
    @Story(value = "Проверка некорректных значений")
    @Test(description = "Некорректный channelId = -1")
    @Owner(value = "KovalchukIS")
    public void editNotificationChannelsForSchoolsWithIncorrectChannelIdTest() throws IOException {

        final String expectedErrorRU
                = "javax.validation.ConstraintViolationException: deactivateChannelsForOrganizations.arg0: должно быть больше или равно 1";

        final String expectedErrorEN
                = "javax.validation.ConstraintViolationException: deactivateChannelsForOrganizations.arg0: must be greater than or equal to 1";

        JSONObject response = executeEditNotificationChannelForSchool((-1L), channelActiveFalse, ORG_UNIT_ID);


        final JSONArray errors = response.getJSONArray("errors");

        final String errorsString = errors.toString();

        Allure.step("Error: " + errorsString);

        Assert.assertTrue(errorsString.contains(expectedErrorRU) || errorsString.contains(expectedErrorEN),
                "Содержит ли текс ошибки " + "\"" + expectedErrorRU + "\"" + " или " + "\"" + expectedErrorEN + "\"" + "?");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "EditNotificationChannelsForSchools")
    @Story(value = "Проверка некорректных значений")
    @Test(description = "Некорректный orgUnitId = 'incorrectOrgUnitID'")
    @Owner(value = "KovalchukIS")
    public void editNotificationChannelsForSchoolsWithIncorrectOrgUnitIdTest() throws IOException {

        JSONObject response = executeEditNotificationChannelForSchool(channelId, channelActiveFalse, "incorrectOrgUnitID");

        final JSONArray errors = response.getJSONArray("errors");

        Assert.assertTrue(errors.toString()
                .contains("Cannot deserialize value of type `java.util.UUID` from String"));
    }
}