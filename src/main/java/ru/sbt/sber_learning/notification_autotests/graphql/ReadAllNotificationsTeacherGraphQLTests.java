package ru.sbt.sber_learning.notification_autotests.graphql;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import ru.sbt.sber_learning.notification_autotests.GraphQLUtilMethods;
import ru.sbt.sber_learning.notification_autotests.UtilMethods;

import java.io.IOException;

@Component
@SpringBootTest(classes = {GraphQLUtilMethods.class, UtilMethods.class})
public class ReadAllNotificationsTeacherGraphQLTests extends AbstractTestNGSpringContextTests {

    @Value("${autotests-users.teacher.login}")
    private String teacherLogin;

    @Value("${autotests-users.password}")
    private String password;

    @Autowired
    private GraphQLUtilMethods graphQLUtilMethods;

    @Autowired
    private UtilMethods utilMethods;

    private static final int STATUS_OK = 200;
    private static final String ORG_UNIT_ID = "00000000-0000-0000-0001-000000000001";

    @AfterMethod
    private void cleanup() {
        utilMethods.cleanDBNotifications(utilMethods.getSelectUserIdFromUsersByLogin(teacherLogin), ORG_UNIT_ID);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ReadAllNotificationsTeacher")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда есть непрочитанные нотификации. Должен вернуть true")
    @Owner(value = "Max Kamaev")
    public void readAllNotifications_whenUnreadExist_shouldReturnTrue() throws IOException {
        final String teacherId = utilMethods.getSelectUserIdFromUsersByLogin(teacherLogin);

        final Long notificationId = utilMethods.fillDBNotificationsMass(teacherId, ORG_UNIT_ID, true, false);
        final Long notReadNotificationId = utilMethods.fillDBNotificationsMass(teacherId, ORG_UNIT_ID, false, false);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        teacherLogin,
                        password,
                        "/graphql/readAllNotificationsTeacher.graphql",
                        null,
                        ORG_UNIT_ID
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final String resultFromResponse = responseGraphQLJson
                .getJSONObject("data")
                .getJSONObject("teacher")
                .get("readAllNotifications").toString();

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(resultFromResponse, "true");

        Assert.assertTrue(utilMethods.isNotificationRead(notificationId));
        Assert.assertTrue(utilMethods.isNotificationRead(notReadNotificationId));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ReadAllNotificationsTeacher")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда нет непрочитанных нотификаций. Должен вернуть true")
    @Owner(value = "Max Kamaev")
    public void readAllNotifications_whenUnreadNotExist_shouldReturnTrue() throws IOException {
        final String teacherId = utilMethods.getSelectUserIdFromUsersByLogin(teacherLogin);

        final Long notificationId = utilMethods.fillDBNotificationsMass(teacherId, ORG_UNIT_ID, true, false);
        final Long notificationId1 = utilMethods.fillDBNotificationsMass(teacherId, ORG_UNIT_ID, true, false);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        teacherLogin,
                        password,
                        "/graphql/readAllNotificationsTeacher.graphql",
                        null,
                        ORG_UNIT_ID
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final String resultFromResponse = responseGraphQLJson
                .getJSONObject("data")
                .getJSONObject("teacher")
                .get("readAllNotifications").toString();

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);

        Assert.assertEquals(resultFromResponse, "true", "Все уведомления были прочитаны?");

        Assert.assertTrue(utilMethods.isNotificationRead(notificationId));
        Assert.assertTrue(utilMethods.isNotificationRead(notificationId1));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ReadAllNotificationsTeacher")
    @Story(value = "Проверка некорректных значений")
    @Test(description = "Когда передаем неверную школу")
    @Owner(value = "Max Kamaev")
    public void readAllNotifications_whenSchoolNotExist_shouldReturnError() throws IOException {
        final String teacherId = utilMethods.getSelectUserIdFromUsersByLogin(teacherLogin);

        final Long notificationId = utilMethods.fillDBNotificationsMass(teacherId, ORG_UNIT_ID, false, false);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        teacherLogin,
                        password,
                        "/graphql/readAllNotificationsTeacher.graphql",
                        null,
                        "00000000-0000-0000-0001-000000000022"
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        Assert.assertTrue(responseGraphQLJson.getJSONArray("errors").toString()
                .contains("Нет прав для вызова 'readAllNotifications'. Нужен пермишен: 'studyProcess:notification:read'"));

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertFalse(utilMethods.isNotificationRead(notificationId));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ReadAllNotificationsTeacher")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда ID школы - null")
    @Owner(value = "Max Kamaev")
    public void readAllNotifications_whenSchoolNotProvided_shouldReturnError() throws IOException {
        final String teacherId = utilMethods.getSelectUserIdFromUsersByLogin(teacherLogin);

        final Long notificationId = utilMethods.fillDBNotificationsMass(teacherId, ORG_UNIT_ID, false, false);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        teacherLogin,
                        password,
                        "/graphql/readAllNotificationsTeacher.graphql",
                        null,
                        null
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        Assert.assertTrue(responseGraphQLJson.getJSONArray("errors").toString()
                .contains("Нет прав для вызова 'readAllNotifications'. Нужен пермишен: 'studyProcess:notification:read'"));

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertFalse(utilMethods.isNotificationRead(notificationId));
    }
}
