package ru.sbt.sber_learning.notification_autotests.graphql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.sbt.sber_learning.notification_autotests.GraphQLUtilMethods;
import ru.sbt.sber_learning.notification_autotests.UtilMethods;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

@Component
@SpringBootTest(classes = {GraphQLUtilMethods.class, UtilMethods.class})
public class GetChannelsByOrgUnitIdGraphQLTests extends AbstractTestNGSpringContextTests {

    @Value("${autotests-users.sys.login}")
    private String adminSysLogin;

    @Value("${autotests-users.old-password}")
    private String adminSysPassword;

    @Value("${autotests-users.business.login}")
    private String businessLogin;

    @Value("${autotests-users.teacher.login}")
    private String teacherLogin;

    @Value("${autotests-users.parent.login}")
    private String parentLogin;

    @Value("${autotests-users.student.login}")
    private String studentLogin;

    @Value("${autotests-users.password}")
    private String password;

    @Autowired
    private GraphQLUtilMethods graphQLUtilMethods;

    @Autowired
    private UtilMethods utilMethods;

    private static final int STATUS_OK = 200;
    private static final int BAD_REQUEST = 400;
    private static final String SCHOOL_ID = "00000000-0000-0000-0001-000000000001";
    private final String fileQuery = "/graphql/getChannelsByOrgUnitId.graphql";
    private final Map<String, Boolean> channelNames = new HashMap<>();

    @BeforeClass
    private void fillDBOrganizationChannels() {
        utilMethods.fillTableOrganizationChannels(1L, SCHOOL_ID, true);
        utilMethods.fillTableOrganizationChannels(2L, SCHOOL_ID, true);
        utilMethods.fillTableOrganizationChannels(3L, SCHOOL_ID, false);

        channelNames.put("UI", true);
        channelNames.put("EMAIL", true);
        channelNames.put("SMS", false);
    }

    @AfterClass
    private void cleanDB() {
        utilMethods.cleanTableOrganizationChannels(1L, new HashSet<>(Collections.singleton(SCHOOL_ID)));
        utilMethods.cleanTableOrganizationChannels(2L, new HashSet<>(Collections.singleton(SCHOOL_ID)));
        utilMethods.cleanTableOrganizationChannels(3L, new HashSet<>(Collections.singleton(SCHOOL_ID)));
    }

    private JSONArray getJsonSuccessResponse(final CloseableHttpResponse responseGraphQL) throws IOException {
        final JSONObject response = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        return response
                .getJSONObject("data")
                .getJSONObject("notificationConfiguration")
                .getJSONArray("getChannelsByOrgUnitId");
    }

    private JSONObject getJsonErrorResponse(final CloseableHttpResponse responseGraphQL) throws IOException {
        final JSONObject response = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        return response
                .getJSONArray("errors")
                .getJSONObject(0)
                .getJSONObject("extensions");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetChannelsByOrgUnitId")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда значения есть в базе и роль пользователя - student. ")
    @Owner(value = "Anastasiya Krutova")
    public void getChannelsByOrgUnitId_everythingExist_studentAuth() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("schoolId", SCHOOL_ID);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        fileQuery,
                        variables,
                        SCHOOL_ID
                );

        final JSONArray channels = getJsonSuccessResponse(responseGraphQL);

        final Map<String, Boolean> channelNamesFromResponse = new HashMap<>();
        channels.forEach(obj -> {
            final JSONObject jsonObj = (JSONObject) obj;
            channelNamesFromResponse.put(jsonObj.getString("channelName"), jsonObj.getBoolean("channelActive"));
        });

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(channelNamesFromResponse, channelNames);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetChannelsByOrgUnitId")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда schoolId не передается. ")
    @Owner(value = "Anastasiya Krutova")
    public void getChannelsByOrgUnitId_schoolIdNotTransmit_studentAuth() throws IOException {

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        fileQuery,
                        null,
                        SCHOOL_ID
                );

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), BAD_REQUEST);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetChannelsByOrgUnitId")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда schoolId = null. ")
    @Owner(value = "Anastasiya Krutova")
    public void getChannelsByOrgUnitId_schoolIdNull_studentAuth() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.putNull("schoolId");

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        fileQuery,
                        variables,
                        SCHOOL_ID
                );

        final JSONObject errors = getJsonErrorResponse(responseGraphQL);

        final String errorType = errors.getString("uiErrorCode");

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(errorType, "DEFAULT_ERROR_MESSAGE_CODE");
        Assert.assertTrue(errors
                .toString()
                .contains("Variable 'schoolId' has coerced Null value for NonNull type 'UUID!"));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetChannelsByOrgUnitId")
    @Story(value = "Проверка пустых значений")
    @Test(description = "Когда schoolId = \"\". ")
    @Owner(value = "Anastasiya Krutova")
    public void getChannelsByOrgUnitId_schoolIdEmpty_studentAuth() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("schoolId", "");

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        fileQuery,
                        variables,
                        SCHOOL_ID
                );

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), BAD_REQUEST);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetChannelsByOrgUnitId")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда значения есть в базе и роль пользователя - businessAdmin ")
    @Owner(value = "Anastasiya Krutova")
    public void getChannelsByOrgUnitId_everythingExist_businessAdminAuth() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("schoolId", SCHOOL_ID);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        businessLogin,
                        password,
                        fileQuery,
                        variables,
                        SCHOOL_ID
                );

        final JSONArray channels = getJsonSuccessResponse(responseGraphQL);

        final Map<String, Boolean> channelNamesFromResponse = new HashMap<>();
        channels.forEach(obj -> {
            final JSONObject jsonObj = (JSONObject) obj;
            channelNamesFromResponse.put(jsonObj.getString("channelName"), jsonObj.getBoolean("channelActive"));
        });

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(channelNamesFromResponse, channelNames);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetChannelsByOrgUnitId")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда значения есть в базе и роль пользователя - adminSys ")
    @Owner(value = "Anastasiya Krutova")
    public void getChannelsByOrgUnitId_everythingExist_adminSysAuth() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("schoolId", SCHOOL_ID);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        adminSysLogin,
                        adminSysPassword,
                        fileQuery,
                        variables,
                        null
                );

        final JSONArray channels = getJsonSuccessResponse(responseGraphQL);

        final Map<String, Boolean> channelNamesFromResponse = new HashMap<>();
        channels.forEach(obj -> {
            final JSONObject jsonObj = (JSONObject) obj;
            channelNamesFromResponse.put(jsonObj.getString("channelName"), jsonObj.getBoolean("channelActive"));
        });

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(channelNamesFromResponse, channelNames);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetChannelsByOrgUnitId")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда значения есть в базе и роль пользователя - teacher ")
    @Owner(value = "Anastasiya Krutova")
    public void getChannelsByOrgUnitId_everythingExist_teacherAuth() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("schoolId", SCHOOL_ID);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        teacherLogin,
                        password,
                        fileQuery,
                        variables,
                        SCHOOL_ID
                );

        final JSONArray channels = getJsonSuccessResponse(responseGraphQL);

        final Map<String, Boolean> channelNamesFromResponse = new HashMap<>();
        channels.forEach(obj -> {
            final JSONObject jsonObj = (JSONObject) obj;
            channelNamesFromResponse.put(jsonObj.getString("channelName"), jsonObj.getBoolean("channelActive"));
        });

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(channelNamesFromResponse, channelNames);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetChannelsByOrgUnitId")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда значения есть в базе и роль пользователя - parent ")
    @Owner(value = "Anastasiya Krutova")
    public void getChannelsByOrgUnitId_everythingExist_parentAuth() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("schoolId", SCHOOL_ID);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        parentLogin,
                        password,
                        fileQuery,
                        variables,
                        SCHOOL_ID
                );

        final JSONArray channels = getJsonSuccessResponse(responseGraphQL);

        final Map<String, Boolean> channelNamesFromResponse = new HashMap<>();
        channels.forEach(obj -> {
            final JSONObject jsonObj = (JSONObject) obj;
            channelNamesFromResponse.put(jsonObj.getString("channelName"), jsonObj.getBoolean("channelActive"));
        });

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(channelNamesFromResponse, channelNames);
    }
}
