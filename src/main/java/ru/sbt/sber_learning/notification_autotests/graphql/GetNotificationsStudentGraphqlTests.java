package ru.sbt.sber_learning.notification_autotests.graphql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.qameta.allure.*;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import ru.sbt.sber_learning.notification_autotests.GraphQLUtilMethods;
import ru.sbt.sber_learning.notification_autotests.UtilMethods;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

@Component
@SpringBootTest(classes = {GraphQLUtilMethods.class, UtilMethods.class})
public class GetNotificationsStudentGraphqlTests extends AbstractTestNGSpringContextTests {

    @Value("${autotests-users.student.login}")
    private String studentLogin;

    @Value("${autotests-users.password}")
    private String password;

    private static final String ORG_UNIT_ID = "00000000-0000-0000-0001-000000000001";

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private UtilMethods utilMethods;

    @Autowired
    private GraphQLUtilMethods graphQLUtilMethods;

    @AfterMethod
    private void cleanUp() {

        utilMethods.cleanDBNotifications(utilMethods.getSelectUserIdFromUsersByLogin(studentLogin), ORG_UNIT_ID);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsStudent")
    @Story(value = "Основная функциональность")
    @Test(description = "Проверка того, что у нового пользователя нет никаких уведомлений")
    @Owner(value = "KovalchukIS")
    public void getNotifications() throws IOException {

        final ObjectNode variables = objectMapper.createObjectNode();
        variables.putObject("paging")
                .put("offset", 0)
                .put("limit", 20);
        variables.put("wasReadIncluded", true);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        "/graphql/getNotificationsStudent.gql",
                        variables,
                        ORG_UNIT_ID
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        final Integer zeroValue = 0;
        final Integer notificationsCountFromDB = utilMethods.selectCountNotificationsFromUser(UUID.fromString(utilMethods.getSelectUserIdFromUsersByLogin(studentLogin)));
        Allure.step("У пользователя " + studentLogin + ", созданного тестовым приложением, не должно быть никаких уведомлений");
        Assert.assertEquals(notificationsCountFromDB, zeroValue);

        final Integer notificationsCountFromResponse  = responseGraphQLJson.getJSONObject("data").getJSONObject("student").getInt("getNotificationsCount");

        Assert.assertEquals(notificationsCountFromResponse, notificationsCountFromDB);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsStudent")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда есть случайное кол-во непрочитанных и прочитанных уведомлений")
    @Owner(value = "KovalchukIS")
    public void getNotificationsShouldGetAll() throws IOException {

        //Создастся случайное count в интервале [10; 20]
        int count = ThreadLocalRandom.current().nextInt(10  , 21);

        final String studentId = utilMethods.getSelectUserIdFromUsersByLogin(studentLogin);

        final ArrayList<Long> notificationIds = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            notificationIds.add(i, utilMethods.fillDBNotificationsMass(studentId, ORG_UNIT_ID, (i % 2 == 0), false));
        }

        final ObjectNode variables = objectMapper.createObjectNode();
        variables.putObject("paging")
                .put("offset", 0)
                .put("limit", 20);
        variables.put("wasReadIncluded", true);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        "/graphql/getNotificationsStudent.gql",
                        variables,
                        ORG_UNIT_ID
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        final JSONArray notificationsFromResponse = responseGraphQLJson.getJSONObject("data").getJSONObject("student").getJSONArray("getNotifications");

        final ArrayList<Long> notificationIdsFromResponse = new ArrayList<>();

        for (int i = 0; i < notificationsFromResponse.length(); i++) {
        notificationIdsFromResponse.add(notificationsFromResponse.getJSONObject(i).getLong("id"));
        }

        Collections.sort(notificationIds);
        Collections.sort(notificationIdsFromResponse);

        Assert.assertEquals(notificationIdsFromResponse, notificationIds);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsStudent")
    @Story(value = "Основная функциональность")
    @Test(description = "Проверка смещения")
    @Owner(value = "KovalchukIS")
    public void getNotificationsOffset5() throws IOException {

        final int offset = 5;

        //Создастся случайное count в интервале [10; 20]
        int count = ThreadLocalRandom.current().nextInt(10  , 21);

        final String studentId = utilMethods.getSelectUserIdFromUsersByLogin(studentLogin);

        final ArrayList<Long> notificationIds = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            notificationIds.add(i, utilMethods.fillDBNotificationsMass(studentId, ORG_UNIT_ID, (i % 2 == 0), false));
        }

        final ObjectNode variables = objectMapper.createObjectNode();
        variables.putObject("paging")
                .put("offset", offset)
                .put("limit", 20);
        variables.put("wasReadIncluded", true);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        "/graphql/getNotificationsStudent.gql",
                        variables,
                        ORG_UNIT_ID
                );

        responseGraphQL.getAllHeaders();
        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        final JSONArray notificationsFromResponse = responseGraphQLJson.getJSONObject("data").getJSONObject("student").getJSONArray("getNotifications");

        final ArrayList<Long> notificationIdsFromResponse = new ArrayList<>();

        for (int i = 0; i < notificationsFromResponse.length(); i++) {
            notificationIdsFromResponse.add(notificationsFromResponse.getJSONObject(i).getLong("id"));
        }

        Collections.sort(notificationIds);
        Collections.sort(notificationIdsFromResponse);

        final ArrayList<Long> notificationIdsWithOffset = new ArrayList<>();

        for (int i = 0; i < notificationIds.size() - offset; i++) {
           notificationIdsWithOffset.add(i, notificationIds.get(i));
        }

        Collections.sort(notificationIdsWithOffset);

        Assert.assertEquals(notificationIdsFromResponse, notificationIdsWithOffset);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsStudent")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда смещение явно больше количества уведомлений и должно вернуться 0 уведомлений")
    @Owner(value = "KovalchukIS")
    public void getNotificationsOffset20() throws IOException {

        final int offset = 20;

        //Создастся случайное count в интервале [10; 20]
        int count = ThreadLocalRandom.current().nextInt(10  , 21);

        final Long[] notificationIds = new Long[count];
        final String studentId = utilMethods.getSelectUserIdFromUsersByLogin(studentLogin);

        for (int i = 0; i < count; i++) {
            notificationIds[i] = utilMethods.fillDBNotificationsMass(studentId, ORG_UNIT_ID, (i % 2 == 0), false);
        }

        final ObjectNode variables = objectMapper.createObjectNode();
        variables.putObject("paging")
                .put("offset", offset)
                .put("limit", 20);
        variables.put("wasReadIncluded", true);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        "/graphql/getNotificationsStudent.gql",
                        variables,
                        ORG_UNIT_ID
                );

        responseGraphQL.getAllHeaders();
        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        final JSONArray notificationsFromResponse = responseGraphQLJson.getJSONObject("data").getJSONObject("student").getJSONArray("getNotifications");
        final int contNotificationsFromResponse = notificationsFromResponse.length();

        Assert.assertEquals(contNotificationsFromResponse, 0);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsStudent")
    @Story(value = "Основная функциональность")
    @Test(description = "Проверка, что количество возвращаемых уведомлений не больше лимита")
    @Owner(value = "KovalchukIS")
    public void getNotificationsLimit3() throws IOException {

        final int limit = 3;

        //Создастся случайное count в интервале [10; 20]
        int count = ThreadLocalRandom.current().nextInt(10  , 21);

        final Long[] notificationIds = new Long[count];
        final String studentId = utilMethods.getSelectUserIdFromUsersByLogin(studentLogin);

        for (int i = 0; i < count; i++) {
            notificationIds[i] = utilMethods.fillDBNotificationsMass(studentId, ORG_UNIT_ID, (i % 2 == 0), false);
        }

        final ObjectNode variables = objectMapper.createObjectNode();
        variables.putObject("paging")
                .put("offset", 0)
                .put("limit", limit);
        variables.put("wasReadIncluded", true);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        "/graphql/getNotificationsStudent.gql",
                        variables,
                        ORG_UNIT_ID
                );

        responseGraphQL.getAllHeaders();
        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        final JSONArray notificationsFromResponse = responseGraphQLJson.getJSONObject("data").getJSONObject("student").getJSONArray("getNotifications");
        final int contNotificationsFromResponse = notificationsFromResponse.length();

        Assert.assertEquals(contNotificationsFromResponse, limit);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsStudent")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда есть случайное кол-во прочитанных и одно непрочитанное уведомление")
    @Owner(value = "KovalchukIS")
    public void getNotificationsShouldGetAllUnread() throws IOException {

        //Создастся случайное count в интервале [10; 20]
        final int count = ThreadLocalRandom.current().nextInt(10  , 21);

        final ArrayList<Long> notificationIds = new ArrayList<>(count);
        final String studentId = utilMethods.getSelectUserIdFromUsersByLogin(studentLogin);

        for (int i = 0; i < count - 1; i++) {
            notificationIds.add(i,utilMethods.fillDBNotificationsMass(studentId, ORG_UNIT_ID, false, false));
        }

        //Специально добавлено одно прочитанное уведомление
        notificationIds.add(10, utilMethods.fillDBNotificationsMass(studentId, ORG_UNIT_ID, true, false));

        final ObjectNode variables = objectMapper.createObjectNode();
        variables.putObject("paging")
                .put("offset", 0)
                .put("limit", 20);
        variables.put("wasReadIncluded", false);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        "/graphql/getNotificationsStudent.gql",
                        variables,
                        ORG_UNIT_ID
                );

        responseGraphQL.getAllHeaders();
        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        final JSONArray notificationsFromResponse = responseGraphQLJson.getJSONObject("data").getJSONObject("student").getJSONArray("getNotifications");

        final ArrayList<Long> notificationIdsFromResponse = new ArrayList<>(count - 1);

        for (int i = 0; i < notificationsFromResponse.length(); i++) {
            notificationIdsFromResponse.add(i, notificationsFromResponse.getJSONObject(i).getLong("id"));
        }

        final ArrayList<Long> notificationIdsWithoutRead = notificationIds;
        notificationIdsWithoutRead.remove(10);
        Collections.sort(notificationIds);
        Collections.sort(notificationIdsFromResponse);

        Assert.assertEquals(notificationIdsFromResponse, notificationIdsWithoutRead);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsStudent")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда offset = null ")
    @Owner(value = "KovalchukIS")
    public void getNotificationsWithNullOffset() throws IOException {

        final ObjectNode variables = objectMapper.createObjectNode();
        variables.putObject("paging")
                .put("offset", (Integer) null)
                .put("limit", 20);
        variables.put("wasReadIncluded", true);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        "/graphql/getNotificationsStudent.gql",
                        variables,
                        ORG_UNIT_ID
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        Assert.assertTrue(responseGraphQLJson.getJSONArray("errors").toString().contains("Variable 'paging' has coerced Null value for NonNull type 'Int!'"));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsStudent")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда limit = null")
    @Owner(value = "KovalchukIS")
    public void getNotificationsWithNullLimit() throws IOException {

        final ObjectNode variables = objectMapper.createObjectNode();
        variables.putObject("paging")
                .put("offset", 0)
                .put("limit", (Integer) null);
        variables.put("wasReadIncluded", true);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        "/graphql/getNotificationsStudent.gql",
                        variables,
                        ORG_UNIT_ID
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        Assert.assertTrue(responseGraphQLJson.getJSONArray("errors").toString().contains("Variable 'paging' has coerced Null value for NonNull type 'Int!'"));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsStudent")
    @Story(value = "Проверка пустых значений")
    @Test(description = "Когда offset = \"\"")
    @Owner(value = "KovalchukIS")
    public void getNotificationsWithEmptyOffset() throws IOException {

        final ObjectNode variables = objectMapper.createObjectNode();
        variables.putObject("paging")
                .put("offset", "")
                .put("limit", 20);
        variables.put("wasReadIncluded", true);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        "/graphql/getNotificationsStudent.gql",
                        variables,
                        ORG_UNIT_ID
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        Assert.assertTrue(responseGraphQLJson.getJSONArray("errors").toString().contains("Variable 'offset' has an invalid value"));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsStudent")
    @Story(value = "Проверка пустых значений")
    @Test(description = "Когда limit = \"\"")
    @Owner(value = "KovalchukIS")
    public void getNotificationsWithEmptyLimit() throws IOException {

        final ObjectNode variables = objectMapper.createObjectNode();
        variables.putObject("paging")
                .put("offset", 0)
                .put("limit", "");
        variables.put("wasReadIncluded", true);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        "/graphql/getNotificationsStudent.gql",
                        variables,
                        ORG_UNIT_ID
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        Assert.assertTrue(responseGraphQLJson.getJSONArray("errors").toString().contains("Variable 'limit' has an invalid value"));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsStudent")
    @Story(value = "Проверка некорректных значений")
    @Test(description = "Когда offset = \"incorrectValue\"")
    @Owner(value = "KovalchukIS")
    public void getNotificationsWithIncorrectOffset() throws IOException {

        final ObjectNode variables = objectMapper.createObjectNode();
        variables.putObject("paging")
                .put("offset", "incorrectValue")
                .put("limit", 20);
        variables.put("wasReadIncluded", true);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        "/graphql/getNotificationsStudent.gql",
                        variables,
                        ORG_UNIT_ID
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        Assert.assertTrue(responseGraphQLJson.getJSONArray("errors").toString().contains("Variable 'offset' has an invalid value"));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsStudent")
    @Story(value = "Проверка некорректных значений")
    @Test(description = "Когда limit = \"incorrectValue\"")
    @Owner(value = "KovalchukIS")
    public void getNotificationsWithIncorrectLimit() throws IOException {

        final ObjectNode variables = objectMapper.createObjectNode();
        variables.putObject("paging")
                .put("offset", 0)
                .put("limit", "incorrectValue");
        variables.put("wasReadIncluded", true);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        "/graphql/getNotificationsStudent.gql",
                        variables,
                        ORG_UNIT_ID
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        Assert.assertTrue(responseGraphQLJson.getJSONArray("errors").toString().contains("Variable 'limit' has an invalid value"));
    }
}