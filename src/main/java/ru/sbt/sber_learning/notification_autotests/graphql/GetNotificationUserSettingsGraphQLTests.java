package ru.sbt.sber_learning.notification_autotests.graphql;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.sbt.sber_learning.notification_autotests.GraphQLUtilMethods;
import ru.sbt.sber_learning.notification_autotests.UtilMethods;

import java.io.IOException;

@Component
@SpringBootTest(classes = {GraphQLUtilMethods.class, UtilMethods.class})
public class GetNotificationUserSettingsGraphQLTests extends AbstractTestNGSpringContextTests {

    @Value("${autotests-users.student.login}")
    private String studentLogin;

    @Value("${autotests-users.password}")
    private String password;

    @Value("${autotests-users.teacher.login}")
    private String teacherLogin;

    @Autowired
    private GraphQLUtilMethods graphQLUtilMethods;

    @Autowired
    private UtilMethods utilMethods;

    private static final int STATUS_OK = 200;
    private static final String SCHOOL_ID = "00000000-0000-0000-0001-000000000001";
    private static final String STUDENT_ROLE = "STUDENT";
    private static final String TEACHER_ROLE = "TEACHER";
    private static final String FILE_GRAPHQL = "/graphql/getNotificationUserSettings.gql";

    private Integer getJsonSuccessResponse(final CloseableHttpResponse responseGraphQL) throws IOException {
        final JSONObject response = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        return response
                .getJSONObject("data")
                .getJSONObject("notificationConfiguration")
                .getJSONArray("getNotificationUserSettings")
                .length();
    }

    private String getJsonErrorResponse(final CloseableHttpResponse responseGraphQL) throws IOException {
        final JSONObject json = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        return json
                .getJSONArray("errors")
                .getJSONObject(0)
                .getJSONObject("extensions")
                .getString("uiErrorCode");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationUserSettings")
    @Story(value = "Основная функциональность")
    @Test(description = "Проверка, что для роли Student количество User Settings в ответе совпадает с количеством в БД. ")
    @Owner(value = "Anastasiya Krutova")
    public void getNotificationUserSettings_shouldReturnCountFromDB_studentAuth() throws IOException {
        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        FILE_GRAPHQL,
                        null,
                        SCHOOL_ID
                );

        final Integer countOfUserSettingsInResponse = getJsonSuccessResponse(responseGraphQL);
        final Integer countOfUserSettingsInDB = utilMethods.selectCountNotificationTypesByRole(STUDENT_ROLE);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(countOfUserSettingsInResponse, countOfUserSettingsInDB);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationUserSettings")
    @Story(value = "Основная функциональность")
    @Test(description = "Проверка, что для роли Teacher количество User Settings в ответе совпадает с количеством в БД. ")
    @Owner(value = "Anastasiya Krutova")
    public void getNotificationUserSettings_shouldReturnCountFromDB_teacherAuth() throws IOException {
        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        teacherLogin,
                        password,
                        FILE_GRAPHQL,
                        null,
                        SCHOOL_ID
                );

        final Integer countOfUserSettingsInResponse = getJsonSuccessResponse(responseGraphQL);
        final Integer countOfUserSettingsInDB = utilMethods.selectCountNotificationTypesByRole(TEACHER_ROLE);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(countOfUserSettingsInResponse, countOfUserSettingsInDB);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationUserSettings")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда SchoolId = школе, к которой данный userId не привязан. " +
                        "Должен вернуть пустой список User Settings. ")
    @Owner(value = "Anastasiya Krutova")
    public void getNotificationUserSettings_shouldReturnEmptyUserSettings_WhenSchoolAnother() throws IOException {
        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        FILE_GRAPHQL,
                        null,
                        "312ffa69-d1a9-492e-825c-949114487d88"
                );

        final String uiErrorCode = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(uiErrorCode, "SECURITY_ACCESS_DENIED");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationUserSettings")
    @Story(value = "Проверка пустых значений")
    @Test(description = "Когда SchoolId =\"\" . Должен вернуть Error. ")
    @Owner(value = "Anastasiya Krutova")
    public void getNotificationUserSettings_shouldReturnError_WhenSchoolEmpty() throws IOException {
        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        FILE_GRAPHQL,
                        null,
                        ""
                );

        final String uiErrorCode = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(uiErrorCode, "SECURITY_ACCESS_DENIED");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationUserSettings")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда SchoolId = null. Должен вернуть Error. ")
    @Owner(value = "Anastasiya Krutova")
    public void getNotificationUserSettings_shouldReturnError_WhenSchoolNull() throws IOException {
        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        FILE_GRAPHQL,
                        null,
                        null
                );

        final String uiErrorCode = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(uiErrorCode, "SECURITY_ACCESS_DENIED");
    }
}
