package ru.sbt.sber_learning.notification_autotests.graphql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import ru.sbt.sber_learning.notification_autotests.GraphQLUtilMethods;
import ru.sbt.sber_learning.notification_autotests.UtilMethods;

import java.io.IOException;

@Component
@SpringBootTest(classes = {GraphQLUtilMethods.class, UtilMethods.class})
public class GetNotificationsCountTeacherGraphQLTests extends AbstractTestNGSpringContextTests {

    @Value("${autotests-users.teacher.login}")
    private String teacherLogin;

    @Value("${autotests-users.student.login}")
    private String studentLogin;

    @Value("${autotests-users.password}")
    private String password;

    @Autowired
    private GraphQLUtilMethods graphQLUtilMethods;

    @Autowired
    private UtilMethods utilMethods;

    private static final int STATUS_OK = 200;

    private static final String STUDENT_FROM_ID = "00000000-0000-0000-0001-300000000006";
    private static final String SCHOOL_ID = "00000000-0000-0000-0001-000000000001";
    
    @AfterMethod
    private void cleanup() {
        utilMethods.cleanDBNotifications(utilMethods.getSelectUserIdFromUsersByLogin(studentLogin), SCHOOL_ID);
        utilMethods.cleanDBNotifications(STUDENT_FROM_ID, SCHOOL_ID);
        utilMethods.cleanDBNotifications(utilMethods.getSelectUserIdFromUsersByLogin(teacherLogin), SCHOOL_ID);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsCountTeacher")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда wasReadIncluded = true. ")
    @Owner(value = "Max Kamaev")
    public void getNotificationsCountTeacher_whenWasReadIncludedTrue_shouldReturnCountFromDB() throws IOException {
        final String teacherId = utilMethods.getSelectUserIdFromUsersByLogin(teacherLogin);
        
        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("wasReadIncluded", true);

        utilMethods.fillDBNotificationsMass(10, teacherId, SCHOOL_ID, false, false);
        utilMethods.fillDBNotificationsMass(5, teacherId, SCHOOL_ID, true, false);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        teacherLogin,
                        password,
                        "/graphql/getNotificationsCountTeacher.gql",
                        variables,
                        SCHOOL_ID
                );

        final JSONObject json = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final int notificationsCount = json
                .getJSONObject("data")
                .getJSONObject("teacher")
                .getInt("getNotificationsCount");
        final int countAllOfNotifications =
                utilMethods.selectCountNotifications(teacherId, SCHOOL_ID, false)
                        + utilMethods.selectCountNotifications(teacherId, SCHOOL_ID, true);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(notificationsCount, countAllOfNotifications);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsCountTeacher")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда wasReadIncluded = false. ")
    @Owner(value = "Max Kamaev")
    public void getNotificationsCountTeacher_whenWasReadIncludedFalse_shouldReturnCountFromDB() throws IOException {
        final String teacherId = utilMethods.getSelectUserIdFromUsersByLogin(teacherLogin);

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("wasReadIncluded", false);

        utilMethods.fillDBNotificationsMass(10, teacherId, SCHOOL_ID, false, false);
        utilMethods.fillDBNotificationsMass(5, teacherId, SCHOOL_ID, true, false);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        teacherLogin,
                        password,
                        "/graphql/getNotificationsCountTeacher.gql",
                        variables,
                        SCHOOL_ID
                );

        final JSONObject json = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final int notificationsCount = json
                .getJSONObject("data")
                .getJSONObject("teacher")
                .getInt("getNotificationsCount");
        final int countOfNotifications = utilMethods.selectCountNotifications(teacherId, SCHOOL_ID, false);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(notificationsCount, countOfNotifications);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsCountTeacher")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда wasReadIncluded не передается и количество нотификаций = 0. ")
    @Owner(value = "Max Kamaev")
    public void getNotificationsCountTeacher_shouldReturnCountFromDB() throws IOException {
        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        teacherLogin,
                        password,
                        "/graphql/getNotificationsCountTeacherWithoutParams.gql",
                        null,
                        SCHOOL_ID
                );

        final JSONObject json = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final int notificationsCount = json
                .getJSONObject("data")
                .getJSONObject("teacher")
                .getInt("getNotificationsCount");

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(notificationsCount, 0);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsCountTeacher")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда wasReadIncluded не передается, в БД присутствуют прочитанные и непрочитанные нотификации. ")
    @Owner(value = "Max Kamaev")
    public void getNotificationsCountTeacher_shouldReturnCountFromDB_DefaultWasReadIsFalse() throws IOException {
        final String teacherId = utilMethods.getSelectUserIdFromUsersByLogin(teacherLogin);

        utilMethods.fillDBNotificationsMass(10, teacherId, SCHOOL_ID, false, false);
        utilMethods.fillDBNotificationsMass(5, teacherId, SCHOOL_ID, true, false);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        teacherLogin,
                        password,
                        "/graphql/getNotificationsCountTeacherWithoutParams.gql",
                        null,
                        SCHOOL_ID
                );

        final JSONObject json = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final int notificationsCount = json
                .getJSONObject("data")
                .getJSONObject("teacher")
                .getInt("getNotificationsCount");
        final int countOfNotifications = utilMethods.selectCountNotifications(teacherId, SCHOOL_ID, false);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(notificationsCount, countOfNotifications);
    }


    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsCountTeacher")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда в базе есть уведомления, в которых userId - отправитель нотификаций. ")
    @Owner(value = "Max Kamaev")
    public void getNotificationsCountTeacher_shouldNotCountOwnNotifications() throws IOException {
        final String teacherId = utilMethods.getSelectUserIdFromUsersByLogin(teacherLogin);

        utilMethods.fillDataBaseWithUserFromId(10, STUDENT_FROM_ID, teacherId, SCHOOL_ID, false, false);
        utilMethods.fillDBNotificationsMass(5, teacherId, SCHOOL_ID, false, false);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        teacherLogin,
                        password,
                        "/graphql/getNotificationsCountTeacherWithoutParams.gql",
                        null,
                        SCHOOL_ID
                );

        final JSONObject json = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final int notificationsCount = json
                .getJSONObject("data")
                .getJSONObject("teacher")
                .getInt("getNotificationsCount");
        final int countOfNotifications = utilMethods.selectCountNotifications(teacherId, SCHOOL_ID, false);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(notificationsCount, countOfNotifications);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsCountTeacher")
    @Story(value = "Проверка некорректных значений")
    @Test(description = "Когда SchoolId = школе, к которой данный userId не привязан. Должен вернуть Error - Security access denied. ")
    @Owner(value = "Max Kamaev")
    public void getNotificationsCountTeacher_shouldReturnSecurityAccessDenied_WhenSchoolAnother() throws IOException {

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        teacherLogin,
                        password,
                        "/graphql/getNotificationsCountTeacherWithoutParams.gql",
                        null,
                        "312ffa69-d1a8-482e-825c-949114487d88"
                );

        final JSONObject json = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final String errorType = json
                .getJSONArray("errors")
                .getJSONObject(0)
                .getJSONObject("extensions")
                .getString("uiErrorCode");

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(errorType, "SECURITY_ACCESS_DENIED");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsCountTeacher")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда SchoolId = null. Должен вернуть Error - Security access denied. ")
    @Owner(value = "Max Kamaev")
    public void getNotificationsCountTeacher_shouldReturnSecurityAccessDenied_WhenSchoolIsNull() throws IOException {

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        teacherLogin,
                        password,
                        "/graphql/getNotificationsCountTeacherWithoutParams.gql",
                        null,
                        null
                );

        final JSONObject json = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final String errorType = json
                .getJSONArray("errors")
                .getJSONObject(0)
                .getJSONObject("extensions")
                .getString("uiErrorCode");

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(errorType, "SECURITY_ACCESS_DENIED");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationsCountTeacher")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда wasReadIncluded = true и роль пользователя - student. ")
    @Owner(value = "Max Kamaev")
    public void getNotificationsCountTeacher_whenWasReadIncludedTrue_shouldReturnCountFromDB_studentAuth() throws IOException {
        final String studentId = utilMethods.getSelectUserIdFromUsersByLogin(studentLogin);

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("wasReadIncluded", true);

        utilMethods.fillDBNotificationsMass(10, studentId, SCHOOL_ID, false, false);
        utilMethods.fillDBNotificationsMass(5, studentId, SCHOOL_ID, true, false);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        "/graphql/getNotificationsCountTeacher.gql",
                        variables,
                        SCHOOL_ID
                );

        final JSONObject json = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final int notificationsCount = json
                .getJSONObject("data")
                .getJSONObject("teacher")
                .getInt("getNotificationsCount");
        final int countAllOfNotifications =
                utilMethods.selectCountNotifications(studentId, SCHOOL_ID, false)
                        + utilMethods.selectCountNotifications(studentId, SCHOOL_ID, true);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(notificationsCount, countAllOfNotifications);
    }
}


