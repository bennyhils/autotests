package ru.sbt.sber_learning.notification_autotests.graphql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;
import ru.sbt.sber_learning.notification_autotests.GraphQLUtilMethods;
import ru.sbt.sber_learning.notification_autotests.UtilMethods;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Component
@SpringBootTest(classes = {GraphQLUtilMethods.class, UtilMethods.class})
public class GetSystemNotificationsGraphQLTests extends AbstractTestNGSpringContextTests {

    @Value("${autotests-users.sys.login}")
    private String adminSysLogin;

    @Value("${autotests-users.old-password}")
    private String adminSysPassword;

    @Value("${autotests-users.business.login}")
    private String businessLogin;

    @Value("${autotests-users.teacher.login}")
    private String teacherLogin;

    @Value("${autotests-users.parent.login}")
    private String parentLogin;

    @Value("${autotests-users.student.login}")
    private String studentLogin;

    @Value("${autotests-users.studentFromAnotherSchool.login}")
    private String studentFromAnotherSchoolLogin;

    @Value("${autotests-users.password}")
    private String password;

    @Autowired
    private GraphQLUtilMethods graphQLUtilMethods;

    @Autowired
    private UtilMethods utilMethods;

    private final ObjectMapper objectMapper = new ObjectMapper();

    private static final int STATUS_OK = 200;
    private static final int BAD_REQUEST = 400;
    private static final String SCHOOL_ID = "00000000-0000-0000-0001-000000000001";
    private static final String ANOTHER_SCHOOL_ID = "d6b30c74-8a92-4367-a94e-9566a51af77b";
    private static final String FILE_QUERY = "/graphql/getSystemNotification.graphql";
    private static Long NOTIFICATION_ID_FROM_ADMIN_SYS;
    private static Long NOTIFICATION_ID_FROM_ADMIN_BUSINESS;

    @BeforeClass
    public void fillDBSystemNotifications() {
        NOTIFICATION_ID_FROM_ADMIN_SYS = utilMethods.fillTableSystemNotifications(
                null,
                "testHeader",
                "testText",
                LocalDateTime.now(ZoneOffset.UTC).toString(),
                LocalDateTime.now(ZoneOffset.UTC).plusMinutes(1).toString(),
                utilMethods.getSelectUserIdFromUsersByLogin(adminSysLogin)
        );

        NOTIFICATION_ID_FROM_ADMIN_BUSINESS = utilMethods.fillTableSystemNotifications(
                SCHOOL_ID,
                "testHeader",
                "testText",
                LocalDateTime.now(ZoneOffset.UTC).toString(),
                LocalDateTime.now(ZoneOffset.UTC).plusMinutes(1).toString(),
                utilMethods.getSelectUserIdFromUsersByLogin(businessLogin)
        );
    }

    @AfterClass
    public void cleanDBSystemNotifications() {
        utilMethods.cleanTableSystemNotificationsByHeaderAndText("testHeader", "testText");
    }

    private String getJsonSuccessResponse(final CloseableHttpResponse responseGraphQL) throws IOException {
        final JSONObject response = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        return response
                .getJSONObject("data")
                .getJSONObject("systemAdmin")
                .getJSONObject("getSystemNotification").get("id").toString();
    }

    private String getJsonErrorResponse(final CloseableHttpResponse responseGraphQL) throws IOException {
        final JSONObject response = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        return response
                .getJSONArray("errors")
                .getJSONObject(0)
                .getJSONObject("extensions")
                .getString("uiErrorCode");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetSystemNotifications")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда значения есть в базе и роль пользователя - student. ")
    @Owner(value = "Anastasiya Krutova")
    public void getSystemNotification_everythingExist_studentAuthTest() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("id", NOTIFICATION_ID_FROM_ADMIN_SYS);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        FILE_QUERY,
                        variables,
                        SCHOOL_ID
                );

        final Long notificationId = Long.valueOf(getJsonSuccessResponse(responseGraphQL));

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(notificationId, NOTIFICATION_ID_FROM_ADMIN_SYS);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetSystemNotifications")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда уведомление отправил бизнес-админ, и его пытается прочесть пользователь этой школы. ")
    @Owner(value = "Anastasiya Krutova")
    public void getSystemNotification_notificationFromAdminBusinessTest() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("id", NOTIFICATION_ID_FROM_ADMIN_BUSINESS);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        FILE_QUERY,
                        variables,
                        SCHOOL_ID
                );

        final Long notificationId = Long.valueOf(getJsonSuccessResponse(responseGraphQL));

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(notificationId, NOTIFICATION_ID_FROM_ADMIN_BUSINESS);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetSystemNotifications")
    @Story(value = "Проверка некорректных значений")
    @Test(description = "Когда SchoolId = школе, к которой данный userId не привязан. " +
                        "Должен вернуть Error - Security access denied. ")
    @Owner(value = "Anastasiya Krutova")
    public void getSystemNotification_schoolIdAnotherTest() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("id", NOTIFICATION_ID_FROM_ADMIN_SYS);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        FILE_QUERY,
                        variables,
                        ANOTHER_SCHOOL_ID
                );

        final String response = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(response, "SECURITY_ACCESS_DENIED");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetSystemNotifications")
    @Story(value = "Проверка некорректных значений")
    @Test(description = "Попытка получить уведомление, id которого нет в базе. Должен вернуть Error. ")
    @Owner(value = "Anastasiya Krutova")
    public void getSystemNotification_idNotExistTest() throws IOException {

        final int nonexistentId = 999;
        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("id", nonexistentId);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        FILE_QUERY,
                        variables,
                        SCHOOL_ID
                );

        final String response = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(response, "DEFAULT_ERROR_MESSAGE_CODE");
    }

    @Ignore("После решения задачи EDU-14261 должен вернуть Error")
    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetSystemNotifications")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда нотификацию отправил бизнес-админ одной школы, а получить ее пытается ученик другой школы. ")
    @Owner(value = "Anastasiya Krutova")
    public void getSystemNotification_anotherSchoolAndStudentTest() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("id", NOTIFICATION_ID_FROM_ADMIN_BUSINESS);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentFromAnotherSchoolLogin,
                        password,
                        FILE_QUERY,
                        variables,
                        ANOTHER_SCHOOL_ID
                );

        final String response = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(response, "SECURITY_ACCESS_DENIED");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetSystemNotifications")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда schoolId = null. Должен вернуть Error - Security access denied.")
    @Owner(value = "Anastasiya Krutova")
    public void getSystemNotification_schoolIdNullTest() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("id", NOTIFICATION_ID_FROM_ADMIN_SYS);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        FILE_QUERY,
                        variables,
                        null
                );

        final String response = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(response, "SECURITY_ACCESS_DENIED");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetSystemNotifications")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда id = null. Должен вернуть Error.")
    @Owner(value = "Anastasiya Krutova")
    public void getSystemNotification_idNullTest() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.putNull("id");

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        FILE_QUERY,
                        variables,
                        SCHOOL_ID
                );

        final String response = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(response, "DEFAULT_ERROR_MESSAGE_CODE");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetSystemNotifications")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда id не передается. Должен вернуться Bad Request. ")
    @Owner(value = "Anastasiya Krutova")
    public void getSystemNotification_idNotTransmitTest() throws IOException {

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        FILE_QUERY,
                        null,
                        SCHOOL_ID
                );

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), BAD_REQUEST);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetSystemNotifications")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда значения есть в базе и роль пользователя - teacher. ")
    @Owner(value = "Anastasiya Krutova")
    public void getSystemNotification_everythingExist_teacherAuthTest() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("id", NOTIFICATION_ID_FROM_ADMIN_SYS);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        teacherLogin,
                        password,
                        FILE_QUERY,
                        variables,
                        SCHOOL_ID
                );

        final Long notificationId = Long.valueOf(getJsonSuccessResponse(responseGraphQL));

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(notificationId, NOTIFICATION_ID_FROM_ADMIN_SYS);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetSystemNotifications")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда значения есть в базе и роль пользователя - parent. ")
    @Owner(value = "Anastasiya Krutova")
    public void getSystemNotification_everythingExist_parentAuthTest() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("id", NOTIFICATION_ID_FROM_ADMIN_SYS);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        parentLogin,
                        password,
                        FILE_QUERY,
                        variables,
                        SCHOOL_ID
                );

        final Long notificationId = Long.valueOf(getJsonSuccessResponse(responseGraphQL));

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(notificationId, NOTIFICATION_ID_FROM_ADMIN_SYS);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetSystemNotifications")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда значения есть в базе и роль пользователя - adminBusiness. ")
    @Owner(value = "Anastasiya Krutova")
    public void getSystemNotification_everythingExist_adminBusinessAuthTest() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("id", NOTIFICATION_ID_FROM_ADMIN_SYS);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        businessLogin,
                        password,
                        FILE_QUERY,
                        variables,
                        SCHOOL_ID
                );

        final Long notificationId = Long.valueOf(getJsonSuccessResponse(responseGraphQL));

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(notificationId, NOTIFICATION_ID_FROM_ADMIN_SYS);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetSystemNotifications")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда значения есть в базе и роль пользователя - adminSys. ")
    @Owner(value = "Anastasiya Krutova")
    public void getSystemNotification_everythingExist_adminSysAuthTest() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("id", NOTIFICATION_ID_FROM_ADMIN_SYS);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        adminSysLogin,
                        adminSysPassword,
                        FILE_QUERY,
                        variables,
                        null
                );

        final Long notificationId = Long.valueOf(getJsonSuccessResponse(responseGraphQL));

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(notificationId, NOTIFICATION_ID_FROM_ADMIN_SYS);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetSystemNotifications")
    @Story(value = "Основная функциональность")
    @Test(description = "getSystemNotificationsCountWithoutCommon")
    @Owner(value = "KovalchukIS")
    public void getSystemNotificationsCountWithoutCommonTest() throws IOException {

        final ObjectNode variables = objectMapper.createObjectNode();
        variables.putArray("sortingFields").addObject()
                .put("name", "notificationDisplayStart")
                .put("asc", true);
        variables.putObject("paging")
                .put("offset", 0)
                .put("limit", 20);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        adminSysLogin,
                        adminSysPassword,
                        "/graphql/getSystemNotificationsCountWithoutCommon.graphql",
                        variables,
                        null
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final int count = responseGraphQLJson
                .getJSONObject("data")
                .getJSONObject("systemAdmin")
                .getInt("getSystemNotificationsCountWithoutCommon");

        Assert.assertEquals(count, 1);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetSystemNotifications")
    @Story(value = "Основная функциональность")
    @Test(description = "getSystemNotificationsCountWithCommon")
    @Owner(value = "KovalchukIS")
    public void getSystemNotificationsCountWithCommonTest() throws IOException {

        final ObjectNode variables = objectMapper.createObjectNode();
        variables.putArray("sortingFields").addObject()
                .put("name", "notificationDisplayStart")
                .put("asc", true);
        variables.putObject("paging")
                .put("offset", 0)
                .put("limit", 20);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        adminSysLogin,
                        adminSysPassword,
                        "/graphql/getSystemNotificationsCountWithCommon.graphql",
                        variables,
                        null
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final int count = responseGraphQLJson
                .getJSONObject("data")
                .getJSONObject("systemAdmin")
                .getInt("getSystemNotificationsCountWithCommon");

        Assert.assertEquals(count, 1);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetSystemNotifications")
    @Story(value = "Основная функциональность")
    @Test(description = "getSystemNotificationsWithCommon")
    @Owner(value = "KovalchukIS")
    public void getSystemNotificationsWithCommonTest() throws IOException {

        final ObjectNode variables = objectMapper.createObjectNode();
        variables.putArray("sortingFields").addObject()
                .put("name", "notificationDisplayStart")
                .put("asc", true);
        variables.putObject("paging")
                .put("offset", 0)
                .put("limit", 20);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        adminSysLogin,
                        adminSysPassword,
                        "/graphql/getSystemNotificationsWithCommon.graphql",
                        variables,
                        null
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final Long idFromResponse = responseGraphQLJson
                .getJSONObject("data")
                .getJSONObject("systemAdmin")
                .getJSONArray("getSystemNotificationsWithCommon")
                .getJSONObject(0)
                .getLong("id");

        Assert.assertEquals(idFromResponse, NOTIFICATION_ID_FROM_ADMIN_SYS);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetSystemNotifications")
    @Story(value = "Основная функциональность")
    @Test(description = "getSystemNotificationsWithCommon")
    @Owner(value = "KovalchukIS")
    public void getSystemNotificationsWithoutCommonTest() throws IOException {

        final ObjectNode variables = objectMapper.createObjectNode();
        variables.putArray("sortingFields").addObject()
                .put("name", "notificationDisplayStart")
                .put("asc", true);
        variables.putObject("paging")
                .put("offset", 0)
                .put("limit", 20);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        adminSysLogin,
                        adminSysPassword,
                        "/graphql/getSystemNotificationsWithoutCommon.graphql",
                        variables,
                        null
                );

        final JSONObject responseGraphQLJson = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final Long idFromResponse = responseGraphQLJson
                .getJSONObject("data")
                .getJSONObject("systemAdmin")
                .getJSONArray("getSystemNotificationsWithoutCommon")
                .getJSONObject(0)
                .getLong("id");

        Assert.assertEquals(idFromResponse, NOTIFICATION_ID_FROM_ADMIN_SYS);
    }
}
