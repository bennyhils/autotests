package ru.sbt.sber_learning.notification_autotests.graphql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.qameta.allure.*;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;
import ru.sbt.sber_learning.notification_autotests.GraphQLUtilMethods;
import ru.sbt.sber_learning.notification_autotests.UtilMethods;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Component
@SpringBootTest(classes = {GraphQLUtilMethods.class, UtilMethods.class})
public class SaveSystemNotificationGraphQLTests extends AbstractTestNGSpringContextTests {

    @Value("${autotests-users.sys.login}")
    private String adminSysLogin;

    @Value("${autotests-users.old-password}")
    private String password;

    @Autowired
    private GraphQLUtilMethods graphQLUtilMethods;

    @Autowired
    private UtilMethods utilMethods;

    private static final int STATUS_OK = 200;
    private static final String SCHOOL_ID = "00000000-0000-0000-0001-000000000001";
    private static final String FILE_QUERY = "/graphql/saveSystemNotification.graphql";
    private static final String NOTIFICATION_HEADER = "autotestHeader";
    private static final String NOTIFICATION_TEXT = "autotestText";
    private static final String ERROR_TEXT_NULL = "Variable 'systemNotification' has coerced Null value for NonNull type 'String!'";
    private static final String ERROR_TEXT_PARSE = "Text '' could not be parsed at index 0";
    private static final String RANDOM_ID_VALUE = String.valueOf((int) (Math.random() * 100));
    private static final String NOTIFICATION_DISPLAY_START = ZonedDateTime
            .now()
            .format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    private static final String NOTIFICATION_DISPLAY_END = ZonedDateTime
            .now()
            .plusMinutes(1)
            .format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);

    @AfterMethod
    private void cleanTableSystemNotifications() {
        utilMethods.cleanTableSystemNotificationsByHeaderAndText(NOTIFICATION_HEADER, NOTIFICATION_TEXT);
        utilMethods.cleanTableNotificationsByTitleAndMessage(NOTIFICATION_HEADER, NOTIFICATION_TEXT);
    }

    private JSONObject getJsonSuccessResponse(final CloseableHttpResponse responseGraphQL) throws IOException {
        final JSONObject response = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        return response
                .getJSONObject("data")
                .getJSONObject("systemAdmin")
                .getJSONObject("saveSystemNotification");
    }

    private JSONObject getJsonErrorResponse(final CloseableHttpResponse responseGraphQL) throws IOException {
        final JSONObject response = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        return response
                .getJSONArray("errors")
                .getJSONObject(0)
                .getJSONObject("extensions");
    }

    private CloseableHttpResponse executeSaveSystemNotification(
            final String notificationId,
            final String schoolId,
            final String notificationHeader,
            final String notificationText,
            final String notificationDisplayStart,
            final String notificationDisplayEnd,
            final Boolean closable
    ) throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        final ObjectNode systemNotification = variables.putObject("systemNotification");

        systemNotification
                .put("id", notificationId)
                .put("schoolId", schoolId)
                .put("notificationHeader", notificationHeader)
                .put("notificationText", notificationText)
                .put("notificationDisplayStart", notificationDisplayStart)
                .put("notificationDisplayEnd", notificationDisplayEnd)
                .put("closable", closable);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods.getGraphQLResponse(
                adminSysLogin,
                password,
                FILE_QUERY,
                variables,
                null
        );

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);

        return responseGraphQL;
    }

    @Ignore("После решения задачи EDU-14549 должно возвращать 1 из БД")
    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveSystemNotification")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда нужные значения есть в базе, выполнение с полным набором параметров. ")
    @Owner(value = "Anastasiya Krutova")
    public void saveSystemNotification_everythingExist() throws IOException {

        final CloseableHttpResponse responseGraphQL = executeSaveSystemNotification(
                RANDOM_ID_VALUE,
                SCHOOL_ID,
                NOTIFICATION_HEADER,
                NOTIFICATION_TEXT,
                NOTIFICATION_DISPLAY_START,
                NOTIFICATION_DISPLAY_END,
                true
        );

        final String notificationIdFromResponse = getJsonSuccessResponse(responseGraphQL).getString("id");

        Assert.assertEquals(notificationIdFromResponse, RANDOM_ID_VALUE);
        Assert.assertEquals(utilMethods.selectCountSystemNotificationsByHeaderAndText(
                NOTIFICATION_HEADER,
                NOTIFICATION_TEXT
        ), 1);
    }

    @Ignore("БД notifications не успевает заполниться и, следовательно, подлежать очистке")
    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveSystemNotification")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда нужные значения есть в базе, выполнение с минимальным обязательным набором параметров. ")
    @Owner(value = "Anastasiya Krutova")
    public void saveSystemNotification_MinCountOfNecessaryParameters() throws IOException {

        final CloseableHttpResponse responseGraphQL = executeSaveSystemNotification(
                null,
                null,
                NOTIFICATION_HEADER,
                NOTIFICATION_TEXT,
                NOTIFICATION_DISPLAY_START,
                NOTIFICATION_DISPLAY_END,
                null
        );

        final String notificationHeaderFromResponse = getJsonSuccessResponse(responseGraphQL).getString(
                "notificationHeader");

        Assert.assertEquals(notificationHeaderFromResponse, NOTIFICATION_HEADER);
        Assert.assertEquals(utilMethods.selectCountSystemNotificationsByHeaderAndText(
                NOTIFICATION_HEADER,
                NOTIFICATION_TEXT
        ), 1);
    }

    @Ignore("После решения задачи EDU-14553 должно возвращать Error")
    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveSystemNotification")
    @Story(value = "Проверка некорректных значений")
    @Test(description = "Когда дата окончания меньше даты начала. ")
    @Owner(value = "Anastasiya Krutova")
    public void saveSystemNotification_WhenEndDateEarlierThanStartDate() throws IOException {

        final CloseableHttpResponse responseGraphQL = executeSaveSystemNotification(
                null,
                null,
                NOTIFICATION_HEADER,
                NOTIFICATION_TEXT,
                NOTIFICATION_DISPLAY_END,
                NOTIFICATION_DISPLAY_START,
                null
        );

        final JSONObject extensions = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(extensions.getString("uiErrorCode"), "DEFAULT_ERROR_MESSAGE_CODE");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveSystemNotification")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда notificationHeader = null. ")
    @Owner(value = "Anastasiya Krutova")
    public void saveSystemNotification_notificationHeaderNull() throws IOException {

        final CloseableHttpResponse responseGraphQL = executeSaveSystemNotification(
                null,
                null,
                null,
                NOTIFICATION_TEXT,
                NOTIFICATION_DISPLAY_START,
                NOTIFICATION_DISPLAY_END,
                null
        );

        final JSONObject extensions = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(extensions.getString("uiErrorCode"), "DEFAULT_ERROR_MESSAGE_CODE");
        Allure.step("Error: " + extensions);
        Assert.assertTrue(
                extensions.toString().contains(ERROR_TEXT_NULL),
                "Содержит ли текст ошибки " + "\"" + ERROR_TEXT_NULL + "\"?"
        );
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveSystemNotification")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда notificationText = null. ")
    @Owner(value = "Anastasiya Krutova")
    public void saveSystemNotification_notificationTextNull() throws IOException {

        final CloseableHttpResponse responseGraphQL = executeSaveSystemNotification(
                null,
                null,
                NOTIFICATION_HEADER,
                null,
                NOTIFICATION_DISPLAY_START,
                NOTIFICATION_DISPLAY_END,
                null
        );

        final JSONObject extensions = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(extensions.getString("uiErrorCode"), "DEFAULT_ERROR_MESSAGE_CODE");
        Allure.step("Error: " + extensions);
        Assert.assertTrue(
                extensions.toString().contains(ERROR_TEXT_NULL),
                "Содержит ли текст ошибки " + "\"" + ERROR_TEXT_NULL + "\"?"
        );
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveSystemNotification")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда notificationDisplayStart = null. ")
    @Owner(value = "Anastasiya Krutova")
    public void saveSystemNotification_notificationDisplayStartNull() throws IOException {

        final CloseableHttpResponse responseGraphQL = executeSaveSystemNotification(
                null,
                null,
                NOTIFICATION_HEADER,
                NOTIFICATION_TEXT,
                null,
                NOTIFICATION_DISPLAY_END,
                null
        );

        final JSONObject extensions = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(extensions.getString("uiErrorCode"), "DEFAULT_ERROR_MESSAGE_CODE");
        Allure.step("Error: " + extensions);
        Assert.assertTrue(
                extensions.toString().contains(ERROR_TEXT_NULL),
                "Содержит ли текст ошибки " + "\"" + ERROR_TEXT_NULL + "\"?"
        );
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveSystemNotification")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда notificationDisplayEnd = null. ")
    @Owner(value = "Anastasiya Krutova")
    public void saveSystemNotification_notificationDisplayEndNull() throws IOException {

        final CloseableHttpResponse responseGraphQL = executeSaveSystemNotification(
                null,
                null,
                NOTIFICATION_HEADER,
                NOTIFICATION_TEXT,
                NOTIFICATION_DISPLAY_START,
                null,
                null
        );

        final JSONObject extensions = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(extensions.getString("uiErrorCode"), "DEFAULT_ERROR_MESSAGE_CODE");
        Allure.step("Error: " + extensions);
        Assert.assertTrue(
                extensions.toString().contains(ERROR_TEXT_NULL),
                "Содержит ли текст ошибки " + "\"" + ERROR_TEXT_NULL + "\"?"
        );
    }

    @Ignore("После решения задачи EDU-14557 должно возвращать Error")
    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveSystemNotification")
    @Story(value = "Проверка пустых значений")
    @Test(description = "Когда notificationHeader = \"\". ")
    @Owner(value = "Anastasiya Krutova")
    public void saveSystemNotification_notificationHeaderEmpty() throws IOException {

        final CloseableHttpResponse responseGraphQL = executeSaveSystemNotification(
                null,
                null,
                "",
                NOTIFICATION_TEXT,
                NOTIFICATION_DISPLAY_START,
                NOTIFICATION_DISPLAY_END,
                null
        );

        final JSONObject extensions = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(extensions.getString("uiErrorCode"), "DEFAULT_ERROR_MESSAGE_CODE");
    }

    @Ignore("После решения задачи EDU-14557 должно возвращать Error")
    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveSystemNotification")
    @Story(value = "Проверка пустых значений")
    @Test(description = "Когда notificationText = \"\". ")
    @Owner(value = "Anastasiya Krutova")
    public void saveSystemNotification_notificationTextEmpty() throws IOException {

        final CloseableHttpResponse responseGraphQL = executeSaveSystemNotification(
                null,
                null,
                NOTIFICATION_HEADER,
                "",
                NOTIFICATION_DISPLAY_START,
                NOTIFICATION_DISPLAY_END,
                null
        );

        final JSONObject extensions = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(extensions.getString("uiErrorCode"), "DEFAULT_ERROR_MESSAGE_CODE");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveSystemNotification")
    @Story(value = "Проверка пустых значений")
    @Test(description = "Когда notificationDisplayStart = \"\". ")
    @Owner(value = "Anastasiya Krutova")
    public void saveSystemNotification_notificationDisplayStartEmpty() throws IOException {

        final CloseableHttpResponse responseGraphQL = executeSaveSystemNotification(
                null,
                null,
                NOTIFICATION_HEADER,
                NOTIFICATION_TEXT,
                "",
                NOTIFICATION_DISPLAY_END,
                null
        );

        final JSONObject extensions = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(extensions.getString("uiErrorCode"), "DEFAULT_ERROR_MESSAGE_CODE");
        Allure.step("Error: " + extensions);
        Assert.assertTrue(
                extensions.toString().contains(ERROR_TEXT_PARSE),
                "Содержит ли текст ошибки " + "\"" + ERROR_TEXT_PARSE + "\"?"
        );
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveSystemNotification")
    @Story(value = "Проверка пустых значений")
    @Test(description = "Когда notificationDisplayEnd = \"\". ")
    @Owner(value = "Anastasiya Krutova")
    public void saveSystemNotification_notificationDisplayEndEmpty() throws IOException {

        final CloseableHttpResponse responseGraphQL = executeSaveSystemNotification(
                null,
                null,
                NOTIFICATION_HEADER,
                NOTIFICATION_TEXT,
                NOTIFICATION_DISPLAY_START,
                "",
                null
        );

        final JSONObject extensions = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(extensions.getString("uiErrorCode"), "DEFAULT_ERROR_MESSAGE_CODE");
        Allure.step("Error: " + extensions);
        Assert.assertTrue(
                extensions.toString().contains(ERROR_TEXT_PARSE),
                "Содержит ли текст ошибки " + "\"" + ERROR_TEXT_PARSE + "\"?"
        );
    }
}
