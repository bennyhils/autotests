package ru.sbt.sber_learning.notification_autotests.graphql;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.sbt.sber_learning.notification_autotests.GraphQLUtilMethods;
import ru.sbt.sber_learning.notification_autotests.UtilMethods;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Component
@SpringBootTest(classes = {GraphQLUtilMethods.class, UtilMethods.class})
public class GetGlobalSettingsGraphQLTests extends AbstractTestNGSpringContextTests {

    @Value("${autotests-users.sys.login}")
    private String adminSysLogin;

    @Value("${autotests-users.old-password}")
    private String adminSysPassword;

    @Value("${autotests-users.business.login}")
    private String businessLogin;

    @Value("${autotests-users.teacher.login}")
    private String teacherLogin;

    @Value("${autotests-users.parent.login}")
    private String parentLogin;

    @Value("${autotests-users.student.login}")
    private String studentLogin;

    @Value("${autotests-users.password}")
    private String password;

    @Autowired
    private GraphQLUtilMethods graphQLUtilMethods;

    @Autowired
    private UtilMethods utilMethods;

    private static final int STATUS_OK = 200;
    private final String fileQuery = "/graphql/getGlobalSettings.graphql";

    private JSONArray getJsonSuccessResponse(final CloseableHttpResponse responseGraphQL) throws IOException {
        final JSONObject response = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        return response
                .getJSONObject("data")
                .getJSONObject("notificationConfiguration")
                .getJSONArray("getGlobalSettings");
    }

    private JSONObject getJsonErrorResponse(final CloseableHttpResponse responseGraphQL) throws IOException {
        final JSONObject response = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        return response
                .getJSONArray("errors")
                .getJSONObject(0)
                .getJSONObject("extensions");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetGlobalSettings")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда значения есть в базе и роль пользователя - student. ")
    @Owner(value = "Anastasiya Krutova")
    public void getGlobalSettings_everythingExist_studentAuth() throws IOException {

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        fileQuery,
                        null,
                        null
                );

        final JSONObject errors = getJsonErrorResponse(responseGraphQL);
        final String uiErrorCode = errors.getString("uiErrorCode");

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(uiErrorCode, "SECURITY_ACCESS_DENIED");
        Assert.assertTrue(errors
                .toString()
                .contains("Нет прав для вызова 'getGlobalSettings'. " +
                          "Нужен пермишен: 'administration:notification:globalSettings:get'"));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetGlobalSettings")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда значения есть в базе и роль пользователя - teacher. ")
    @Owner(value = "Anastasiya Krutova")
    public void getGlobalSettings_everythingExist_teacherAuth() throws IOException {

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        teacherLogin,
                        password,
                        fileQuery,
                        null,
                        null
                );

        final JSONObject errors = getJsonErrorResponse(responseGraphQL);
        final String uiErrorCode = errors.getString("uiErrorCode");

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(uiErrorCode, "SECURITY_ACCESS_DENIED");
        Assert.assertTrue(errors
                .toString()
                .contains("Нет прав для вызова 'getGlobalSettings'. " +
                          "Нужен пермишен: 'administration:notification:globalSettings:get'"));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetGlobalSettings")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда значения есть в базе и роль пользователя - adminBusiness. ")
    @Owner(value = "Anastasiya Krutova")
    public void getGlobalSettings_everythingExist_adminBusinessAuth() throws IOException {

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        businessLogin,
                        password,
                        fileQuery,
                        null,
                        null
                );

        final JSONObject errors = getJsonErrorResponse(responseGraphQL);
        final String uiErrorCode = errors.getString("uiErrorCode");

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(uiErrorCode, "SECURITY_ACCESS_DENIED");
        Assert.assertTrue(errors
                .toString()
                .contains("Нет прав для вызова 'getGlobalSettings'. " +
                          "Нужен пермишен: 'administration:notification:globalSettings:get'"));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetGlobalSettings")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда значения есть в базе и роль пользователя - adminSys. ")
    @Owner(value = "Anastasiya Krutova")
    public void getGlobalSettings_everythingExist_adminSysAuth() throws IOException {

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        adminSysLogin,
                        adminSysPassword,
                        fileQuery,
                        null,
                        null
                );

        final JSONArray notificationTypes = getJsonSuccessResponse(responseGraphQL);
        final JSONArray channels = notificationTypes
                .getJSONObject(0)
                .getJSONArray("channels");

        final Set<String> channelNames = new HashSet<>();
        channels.forEach(jsonObj -> channelNames.add(((JSONObject) jsonObj).get("channelName").toString()));

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(channelNames, new HashSet<>(Arrays.asList("UI", "SMS", "EMAIL")));
        Assert.assertEquals(utilMethods.selectCountNotificationTypeChannels(), notificationTypes.length());
    }
}
