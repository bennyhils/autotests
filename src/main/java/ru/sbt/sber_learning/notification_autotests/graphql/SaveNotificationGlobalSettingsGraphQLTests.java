package ru.sbt.sber_learning.notification_autotests.graphql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.qameta.allure.*;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.*;
import ru.sbt.sber_learning.notification_autotests.GraphQLUtilMethods;
import ru.sbt.sber_learning.notification_autotests.UtilMethods;

import java.io.IOException;

import static java.lang.Boolean.parseBoolean;

@Component
@SpringBootTest(classes = {GraphQLUtilMethods.class, UtilMethods.class})
public class SaveNotificationGlobalSettingsGraphQLTests extends AbstractTestNGSpringContextTests {
    private static final int STATUS_OK = 200;
    private int notification_type_id;

    @Value("${autotests-users.sys.login}")
    private String login;

    @Value("${autotests-users.old-password}")
    private String password;

    @Autowired
    private GraphQLUtilMethods graphQLUtilMethods;

    @Autowired
    private UtilMethods utilMethods;

    @BeforeClass(description = "Получение url для выполнения тестов")
    public void addNewNotificationType() {

        notification_type_id = utilMethods.selectMaxNoTificationTypeIDFromDB() + 1;
        final boolean is_active = true;
        final boolean locked = true;
        final String notification_type_description = "test notification type description";
        final int notification_type_group_id = 4;
        final String notification_type_name = "TEST_NOTIFICATION_TYPE_NAME" + notification_type_id;
        final String notification_type_scope = "TEST SCOPE";
        utilMethods.fillDBNotificationTypeId(notification_type_id, is_active, locked, notification_type_description, notification_type_group_id,
                notification_type_name, notification_type_scope);

    }

    @AfterClass
    private void cleanup() {
        utilMethods.cleanDBGlobalActiveChannels(notification_type_id);
        utilMethods.cleanDBNotificationTypes(notification_type_id);
    }

    private JSONObject executeSaveNotificationGlobalSettings(
            final String channelId,
            final String channelName,
            final String channelActive,
            final String notificationTypeId,
            final String notificationTypeName,
            final String notificationTypeDescription,
            final String notificationTypeScope,
            final String notificationTypeGroupId,
            final String notificationTypeLocked,
            final int status
    ) throws IOException{

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        final ObjectNode globalSettingModels = variables.putObject("globalSettingModels");

        final ObjectNode notificationType = globalSettingModels.putObject("notificationType");
        final ObjectNode channels = globalSettingModels.putObject("channels");

        channels
                .put("channelId", channelId)
                .put("channelName", channelName)
                .put("channelActive", channelActive);

        notificationType
                .put("id", notificationTypeId)
                .put("name", notificationTypeName)
                .put("description", notificationTypeDescription)
                .put("groupId", notificationTypeGroupId)
                .put("locked", notificationTypeLocked);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        login,
                        password,
                        "/graphql/saveNotificationGlobalSettings.gql",
                        variables,
                        null
                );
        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), status);
        return new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveNotificationGlobalSettings")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда нужные значения есть в базе, выполнение метода с полным набором параметров.")
    @Owner(value = "Anna Ladygina")
    public void saveNotificationGlobalSettings_everythingExist_shouldReturnTrue() throws IOException {

        final String channelId = "1";
        final String channelName = "channelNameTEST";
        final String channelActive = "true";

        final String notificationTypeId = String.valueOf(notification_type_id);
        final String notificationTypeName = "nameTEST";
        final String notificationTypeDescription = "descriptionTEST";
        final String notificationTypeScope = "scopeTEST";
        final String notificationTypeGroupId = "4";
        final String notificationTypeLocked = "true";


        final JSONObject json = executeSaveNotificationGlobalSettings(channelId, channelName, channelActive, notificationTypeId,
                notificationTypeName, notificationTypeDescription, notificationTypeScope, notificationTypeGroupId, notificationTypeLocked, STATUS_OK);
        final String response = json
                .getJSONObject("data")
                .getJSONObject("notificationConfiguration")
                .get("saveNotificationGlobalSettings").toString();
        Assert.assertTrue(parseBoolean(response));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveNotificationGlobalSettings")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда нужные значения есть в базе, выполнение метода с минимальным обязательным набором параметров.")
    @Owner(value = "Anna Ladygina")
    public void saveNotificationGlobalSettings_everythingExistMinCountOfNecessaryParameters_shouldReturnTrue() throws IOException {

        final String channelId = "2";
        final String channelActive = "false";

        final String notificationTypeId = String.valueOf(notification_type_id);
        final String notificationTypeGroupId = "4";
        final String notificationTypeLocked = "false";


        final JSONObject json = executeSaveNotificationGlobalSettings(channelId, null, channelActive, notificationTypeId,
                null, null, null, notificationTypeGroupId,
                notificationTypeLocked, STATUS_OK);

        final String response = json
                .getJSONObject("data")
                .getJSONObject("notificationConfiguration")
                .get("saveNotificationGlobalSettings").toString();


        Assert.assertTrue(parseBoolean(response));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveNotificationGlobalSettings")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда channelId = null.")
    @Owner(value = "Anna Ladygina")
    public void saveNotificationGlobalSettings_channelIdIsNull_shouldReturnNULL() throws IOException {

        final String channelId = null;
        final String channelActive = "true";

        final String notificationTypeId = String.valueOf(notification_type_id);
        final String notificationTypeGroupId = "4";
        final String notificationTypeLocked = "false";

        final JSONObject json = executeSaveNotificationGlobalSettings(channelId, null, channelActive, notificationTypeId,
                null, null, null, notificationTypeGroupId, notificationTypeLocked, 200);

        final String data = json.get("data").toString();
        Assert.assertEquals(data,"null");

        final String response = json.get("errors").toString();
        Assert.assertTrue(response.contains("Variable 'globalSettingModels' has coerced Null value for NonNull type 'ID!'"));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveNotificationGlobalSettings")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда notificationTypeGroupId = null.")
    @Owner(value = "Anna Ladygina")
    public void saveNotificationGlobalSettings_notificationTypeGroupIdIsNull_shouldReturnNULL() throws IOException {

        final String channelId = "1";
        final String channelActive = "true";

        final String notificationTypeId = String.valueOf(notification_type_id);
        final String notificationTypeGroupId = null;
        final String notificationTypeLocked = "false";

        final JSONObject json = executeSaveNotificationGlobalSettings(channelId, null, channelActive, notificationTypeId,
                null, null, null, notificationTypeGroupId, notificationTypeLocked, 200);

        final String data = json.get("data").toString();
        Assert.assertEquals(data,"null");

        final String response = json.get("errors").toString();
        Assert.assertTrue(response.contains("Variable 'globalSettingModels' has coerced Null value for NonNull type 'ID!'"));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveNotificationGlobalSettings")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда notificationTypeLocked = null.")
    @Owner(value = "Anna Ladygina")
    public void saveNotificationGlobalSettings_notificationTypeLockedIsNull_shouldReturnNULL() throws IOException {

        final String channelActive = "true";
        final String channelId = "1";

        final String notificationTypeId = String.valueOf(notification_type_id);
        final String notificationTypeGroupId = "4";
        final String notificationTypeLocked = null;

        final JSONObject json = executeSaveNotificationGlobalSettings(channelId, null, channelActive, notificationTypeId,
                null, null, null, notificationTypeGroupId, notificationTypeLocked, 200);

        final String data = json.get("data").toString();
        Assert.assertEquals(data,"null");

        final String response = json.get("errors").toString();
        Assert.assertTrue(response.contains("graphql.execution.NonNullableValueCoercedAsNullException: Variable 'globalSettingModels' has coerced Null value for NonNull type 'Boolean!'"));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveNotificationGlobalSettings")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда channelActive = null.")
    @Owner(value = "Anna Ladygina")
    public void saveNotificationGlobalSettings_channelActiveIsNull_shouldReturnNULL() throws IOException {

        final String channelActive = null;
        final String channelId = "1";

        final String notificationTypeId = String.valueOf(notification_type_id);
        final String notificationTypeGroupId = "4";
        final String notificationTypeLocked = "null";

        final JSONObject json = executeSaveNotificationGlobalSettings(channelId, null, channelActive, notificationTypeId,
                null, null, null, notificationTypeGroupId, notificationTypeLocked, 200);

        final String data = json.get("data").toString();
        Assert.assertEquals(data,"null");

        final String response = json.get("errors").toString();
        Assert.assertTrue(response.contains("graphql.execution.NonNullableValueCoercedAsNullException: Variable 'globalSettingModels' has coerced Null value for NonNull type 'Boolean!'"));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveNotificationGlobalSettings")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда notificationType id = null.")
    @Owner(value = "Anna Ladygina")
    public void saveNotificationGlobalSettings_notificationTypeIdIsNull_shouldReturnNULL() throws IOException {

        final String channelActive = "true";
        final String channelId = "1";

        final String notificationTypeId = null;
        final String notificationTypeGroupId = "4";
        final String notificationTypeLocked = "false";
        final JSONObject json = executeSaveNotificationGlobalSettings(channelId, null, channelActive, notificationTypeId,
                null, null, null, notificationTypeGroupId, notificationTypeLocked, 200);

        final String data = json.get("data").toString();
        Assert.assertEquals(data,"null");

        final String response = json.get("errors").toString();
        Assert.assertTrue(response.contains("graphql.execution.NonNullableValueCoercedAsNullException: Variable 'globalSettingModels' has coerced Null value for NonNull type 'ID!'"));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveNotificationGlobalSettings")
    @Story(value = "Проверка пустых значений")
    @Test(description = "Когда channelId = \"\".")
    @Owner(value = "Anna Ladygina")
    public void saveNotificationGlobalSettings_channelIdIsEmpty_shouldReturnNULL() throws IOException {

        final String expectedErrorRU
                = "javax.validation.ConstraintViolationException: batchUpdate.arg0[0].channelId: должно быть больше или равно 1";

        final String expectedErrorEN
                = "javax.validation.ConstraintViolationException: batchUpdate.arg0[0].channelId: must be greater than or equal to 1";

        final String channelActive = "true";
        final String channelId = "";

        final String notificationTypeId = String.valueOf(notification_type_id);
        final String notificationTypeGroupId = "4";
        final String notificationTypeLocked = "false";

        final JSONObject json = executeSaveNotificationGlobalSettings(channelId, null, channelActive, notificationTypeId,
                null, null, null, notificationTypeGroupId, notificationTypeLocked, 200);

        final String data = json.getJSONObject("data").get("notificationConfiguration").toString();
        Assert.assertEquals(data, "null");

        final String response = json.get("errors").toString();

        Allure.step("Error: " + response);

        Assert.assertTrue(response.contains(expectedErrorRU) || response.contains(expectedErrorEN),
                "Содержит ли текст ошибки " + "\"" + expectedErrorRU + "\"" + " или " + "\"" + expectedErrorEN + "\"?");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveNotificationGlobalSettings")
    @Story(value = "Проверка пустых значений")
    @Test(description = "Когда notificationTypeId = \"\".")
    @Owner(value = "Anna Ladygina")
    public void saveNotificationGlobalSettings_notificationTypeIdIsEmpty_shouldReturnNULL() throws IOException {

        final String expectedErrorRU
                = "javax.validation.ConstraintViolationException: batchUpdate.arg0[0].notificationTypeId: должно быть больше или равно 1";

        final String expectedErrorEN
                = "javax.validation.ConstraintViolationException: batchUpdate.arg0[0].notificationTypeId: must be greater than or equal to 1";

        final String channelActive = "true";
        final String channelId = "1";

        final String notificationTypeId = "";
        final String notificationTypeGroupId = "4";
        final String notificationTypeLocked = "false";

        final JSONObject json = executeSaveNotificationGlobalSettings(channelId, null, channelActive, notificationTypeId,
                null, null, null, notificationTypeGroupId, notificationTypeLocked, 200);

        final String data = json.getJSONObject("data").get("notificationConfiguration").toString();
        Assert.assertEquals(data,"null");

        final String response = json.get("errors").toString();

        Allure.step("Error: " + response);

        Assert.assertTrue(response.contains(expectedErrorRU) || response.contains(expectedErrorEN),
                "Содержит ли текст ошибки " + "\"" + expectedErrorRU + "\"" + " или " + "\"" + expectedErrorEN + "\"?");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveNotificationGlobalSettings")
    @Story(value = "Проверка пустых значений")
    @Test(description = "Когда notificationTypeGroupId = \"\".")
    @Owner(value = "Anna Ladygina")
    public void saveNotificationGlobalSettings_notificationTypeGroupIdIsEmpty_shouldReturnTrue() throws IOException {

        final String channelActive = "true";
        final String channelId = "1";

        final String notificationTypeId = String.valueOf(notification_type_id);
        final String notificationTypeGroupId = "";
        final String notificationTypeLocked = "false";

        final JSONObject json = executeSaveNotificationGlobalSettings(channelId, null, channelActive, notificationTypeId,
                null, null, null, notificationTypeGroupId, notificationTypeLocked, 200);

        final String response = json
                .getJSONObject("data")
                .getJSONObject("notificationConfiguration")
                .get("saveNotificationGlobalSettings").toString();

        Assert.assertTrue(parseBoolean(response));
    }

    //todo: странный тест - на пустом параметре channelIsActive думаю стоит падать
    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveNotificationGlobalSettings")
    @Story(value = "Проверка пустых значений")
    @Test(description = "Когда channelActive = \"\".")
    @Owner(value = "Anna Ladygina")
    public void saveNotificationGlobalSettings_channelActiveIsEmpty_shouldReturnTrue() throws IOException {

        final String channelActive = "";
        final String channelId = "1";

        final String notificationTypeId = String.valueOf(notification_type_id);
        final String notificationTypeGroupId = "4";
        final String notificationTypeLocked = "false";

        final JSONObject json = executeSaveNotificationGlobalSettings(channelId, null, channelActive, notificationTypeId,
                null, null, null, notificationTypeGroupId, notificationTypeLocked, 200);

        final String response = json
                .getJSONObject("data")
                .getJSONObject("notificationConfiguration")
                .get("saveNotificationGlobalSettings").toString();

        Assert.assertTrue(parseBoolean(response));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveNotificationGlobalSettings")
    @Story(value = "Проверка пустых значений")
    @Test(description = "Когда notificationTypeLocked = \"\".")
    @Owner(value = "Anna Ladygina")
    public void saveNotificationGlobalSettings_notificationTypeLockedIsEmpty_shouldReturnTrue() throws IOException {

        final String channelActive = "true";
        final String channelId = "1";

        final String notificationTypeId = String.valueOf(notification_type_id);
        final String notificationTypeGroupId = "4";
        final String notificationTypeLocked = "";

        final JSONObject json = executeSaveNotificationGlobalSettings(channelId, null, channelActive, notificationTypeId,
                null, null, null, notificationTypeGroupId, notificationTypeLocked, 200);

        final String response = json
                .getJSONObject("data")
                .getJSONObject("notificationConfiguration")
                .get("saveNotificationGlobalSettings").toString();

        Assert.assertTrue(parseBoolean(response));
    }

}

