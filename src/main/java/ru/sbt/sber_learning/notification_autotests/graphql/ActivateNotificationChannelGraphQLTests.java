package ru.sbt.sber_learning.notification_autotests.graphql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.sbt.sber_learning.notification_autotests.GraphQLUtilMethods;
import ru.sbt.sber_learning.notification_autotests.UtilMethods;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.Map;

@Component
@SpringBootTest(classes = {GraphQLUtilMethods.class, UtilMethods.class})
public class ActivateNotificationChannelGraphQLTests extends AbstractTestNGSpringContextTests {

    @Value("${autotests-users.student.login}")
    private String studentLogin;

    @Value("${autotests-users.password}")
    private String password;

    @Value("${autotests-users.teacher.login}")
    private String teacherLogin;

    @Value("${autotests-users.sys.login}")
    private String adminSysLogin;

    @Value("${autotests-users.old-password}")
    private String adminSysPassword;

    @Autowired
    private GraphQLUtilMethods graphQLUtilMethods;

    @Autowired
    private UtilMethods utilMethods;

    private static final int STATUS_OK = 200;
    private static final int BAD_REQUEST = 400;
    private static final String SCHOOL_ID = "00000000-0000-0000-0001-000000000001";
    private final String fileQueryActivate = "/graphql/activateNotificationChannel.gql";
    private final Map<Long, Boolean> defaultStateChannels = new HashMap<>();
    private LocalDateTime creationTimeBefore;

    @BeforeClass
    private void defaultStateDB() {
        stateOfChannels(defaultStateChannels);
        creationTimeBefore = LocalDateTime.now(ZoneOffset.UTC).minusSeconds(5);
    }

    @AfterClass
    private void cleanDB() {
        final LocalDateTime creationTimeAfter = LocalDateTime.now(ZoneOffset.UTC).plusSeconds(15);
        utilMethods.cleanDBNotificationsByCreationTime(
                creationTimeBefore,
                creationTimeAfter
        );
    }

    @AfterMethod
    private void returnToDefault() throws IOException {
        final Map<Long, Boolean> actualStateChannels = new HashMap<>();
        stateOfChannels(actualStateChannels);

        for (final Long channelKey : defaultStateChannels.keySet()) {
            final Boolean valueOfActualState = actualStateChannels.get(channelKey);
            final Boolean valueOfDefaultState = defaultStateChannels.get(channelKey);
            final String fileQueryDeactivate = "/graphql/deactivateNotificationChannel.gql";

            if (!valueOfDefaultState.equals(valueOfActualState)) {
                mutationNotificationChannels(
                        adminSysLogin,
                        adminSysPassword,
                        valueOfDefaultState ? fileQueryActivate : fileQueryDeactivate,
                        channelKey,
                        null
                );
            }
        }
    }

    private void stateOfChannels(final Map<Long, Boolean> stateOfChannels) {
        stateOfChannels.put(1L, utilMethods.isChannelActive(1L));
        stateOfChannels.put(2L, utilMethods.isChannelActive(2L));
        stateOfChannels.put(3L, utilMethods.isChannelActive(3L));
    }

    private CloseableHttpResponse mutationNotificationChannels(
            final String login,
            final String password,
            final String fileQuery,
            final Long channelId,
            final String schoolId
    ) throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("channelId", channelId);

        return graphQLUtilMethods
                .getGraphQLResponse(
                        login,
                        password,
                        fileQuery,
                        variables,
                        schoolId
                );
    }

    private String getJsonSuccessResponse(final CloseableHttpResponse responseGraphQL) throws IOException {
        final JSONObject response = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        return response
                .getJSONObject("data")
                .getJSONObject("notificationConfiguration")
                .get("activateNotificationChannel").toString();
    }

    private String getJsonErrorResponse(final CloseableHttpResponse responseGraphQL) throws IOException {
        final JSONObject response = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        return response
                .getJSONArray("errors")
                .getJSONObject(0)
                .getJSONObject("extensions")
                .getString("uiErrorCode");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ActivateNotificationChannel")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда канал (2L - EMAIL) есть в БД и он неактивный. ")
    @Owner(value = "Anastasiya Krutova")
    public void activateNotificationChannel_channel2FoundAndInactive() throws IOException {

        final CloseableHttpResponse responseGraphQL =
                mutationNotificationChannels(adminSysLogin, adminSysPassword, fileQueryActivate, 2L, null);

        final String isChannelActive = getJsonSuccessResponse(responseGraphQL);

        Assert.assertTrue(utilMethods.isChannelActive(2L));
        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(isChannelActive, "true");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ActivateNotificationChannel")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда канал (3L - SMS) есть в БД и он неактивный. ")
    @Owner(value = "Anastasiya Krutova")
    public void activateNotificationChannel_channel3FoundAndInactive() throws IOException {

        final CloseableHttpResponse responseGraphQL =
                mutationNotificationChannels(adminSysLogin, adminSysPassword, fileQueryActivate, 3L, null);

        final String isChannelActive = getJsonSuccessResponse(responseGraphQL);

        Assert.assertTrue(utilMethods.isChannelActive(3L));
        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(isChannelActive, "true");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ActivateNotificationChannel")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда канал (1L - UI) есть в БД и он активный. Нельзя изменять активность 1 канала. ")
    @Owner(value = "Anastasiya Krutova")
    public void activateNotificationChannel_channel1FoundAndActive() throws IOException {

        final CloseableHttpResponse responseGraphQL =
                mutationNotificationChannels(adminSysLogin, adminSysPassword, fileQueryActivate, 1L, null);

        final String errorType = getJsonErrorResponse(responseGraphQL);

        Assert.assertTrue(utilMethods.isChannelActive(1L));
        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(errorType, "VALIDATION_ERROR");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ActivateNotificationChannel")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда канала (100L) нет в БД. ")
    @Owner(value = "Anastasiya Krutova")
    public void activateNotificationChannel_channelNotFound() throws IOException {

        final CloseableHttpResponse responseGraphQL =
                mutationNotificationChannels(adminSysLogin, adminSysPassword, fileQueryActivate, 100L, null);

        final String errorType = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(errorType, "NOTIFICATION_CHANNEL_NOT_FOUND");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ActivateNotificationChannel")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда channelId = null. ")
    @Owner(value = "Anastasiya Krutova")
    public void activateNotificationChannel_channelNull() throws IOException {

        final CloseableHttpResponse responseGraphQL =
                mutationNotificationChannels(adminSysLogin, adminSysPassword, fileQueryActivate, null, null);

        final JSONObject json = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final JSONObject errorType = json
                .getJSONArray("errors")
                .getJSONObject(0)
                .getJSONObject("extensions");

        final String uiErrorCode = errorType.getString("uiErrorCode");

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(uiErrorCode, "DEFAULT_ERROR_MESSAGE_CODE");
        Assert.assertTrue(errorType.getString("trace")
                .contains("Variable 'channelId' has coerced Null value for NonNull type"));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ActivateNotificationChannel")
    @Story(value = "Проверка пустых значений")
    @Test(description = "Когда channelId = \"\". ")
    @Owner(value = "Anastasiya Krutova")
    public void activateNotificationChannel_channelEmpty() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("channelId", "");

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        adminSysLogin,
                        adminSysPassword,
                        fileQueryActivate,
                        variables,
                        null
                );

        final String errorType = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(errorType, "NOTIFICATION_CHANNEL_NOT_FOUND");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ActivateNotificationChannel")
    @Story(value = "Проверка некорректных значений")
    @Test(description = "Когда channelId не передается. ")
    @Owner(value = "Anastasiya Krutova")
    public void activateNotificationChannel_channelNotTransmit() throws IOException {

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        adminSysLogin,
                        adminSysPassword,
                        fileQueryActivate,
                        null,
                        null
                );

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), BAD_REQUEST);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ActivateNotificationChannel")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда канал (3L - SMS) есть в БД и роль пользователя - teacher. Должен вернуться Error. ")
    @Owner(value = "Anastasiya Krutova")
    public void activateNotificationChannel_shouldReturnAccessDenied_teacherAuth() throws IOException {

        final CloseableHttpResponse responseGraphQL =
                mutationNotificationChannels(teacherLogin, password, fileQueryActivate, 3L, SCHOOL_ID);

        final String errorType = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(errorType, "SECURITY_ACCESS_DENIED");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ActivateNotificationChannel")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда канал (2L - EMAIL) есть в БД, он неактивный и роль пользователя - student. " +
                        "Должен вернуться Error. ")
    @Owner(value = "Anastasiya Krutova")
    public void activateNotificationChannel_shouldReturnAccessDenied_studentAuth() throws IOException {

        final CloseableHttpResponse responseGraphQL =
                mutationNotificationChannels(studentLogin, password, fileQueryActivate, 2L, SCHOOL_ID);

        final String errorType = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(errorType, "SECURITY_ACCESS_DENIED");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ActivateNotificationChannel")
    @Story(value = "Проверка некорректных значений")
    @Test(description = "Когда у роли student schoolId - другой. Должен вернуться Error. ")
    @Owner(value = "Anastasiya Krutova")
    public void activateNotificationChannel_schoolIdAnother_studentAuth() throws IOException {

        final String anotherSchoolId = "312ffa69-d1a8-482e-825c-949114487d88";
        final CloseableHttpResponse responseGraphQL =
                mutationNotificationChannels(studentLogin, password, fileQueryActivate, 3L, anotherSchoolId);

        final String errorType = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(errorType, "SECURITY_ACCESS_DENIED");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ActivateNotificationChannel")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда schoolId = null. Должен вернуться Error. ")
    @Owner(value = "Anastasiya Krutova")
    public void activateNotificationChannel_schoolIdNull_studentAuth() throws IOException {

        final CloseableHttpResponse responseGraphQL =
                mutationNotificationChannels(studentLogin, password, fileQueryActivate, 2L, null);

        final String errorType = getJsonErrorResponse(responseGraphQL);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(errorType, "SECURITY_ACCESS_DENIED");
    }
}
