package ru.sbt.sber_learning.notification_autotests.graphql;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.sbt.sber_learning.notification_autotests.GraphQLUtilMethods;
import ru.sbt.sber_learning.notification_autotests.UtilMethods;

import java.io.IOException;

@Component
@SpringBootTest(classes = {GraphQLUtilMethods.class, UtilMethods.class})
public class GetNotificationReadIntervalGraphQLTest extends AbstractTestNGSpringContextTests {

    private static final int STATUS_OK = 200;

    @Value("${autotests-users.sys.login}")
    private String login;

    @Value("${autotests-users.old-password}")
    private String password;

    @Autowired
    private GraphQLUtilMethods graphQLUtilMethods;

    @Autowired
    private UtilMethods utilMethods;

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "GetNotificationReadInterval")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда значение есть в базе")
    @Owner(value = "Anna Ladygina")
    public void getNotificationReadInterval_everythingExist_shouldReturnIntervalFromDB() throws IOException {

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        login,
                        password,
                        "/graphql/getNotificationReadInterval.gql",
                        null,
                        null
                );

        final JSONObject json = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final String intervalFromResponse = json
                .getJSONObject("data")
                .getJSONObject("notificationConfiguration")
                .get("getNotificationReadInterval").toString();
        final String intervalFromDB = utilMethods.selectNoTificationsReadIntervalFromDB();

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(intervalFromResponse, intervalFromDB);
    }

}
