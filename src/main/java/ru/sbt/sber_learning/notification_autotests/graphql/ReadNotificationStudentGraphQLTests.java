package ru.sbt.sber_learning.notification_autotests.graphql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.qameta.allure.*;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;
import ru.sbt.sber_learning.notification_autotests.GraphQLUtilMethods;
import ru.sbt.sber_learning.notification_autotests.UtilMethods;

import java.io.IOException;

@Component
@SpringBootTest(classes = {GraphQLUtilMethods.class, UtilMethods.class})
public class ReadNotificationStudentGraphQLTests extends AbstractTestNGSpringContextTests {

    @Value("${autotests-users.student.login}")
    private String studentLogin;

    @Value("${autotests-users.password}")
    private String password;

    @Autowired
    private GraphQLUtilMethods graphQLUtilMethods;

    @Autowired
    private UtilMethods utilMethods;

    private static final int STATUS_OK = 200;
    private static final String SCHOOL_ID = "00000000-0000-0000-0001-000000000001";
    private static final String FILE_QUERY = "/graphql/readNotificationStudent.graphql";
    private static final String ERROR_PERMISSION_TEXT = "Нет прав для вызова 'readNotification'. " +
                                                        "Нужен пермишен: 'studyProcess:notification:read'";
    private static final String ERROR_NULL_TEXT = "Variable 'notificationId' has coerced Null value for NonNull type 'ID!'";
    private Long readNotificationId;
    private Long[] notReadNotificationIds = new Long[2];

    @BeforeMethod
    private void fillTableNotifications() {
        readNotificationId = utilMethods.fillDBNotificationsMass(
                utilMethods.getSelectUserIdFromUsersByLogin(studentLogin),
                SCHOOL_ID,
                true,
                false
        );
        notReadNotificationIds = utilMethods.fillDBNotificationsMass(
                2,
                utilMethods.getSelectUserIdFromUsersByLogin(studentLogin),
                SCHOOL_ID,
                false,
                false
        );
    }

    @AfterMethod
    private void cleanup() {
        utilMethods.cleanDBNotifications(utilMethods.getSelectUserIdFromUsersByLogin(studentLogin), SCHOOL_ID);
    }

    private String getJsonSuccessResponse(final CloseableHttpResponse responseGraphQL) throws IOException {
        final JSONObject response = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        return response
                .getJSONObject("data")
                .getJSONObject("student")
                .get("readNotification").toString();
    }

    private JSONObject getJsonErrorResponse(final CloseableHttpResponse responseGraphQL) throws IOException {
        final JSONObject response = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));

        return response
                .getJSONArray("errors")
                .getJSONObject(0)
                .getJSONObject("extensions");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ReadNotificationStudent")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда указан существующий ID непрочитанной нотификации. Должна стать прочитанной. ")
    @Owner(value = "Anastasiya Krutova")
    public void readNotification_whenNotificationExistsAndWasNotRead_shouldReturnIDFromDB() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("notificationId", notReadNotificationIds[0]);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        FILE_QUERY,
                        variables,
                        SCHOOL_ID
                );

        final String notificationIdFromResponse = getJsonSuccessResponse(responseGraphQL);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(notificationIdFromResponse, notReadNotificationIds[0].toString());

        Assert.assertTrue(utilMethods.isNotificationRead(notReadNotificationIds[0]));
        Assert.assertFalse(utilMethods.isNotificationRead(notReadNotificationIds[1]));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ReadNotificationStudent")
    @Story(value = "Основная функциональность")
    @Test(description = "Когда указан существующий ID прочитанной нотификации. Должна остаться прочитанной. ")
    @Owner(value = "Anastasiya Krutova")
    public void readNotification_whenNotificationExistsAndWasRead_shouldReturnIDFromDB() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("notificationId", readNotificationId);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        FILE_QUERY,
                        variables,
                        SCHOOL_ID
                );

        final String notificationIdFromResponse = getJsonSuccessResponse(responseGraphQL);

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(notificationIdFromResponse, readNotificationId.toString());

        Assert.assertTrue(utilMethods.isNotificationRead(readNotificationId));
        Assert.assertFalse(utilMethods.isNotificationRead(notReadNotificationIds[0]));
        Assert.assertFalse(utilMethods.isNotificationRead(notReadNotificationIds[1]));
    }

    @Ignore(value = "Bug: EDU-14717. После решения должен возвращать ERROR")
    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ReadNotificationStudent")
    @Story(value = "Проверка некорректных значений")
    @Test(description = "Когда указан несуществующий notificationId. ")
    @Owner(value = "Anastasiya Krutova")
    public void readNotification_whenNotificationDoesntExists_shouldReturnError() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("notificationId",  "999");

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        FILE_QUERY,
                        variables,
                        SCHOOL_ID
                );

        final JSONObject errors = getJsonErrorResponse(responseGraphQL);
        final String uiErrorCode = errors.getString("uiErrorCode");

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(uiErrorCode, "DEFAULT_ERROR_MESSAGE_CODE");
        Assert.assertFalse(utilMethods.isNotificationRead(notReadNotificationIds[1]));
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ReadNotificationStudent")
    @Story(value = "Проверка некорректных значений")
    @Test(description = "Когда передается неверный schoolId. ")
    @Owner(value = "Anastasiya Krutova")
    public void readNotification_whenWrongSchool_shouldReturnError() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("notificationId", notReadNotificationIds[0]);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        FILE_QUERY,
                        variables,
                        "00000002-0002-0000-0002-000000000002"
                );

        final JSONObject errors = getJsonErrorResponse(responseGraphQL);
        final String uiErrorCode = errors.getString("uiErrorCode");

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertFalse(utilMethods.isNotificationRead(notReadNotificationIds[0]));
        Assert.assertEquals(uiErrorCode, "SECURITY_ACCESS_DENIED");

        Allure.step("Error: " + errors.toString());
        Assert.assertTrue(errors.toString().contains(ERROR_PERMISSION_TEXT),
                "Содержит ли текст ошибки " + "\"" + ERROR_PERMISSION_TEXT + "\"?");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ReadNotificationStudent")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда schoolId = null. ")
    @Owner(value = "Anastasiya Krutova")
    public void readNotification_whenSchoolIdNull_shouldReturnError() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("notificationId", notReadNotificationIds[0]);

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        FILE_QUERY,
                        variables,
                        null
                );

        final JSONObject errors = getJsonErrorResponse(responseGraphQL);
        final String uiErrorCode = errors.getString("uiErrorCode");

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(uiErrorCode, "SECURITY_ACCESS_DENIED");
        Assert.assertFalse(utilMethods.isNotificationRead(notReadNotificationIds[0]));

        Allure.step("Error: " + errors.toString());
        Assert.assertTrue(errors.toString().contains(ERROR_PERMISSION_TEXT),
                "Содержит ли текст ошибки " + "\"" + ERROR_PERMISSION_TEXT + "\"?");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ReadNotificationStudent")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда notificationId = null. ")
    @Owner(value = "Anastasiya Krutova")
    public void readNotification_whenNotificationIdNull_shouldReturnError() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.putNull("notificationId");

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        FILE_QUERY,
                        variables,
                        SCHOOL_ID
                );

        final JSONObject errors = getJsonErrorResponse(responseGraphQL);
        final String uiErrorCode = errors.getString("uiErrorCode");

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(uiErrorCode, "DEFAULT_ERROR_MESSAGE_CODE");
        Assert.assertFalse(utilMethods.isNotificationRead(notReadNotificationIds[0]));

        Allure.step("Error: " + errors.toString());
        Assert.assertTrue(errors.toString().contains(ERROR_NULL_TEXT),
                "Содержит ли текст ошибки " + "\"" + ERROR_NULL_TEXT + "\"?");
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "ReadNotificationStudent")
    @Story(value = "Проверка null значений")
    @Test(description = "Когда notificationId не передается. ")
    @Owner(value = "Anastasiya Krutova")
    public void readNotification_whenNotificationIdNotTransmit_shouldReturnError() throws IOException {

        final ObjectNode variables = new ObjectMapper().createObjectNode();

        final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                .getGraphQLResponse(
                        studentLogin,
                        password,
                        FILE_QUERY,
                        variables,
                        SCHOOL_ID
                );

        final JSONObject errors = getJsonErrorResponse(responseGraphQL);
        final String uiErrorCode = errors.getString("uiErrorCode");

        Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
        Assert.assertEquals(uiErrorCode, "DEFAULT_ERROR_MESSAGE_CODE");
        Assert.assertFalse(utilMethods.isNotificationRead(notReadNotificationIds[0]));

        Allure.step("Error: " + errors.toString());
        Assert.assertTrue(errors.toString().contains(ERROR_NULL_TEXT),
                "Содержит ли текст ошибки " + "\"" + ERROR_NULL_TEXT + "\"?");

    }
}