package ru.sbt.sber_learning.notification_autotests;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import com.vimalselvam.graphql.GraphqlTemplate;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import static org.apache.http.impl.client.HttpClients.createDefault;

@Slf4j
@Component
public class GraphQLUtilMethods {

    private static final String SCHOOL_ID = "00000000-0000-0000-0001-000000000001";

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Value("${notifications.graphqlURI}")
    private String graphqlURI;

    @Value("${autotests-users.sys.login}")
    private String adminSysLogin;

    @Value("${autotests-users.old-password}")
    private String oldPassword;

    @Value("${autotests-users.password}")
    private String password;

    @Autowired
    UtilMethods utilMethods;

    public CloseableHttpResponse getGraphQLResponse(
            final String login,
            final String password,
            final String fileGraphQL,
            final ObjectNode variables,
            final String schoolId
    ) throws IOException {

        final String tokenId = getToken(login, password);
        final InputStream graphQLRequest = new ClassPathResource(fileGraphQL).getInputStream();
        final String payload = GraphqlTemplate.parseGraphql(graphQLRequest, variables);
        return prepareResponse(payload, tokenId, schoolId);
    }

    private String getToken(final String login, final String password) {
        final String url = graphqlURI + "/auth/realms/EduPowerKeycloak/protocol/openid-connect/token";
        final CloseableHttpClient client = HttpClients.createDefault();
        final HttpPost httpPost = new HttpPost(url);

        final List<NameValuePair> params = new ArrayList<>(2);
        params.add(new BasicNameValuePair("client_id", "edupower"));
        params.add(new BasicNameValuePair("username", login));
        params.add(new BasicNameValuePair("password", password));
        params.add(new BasicNameValuePair("grant_type", "password"));

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
        } catch (final UnsupportedEncodingException e) {
            throw new IllegalStateException(e);
        }

        final HttpResponse response;
        try {
            response = client.execute(httpPost);
        } catch (final IOException e) {
            throw new IllegalStateException(e);
        }

        final HttpEntity entity = response.getEntity();
        try (final InputStream content = entity.getContent()) {

            final JsonNode jsonNode = objectMapper.readTree(content);
            return jsonNode.get("access_token").asText();
        } catch (final IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private CloseableHttpResponse prepareResponse(
            final String graphqlPayload,
            final String tokenId,
            final String schoolId
    ) throws IOException {
        final CloseableHttpClient client = createDefault();
        final HttpPost httpPost = new HttpPost(graphqlURI + "/services/graphql");
        final StringEntity entity = new StringEntity(graphqlPayload);

        httpPost.setEntity(entity);
        httpPost.setHeader("Content-type", "application/json");
        httpPost.setHeader("Cookie", "tokenId=" + tokenId);
        httpPost.setHeader("schoolId", schoolId);

        return client.execute(httpPost);
    }

    public String getTemporaryPassFromPdf(final String userIds) throws IOException {

        final ObjectNode variables = objectMapper.createObjectNode();

        variables.put("userIds", userIds);

        final CloseableHttpResponse responseGraphQL = getGraphQLResponse(
                adminSysLogin,
                oldPassword,
                "/graphql/setTemporaryPasswords.gql",
                variables,
                null
        );

        final JSONObject response = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
        final JSONObject data = response.getJSONObject("data");
        final String temporaryPass = data.getJSONObject("user").getString("setTemporaryPasswords");

        final File file = new File("./temporaryPass.pdf");

        try (final FileOutputStream fos = new FileOutputStream(file)) {

            final String b64 = temporaryPass;
            final byte[] decoder = Base64.getDecoder().decode(b64);

            fos.write(decoder);
            log.info("PDF файл с паролем сохранен");
        } catch (final Exception e) {
            log.error("PDF файл с паролем НЕ сохранен!", e);
            throw new IllegalStateException(e);
        }

        final PdfReader reader;
        try {
            reader = new PdfReader("./temporaryPass.pdf");

            final String textFromPage = PdfTextExtractor.getTextFromPage(reader, 1);
            reader.close();

            final int indexOfPass = textFromPage.indexOf("Пароль: ");
            final String pass = textFromPage.substring(indexOfPass + 8, indexOfPass + 14);
            return pass;

        } catch (final IOException e) {
            log.error("Ошибка чтения файла с паролем!", e);
            throw new IllegalStateException(e);
        }
    }

    public boolean setPassword(final String userLogin, final String schoolId) throws IOException {

        final String userId = utilMethods.getSelectUserIdFromUsersByLogin(userLogin);

        final String tempPass = getTemporaryPassFromPdf(userId);
        utilMethods.deleteSecurityUserIdFromKCDB(userLogin);

        final ObjectNode variablesSetPass = objectMapper.createObjectNode();
        variablesSetPass.put("newPassword", password);
        variablesSetPass.put("oldPassword", tempPass);

        final CloseableHttpResponse responseGraphQLSetPass = getGraphQLResponse(
                userLogin,
                tempPass,
                "/graphql/setPassword.gql",
                variablesSetPass,
                schoolId
        );

        final JSONObject responseSetPass = new JSONObject(EntityUtils.toString(responseGraphQLSetPass.getEntity()));
        final JSONObject dataSetPass = responseSetPass.getJSONObject("data");
        final String id = dataSetPass.getJSONObject("user").getJSONObject("setPassword").getString("id");

        return !id.isEmpty();
    }
}