package ru.sbt.sber_learning.notification_autotests;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.stereotype.Component;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import ru.sbt.sber_learning.notification_autotests.graphql.*;

@Slf4j
@Component
public class NotificationAutotestsApp implements CommandLineRunner {

    @Value("${system.time-for-start}")
    private Long timeForStart;

    public static void main(final String[] args) {
        final SpringApplication notificationsAutotestsApp = new SpringApplication(NotificationAutotestsApp.class);
        notificationsAutotestsApp.run(args);
    }

    @Override
    public void run(final String... args) {
        try {
            //Для того чтобы поднялся сервис notifications
            Thread.sleep(timeForStart);
        } catch (final InterruptedException e) {
            log.error("Error: ", e);
        }

        log.info("NOTIFICATIONS AUTOTESTS BEGIN TO RUN!");

        final TestListenerAdapter tla = new TestListenerAdapter();
        final TestNG testng = new TestNG();
        testng.setTestClasses(new Class[]{

                //Создание пользователей для автотестов
                UsersManagerTests.class,

                //GraphQL api tests
                ActivateNotificationChannelGraphQLTests.class,
                DeactivateNotificationChannelGraphQLTests.class,
                DeleteSystemNotificationGraphQLTests.class,
                EditNotificationChannelsForSchoolsGraphQLTests.class,
                GetChannelsByOrgUnitIdGraphQLTests.class,
                GetChannelsBySchoolIdsGraphQL.class,
                GetGlobalSettingsGraphQLTests.class,
                GetNotificationReadIntervalGraphQLTest.class,
                GetNotificationsAsParentGraphQLTests.class,
                GetNotificationsCountStudentGraphQLTests.class,
                GetNotificationsCountParentGraphQLTests.class,
                GetNotificationsCountTeacherGraphQLTests.class,
                GetNotificationsStudentGraphqlTests.class,
                GetNotificationsTeacherGraphQLTests.class,
                GetNotificationUserSettingsGraphQLTests.class,
                GetSystemNotificationsGraphQLTests.class,
                ReadAllNotificationsStudentGraphQLTests.class,
                ReadAllNotificationsTeacherGraphQLTests.class,
                ReadNotificationStudentGraphQLTests.class,
                ReadNotificationTeacherGraphQLTests.class,
                SaveNotificationGlobalSettingsGraphQLTests.class,
                SaveNotificationUserSettingsGraphQLTests.class,
                SaveSystemNotificationGraphQLTests.class,
        });
        testng.addListener(tla);
        testng.run();
        log.info("NOTIFICATIONS AUTOTESTS END!");
    }
}