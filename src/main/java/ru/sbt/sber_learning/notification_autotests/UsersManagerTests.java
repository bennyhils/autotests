package ru.sbt.sber_learning.notification_autotests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.qameta.allure.*;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

@Component
@SpringBootTest(classes = {GraphQLUtilMethods.class, UtilMethods.class})
public class UsersManagerTests extends AbstractTestNGSpringContextTests {

    private static final int STATUS_OK = 200;
    private static final String SCHOOL_ID = "00000000-0000-0000-0001-000000000001";
    private static final String ANOTHER_SCHOOL_ID = "d6b30c74-8a92-4367-a94e-9566a51af77b";

    @Value("${autotests-users.business.first-name}")
    private String businessFirstName;

    @Value("${autotests-users.teacher.first-name}")
    private String teacherFirstName;

    @Value("${autotests-users.parent.first-name}")
    private String parentFirstName;

    @Value("${autotests-users.student.first-name}")
    private String studentFirstName;

    @Value("${autotests-users.last-name}")
    private String businessLastName;

    @Value("${autotests-users.last-name}")
    private String teacherLastName;

    @Value("${autotests-users.last-name}")
    private String parentLastName;

    @Value("${autotests-users.last-name}")
    private String studentLastName;

    @Value("${autotests-users.business.login}")
    private String businessLogin;

    @Value("${autotests-users.teacher.login}")
    private String teacherLogin;

    @Value("${autotests-users.parent.login}")
    private String parentLogin;

    @Value("${autotests-users.student.login}")
    private String studentLogin;

    @Value("${autotests-users.business.email}")
    private String businessEmail;

    @Value("${autotests-users.old-password}")
    private String oldPassword;

    @Value("${autotests-users.studentFromAnotherSchool.login}")
    private String studentFromAnotherSchoolLogin;

    @Value("${autotests-users.password}")
    private String password;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private GraphQLUtilMethods graphQLUtilMethods;

    @Autowired
    private UtilMethods utilMethods;

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveUser")
    @Story(value = "SaveAdminBusinessViaAdminSys")
    @Test(description = "Создание пользователя с ролью конфигуратор системы с помощью системного администратора",
            priority=1)
    @Owner(value = "KovalchukIS")
    public void saveAdminBusinessViaAdminSys() throws IOException {

        final boolean isNotUser = (utilMethods.getSelectUserIdFromUsersByLogin(businessLogin) == null);

        if (isNotUser) {
            Allure.step("Будет создан пользователь " + businessLogin);
            final ObjectNode variablesSaveBusiness = objectMapper.createObjectNode();
            final ObjectNode user = variablesSaveBusiness.putObject("user");

            user
                    .put("firstName", businessFirstName)
                    .put("middleName", "")
                    .put("lastName", businessLastName)
                    .put("login", businessLogin)
                    .put("status", "ACTIVE")
                    .put("email", businessEmail);

            user.putArray("parentRoles");

            user.putArray("businessAdminRoles")
                    .addObject()
                    .put("status", "ACTIVE")
                    .put("schoolId", SCHOOL_ID);

            final ArrayNode functionalRoles = user.putArray("newAndUpdatedFunctionalRoles");
            functionalRoles.addObject()
                    .put("code", "BUSINESS_STATISTICS")
                    .put("schoolId", SCHOOL_ID)
                    .putNull("dateEnd")
                    .putNull("dateStart");
            functionalRoles.addObject()
                    .put("code", "BASE_ROLE")
                    .put("schoolId", SCHOOL_ID)
                    .putNull("dateEnd")
                    .putNull("dateStart");
            functionalRoles.addObject()
                    .put("code", "CONTENT_BASE")
                    .put("schoolId", SCHOOL_ID)
                    .putNull("dateEnd")
                    .putNull("dateStart");
            functionalRoles.addObject()
                    .put("code", "SCHOOL_USERS_ADM")
                    .put("schoolId", SCHOOL_ID)
                    .putNull("dateEnd")
                    .putNull("dateStart");
            functionalRoles.addObject()
                    .put("code", "BUSINESS_ADM")
                    .put("schoolId", SCHOOL_ID)
                    .putNull("dateEnd")
                    .putNull("dateStart");

            user.putArray("deletedFunctionalRoles");

            final CloseableHttpResponse responseGraphQLSaveBusiness = graphQLUtilMethods
                    .getGraphQLResponse(
                            "admin-sys",
                            oldPassword,
                            "/graphql/saveUserViaAdminSys.graphql",
                            variablesSaveBusiness,
                            null
                    );

            final JSONObject responseSaveBusiness = new JSONObject(EntityUtils.toString(responseGraphQLSaveBusiness.getEntity()));
            final JSONObject dataSaveBusiness = responseSaveBusiness.getJSONObject("data");
            final String loginBusinessFromResponse = dataSaveBusiness.getJSONObject("systemAdmin").getJSONObject("saveUser").getString("login");

            Assert.assertEquals(responseGraphQLSaveBusiness.getStatusLine().getStatusCode(), STATUS_OK);
            Assert.assertEquals(loginBusinessFromResponse, businessLogin);
        }
            Allure.step("Пользователь с логином " + businessLogin + " был создан или существовал. У пользователя будет установлен пароль по умолчанию");
            final boolean isPassSet = graphQLUtilMethods.setPassword(businessLogin, SCHOOL_ID);
            Assert.assertTrue(isPassSet);

    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveUser")
    @Story(value = "SaveTeacherViaAdminBusiness")
    @Test(description = "Создание пользователя с ролью учитель русского языка конфигуратором системы",
            priority=2)
    @Owner(value = "KovalchukIS")
    public void saveTeacherViaAdminBusiness() throws IOException {

        final boolean isNotUser = (utilMethods.getSelectUserIdFromUsersByLogin(teacherLogin) == null);

        if (isNotUser) {
            Allure.step("Будет создан пользователь " + teacherLogin);
            final ObjectNode variables = objectMapper.createObjectNode();
            final ObjectNode user = variables.putObject("user");

            user
                    .put("firstName", teacherFirstName)
                    .put("middleName", "")
                    .put("lastName", teacherLastName)
                    .put("login", teacherLogin)
                    .put("status", "ACTIVE");

            user.putArray("teacherRoles")
                    .addObject()
                    .put("schoolId", SCHOOL_ID)
                    .put("status", "ACTIVE")
                    .putArray("subjects").add(4);

            user.putArray("parentRoles");

            final ArrayNode newAndUpdatedFunctionalRoles = user.putArray("newAndUpdatedFunctionalRoles");
            newAndUpdatedFunctionalRoles.addObject()
                    .put("code", "CONTENT_SCHOOL")
                    .put("schoolId", SCHOOL_ID)
                    .putNull("dateEnd")
                    .putNull("dateStart");
            newAndUpdatedFunctionalRoles.addObject()
                    .put("code", "TEACHER")
                    .put("schoolId", SCHOOL_ID)
                    .putNull("dateEnd")
                    .putNull("dateStart");
            newAndUpdatedFunctionalRoles.addObject()
                    .put("code", "BASE_ROLE")
                    .put("schoolId", SCHOOL_ID)
                    .putNull("dateEnd")
                    .putNull("dateStart");
            newAndUpdatedFunctionalRoles.addObject()
                    .put("code", "CONTENT_BASE")
                    .put("schoolId", SCHOOL_ID)
                    .putNull("dateEnd")
                    .putNull("dateStart");

            user.putArray("deletedFunctionalRoles");

            final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                    .getGraphQLResponse(
                            businessLogin,
                            password,
                            "/graphql/saveUserViaAdminBusiness.graphql",
                            variables,
                            SCHOOL_ID
                    );

            final JSONObject response = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
            final JSONObject data = response.getJSONObject("data");
            final String loginFromResponse = data.getJSONObject("businessAdmin").getJSONObject("saveUser").getString("login");

            Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
            Assert.assertEquals(loginFromResponse, teacherLogin);
        }

        Allure.step("Пользователь с логином " + teacherLogin + " был создан или существовал. У пользователя будет установлен пароль по умолчанию");
        final boolean isPassSet = graphQLUtilMethods.setPassword(teacherLogin, SCHOOL_ID);
        Assert.assertTrue(isPassSet);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveUser")
    @Story(value = "SaveParentViaAdminBusiness")
    @Test(description = "Создание пользователя с ролью родитель конфигуратором системы",
            priority=4)
    @Owner(value = "KovalchukIS")
    public void saveParentViaAdminBusiness() throws IOException {

        final boolean isNotUser = (utilMethods.getSelectUserIdFromUsersByLogin(parentLogin) == null);

        if (isNotUser) {
            Allure.step("Будет создан пользователь " + parentLogin);
            final ObjectNode variables = objectMapper.createObjectNode();
            final ObjectNode user = variables.putObject("user");

            final String childrenId = utilMethods.getSelectUserIdFromUsersByLogin(studentLogin);

            user
                    .put("firstName", parentFirstName)
                    .put("middleName", "")
                    .put("lastName", parentLastName)
                    .put("login", parentLogin)
                    .put("status", "ACTIVE");

            user.putArray("parentRoles")
                    .addObject()
                    .put("schoolId", SCHOOL_ID)
                    .put("status", "ACTIVE")
                    .putArray("children").add(childrenId);

            final ArrayNode newAndUpdatedFunctionalRoles = user.putArray("newAndUpdatedFunctionalRoles");
            newAndUpdatedFunctionalRoles.addObject()
                    .put("code", "PARENT")
                    .put("schoolId", SCHOOL_ID)
                    .putNull("dateEnd")
                    .putNull("dateStart");
            newAndUpdatedFunctionalRoles.addObject()
                    .put("code", "BASE_ROLE")
                    .put("schoolId", SCHOOL_ID)
                    .putNull("dateEnd")
                    .putNull("dateStart");

            user.putArray("deletedFunctionalRoles");

            final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                    .getGraphQLResponse(
                            businessLogin,
                            password,
                            "/graphql/saveUserViaAdminBusiness.graphql",
                            variables,
                            SCHOOL_ID
                    );

            final JSONObject response = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
            final JSONObject data = response.getJSONObject("data");
            final String loginFromResponse = data.getJSONObject("businessAdmin").getJSONObject("saveUser").getString("login");

            Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
            Assert.assertEquals(loginFromResponse, parentLogin);
        }

        Allure.step("Пользователь с логином " + parentLogin + " был создан или существовал. У пользователя будет установлен пароль по умолчанию");
        final boolean isPassSet = graphQLUtilMethods.setPassword(parentLogin, SCHOOL_ID);
        Assert.assertTrue(isPassSet);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveUser")
    @Story(value = "SaveStudentViaAdminBusiness")
    @Test(description = "Создание пользователя с ролью ученик конфигуратором системы",
            priority=3)
    @Owner(value = "KovalchukIS")
    public void saveStudentViaAdminBusiness() throws IOException {

        final boolean isNotUser = (utilMethods.getSelectUserIdFromUsersByLogin(studentLogin) == null);

        if (isNotUser) {
            Allure.step("Будет создан пользователь " + studentLogin);
            final ObjectNode variables = objectMapper.createObjectNode();
            final ObjectNode user = variables.putObject("user");

            user
                    .put("firstName", studentFirstName)
                    .put("middleName", "")
                    .put("lastName", studentLastName)
                    .put("login", studentLogin)
                    .put("status", "ACTIVE");

            user.putArray("studentRoles")
                    .addObject()
                    .put("status", "ACTIVE")
                    .put("schoolId", SCHOOL_ID)
                    .put("isGraduate", false);
            user.putArray("parentRoles");
            final ArrayNode newAndUpdatedFunctionalRoles = user.putArray("newAndUpdatedFunctionalRoles");
            newAndUpdatedFunctionalRoles.addObject()
                    .put("code", "STUDENT")
                    .put("schoolId", SCHOOL_ID)
                    .putNull("dateEnd")
                    .putNull("dateStart");
            newAndUpdatedFunctionalRoles.addObject()
                    .put("code", "PEER2PEER")
                    .put("schoolId", SCHOOL_ID)
                    .putNull("dateEnd")
                    .putNull("dateStart");
            newAndUpdatedFunctionalRoles.addObject()
                    .put("code", "BASE_ROLE")
                    .put("schoolId", SCHOOL_ID)
                    .putNull("dateEnd")
                    .putNull("dateStart");

            user.putArray("deletedFunctionalRoles");

            final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                    .getGraphQLResponse(
                            businessLogin,
                            password,
                            "/graphql/saveUserViaAdminBusiness.graphql",
                            variables,
                            SCHOOL_ID
                    );

            final JSONObject response = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
            final JSONObject data = response.getJSONObject("data");
            final String loginFromResponse = data.getJSONObject("businessAdmin").getJSONObject("saveUser").getString("login");

            Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
            Assert.assertEquals(loginFromResponse, studentLogin);
        }

            Allure.step("Пользователь с логином " + studentLogin + " был создан или существовал. У пользователя будет установлен пароль по умолчанию");
            final boolean isPassSet = graphQLUtilMethods.setPassword(studentLogin, SCHOOL_ID);
            Assert.assertTrue(isPassSet);
    }

    @Epic(value = "NotificationsAutotests - GQL")
    @Feature(value = "SaveUser")
    @Story(value = "SaveStudentFromAnotherSchoolViaAdminSys")
    @Test(description = "Создание пользователя с ролью ученик другой школы с помощью системного администратора", priority = 5)
    @Owner(value = "Anastasiya Krutova")
    public void saveStudentFromAnotherSchoolViaAdminSys() throws IOException {

        final boolean isNotUser = (utilMethods.getSelectUserIdFromUsersByLogin(studentFromAnotherSchoolLogin) == null);

        if (isNotUser) {
            Allure.step("Будет создан пользователь " + studentFromAnotherSchoolLogin);

            final ObjectNode variables = objectMapper.createObjectNode();
            final ObjectNode user = variables.putObject("user");

            user
                    .put("firstName", studentFirstName)
                    .put("middleName", "")
                    .put("lastName", studentLastName)
                    .put("login", studentFromAnotherSchoolLogin)
                    .put("status", "ACTIVE");

            user
                    .putArray("studentRoles")
                    .addObject()
                    .put("status", "ACTIVE")
                    .put("schoolId", ANOTHER_SCHOOL_ID)
                    .put("isGraduate", false);
            user.putArray("parentRoles");

            final ArrayNode newAndUpdatedFunctionalRoles = user.putArray("newAndUpdatedFunctionalRoles");

            newAndUpdatedFunctionalRoles.addObject()
                                        .put("code", "STUDENT")
                                        .put("schoolId", ANOTHER_SCHOOL_ID)
                                        .putNull("dateEnd")
                                        .putNull("dateStart");
            newAndUpdatedFunctionalRoles.addObject()
                                        .put("code", "PEER2PEER")
                                        .put("schoolId", ANOTHER_SCHOOL_ID)
                                        .putNull("dateEnd")
                                        .putNull("dateStart");
            newAndUpdatedFunctionalRoles.addObject()
                                        .put("code", "BASE_ROLE")
                                        .put("schoolId", ANOTHER_SCHOOL_ID)
                                        .putNull("dateEnd")
                                        .putNull("dateStart");
            user.putArray("deletedFunctionalRoles");

            final CloseableHttpResponse responseGraphQL = graphQLUtilMethods
                    .getGraphQLResponse(
                            "admin-sys",
                            oldPassword,
                            "/graphql/saveUserViaAdminSys.graphql",
                            variables,
                            null
                    );

            final JSONObject response = new JSONObject(EntityUtils.toString(responseGraphQL.getEntity()));
            final JSONObject data = response.getJSONObject("data");
            final String loginFromResponse = data
                    .getJSONObject("systemAdmin")
                    .getJSONObject("saveUser")
                    .getString("login");

            Assert.assertEquals(responseGraphQL.getStatusLine().getStatusCode(), STATUS_OK);
            Assert.assertEquals(loginFromResponse, studentFromAnotherSchoolLogin);
        }

        Allure.step("Пользователь с логином " + studentFromAnotherSchoolLogin +
                    " был создан или существовал. У пользователя будет установлен пароль по умолчанию");
        final boolean isPassSet = graphQLUtilMethods.setPassword(studentFromAnotherSchoolLogin, ANOTHER_SCHOOL_ID);

        Assert.assertTrue(isPassSet);
    }
}