package ru.sbt.sber_learning.notification_autotests;

import io.qameta.allure.Allure;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Set;
import java.util.UUID;

import static io.restassured.RestAssured.given;

@Slf4j
@Configuration
public class UtilMethods {

    private final int ok = 200;

    @Value("${notifications.baseURI}")
    private String baseURI;

    @Value("${spring.datasource.url}")
    private String dbUrl;

    @Value("${spring.datasource.username}")
    private String user;

    @Value("${spring.datasource.password}")
    private String pass;

    @Value("${spring.datasource.keycloak.url}")
    private String dbUrlKeycloak;

    @Value("${spring.datasource.keycloak.username}")
    private String userKeycloak;

    @Value("${spring.datasource.keycloak.password}")
    private String passKeycloak;

    @Value("${spring.datasource.global.url}")
    private String dbUrlGlobal;

    @Value("${spring.datasource.global.username}")
    private String userGlobal;

    @Value("${spring.datasource.global.password}")
    private String passGlobal;

    @Value("${autotests-users.teacher.login}")
    private String teacherLogin;

    private static final String INSERT_INTO_NOTIFICATIONS =
            "INSERT INTO notifications " +
                    "(notification_type_id, related_object_id, related_object_type, title, message, was_read, " +
                    "event_time, user_to_id, user_from_id, extra_attributes, organization_id) " +
                    "VALUES(5, 3, 'TASK', 'notifTest', 'AUTOTESTS', '%s', '%s', '%s', '%s', '', '%s') ";

    private static final String INSERT_INTO_NOTIFICATIONS_CUSTOM =
            "INSERT INTO notifications " +
                    "(notification_type_id, related_object_id, related_object_type, title, message, was_read, " +
                    "event_time, user_to_id, user_from_id, extra_attributes, organization_id) " +
                    "VALUES('%s', '%s', '%s', 'notifTest', 'AUTOTESTS', '%s', '%s', '%s', '%s', '', '%s') ";

    private static final String INSERT_INTO_ORGANIZATION_CHANNELS =
            "INSERT INTO organization_channels " +
                    "(channel_id, organization_id, is_active) " +
                    "VALUES('%s', '%s', '%s')";

    private static final String INSERT_INTO_NOTIFICATION_TYPES =
            "INSERT INTO notification_types " +
            "(notification_type_id, locked, notification_type_description, notification_type_group_id, notification_type_name) " +
            "VALUES ('%s', '%s', '%s', '%s', '%s')";

    private static final String INSERT_INTO_SYSTEM_NOTIFICATIONS =
            "INSERT INTO system_notifications " +
                    "(school_id, notification_header, notification_text, " +
                    "notification_display_start, notification_display_end, author_id, closable) " +
                    "VALUES(%s, '%s', '%s', '%s', '%s', '%s', true)";

    private static final String SELECT_COUNT_FROM_SYSTEM_NOTIFICATIONS_BY_ID =
            "SELECT count(*) AS total FROM system_notifications " +
                    "WHERE system_notification_id = '%s'";

    private static final String SELECT_SYSTEM_NOTIFICATION_ID_FROM_SYSTEM_NOTIFICATIONS =
            "SELECT MAX(system_notification_id) AS system_notification_id FROM system_notifications " +
                    "WHERE author_id = '%s' AND notification_header = '%s'";

    private static final String SELECT_IS_ACTIVE_FROM_SYSTEM_CHANNELS_BY_CHANNEL_ID =
            "SELECT is_active FROM system_channels " +
                    "WHERE channel_id = '%s'";

    private static final String SELECT_IS_ACTIVE_FROM_ORGANIZATION_CHANNELS_BY_CHANNEL_ID =
            "SELECT is_active FROM organization_channels " +
                    "WHERE channel_id = '%s' AND organization_id = '%s'";

    private static final String SELECT_NOTIFICATION_ID_FROM_NOTIFICATIONS_BY_USER_AND_ORG_UNIT =
            "SELECT MAX(notification_id) as notification_id FROM notifications " +
                    "WHERE user_to_id = '%s' AND organization_id = '%s'";

    private static final String SELECT_COUNT_FROM_NOTIFICATIONS_BY_USER_AND_ORG_UNIT =
            "SELECT count(*) AS total FROM notifications " +
                    "WHERE user_to_id = '%s' AND organization_id = '%s' AND was_read = '%s'";

    private static final String SELECT_COUNT_FROM_NOTIFICATIONS_BY_HEADER_AND_TEXT =
            "SELECT count(*) AS total FROM system_notifications " +
                    "WHERE notification_header = '%s' AND notification_text = '%s'";

    private static final String SELECT_COUNT_FROM_NOTIFICATIONS_BY_USER_FROM =
            "SELECT count(*) AS total FROM notifications " +
                    "WHERE user_from_id = '%s'";

    private static final String SELECT_MAX_FROM_NOTIFICATION_TYPE =
            "SELECT MAX(notification_type_id) from notification_types;";

    private static final String SELECT_COUNT_FROM_NOTIFICATION_TYPE_CHANNELS =
            "SELECT COUNT(DISTINCT notification_type_id) As total " +
                    "FROM notification_type_channels";

    private static final String SELECT_COUNT_FROM_ROLE_NOTIFICATION_TYPES =
            "SELECT count (*) AS total FROM role_notification_types, notification_types, system_channels " +
                    "WHERE notification_types.notification_type_id = role_notification_types.notification_type_id " +
                    "AND (notification_types.locked = FALSE) AND (role_name = '%s' OR role_name = 'ALL')";

    private static final String SELECT_NOTIFICATION_READ_INTERVAL_FROM_CONFIGURATION =
            "SELECT value FROM configurations " +
                    "WHERE property_code = 'read.interval'";

    private static final String SELECT_WAS_READ_FROM_NOTIFICATIONS_BY_NOTIFICATION_ID =
            "SELECT was_read FROM notifications " +
                    "WHERE notification_id = '%s'";

    private static final String SELECT_USER_ID_FROM_USERS_BY_LOGIN =
            "select user_id from users where login = '%s';";

    private static final String DELETE_FROM_NOTIFICATIONS_BY_TITLE_AND_MESSAGE =
            "DELETE FROM notifications " +
                    "WHERE title = '%s' AND message = '%s'";

    private static final String DELETE_FROM_NOTIFICATIONS_BY_CREATION_TIME =
            "DELETE FROM notifications " +
                    "WHERE creation_time BETWEEN '%s' AND '%s'";

    private static final String DELETE_FROM_NOTIFICATIONS_USERTO =
            "DELETE FROM notifications " +
                    "WHERE user_to_id = '%s' AND organization_id = '%s' ";

    private static final String DELETE_FROM_NOTIFICATIONS_USERTO_IS_NULL =
            "DELETE FROM notifications " +
                    "WHERE user_to_id IS NULL AND organization_id = '%s' ";

    private static final String DELETE_FROM_NOTIFICATIONS_organization_id_IS_NULL =
            "DELETE FROM notifications " +
                    "WHERE user_to_id = '%s' AND organization_id IS NULL ";

    private static final String DELETE_FROM_USER_ACTIVE_CHANNELS =
            "DELETE FROM user_channels " +
                    "WHERE user_id = '%s';";

    private static final String DELETE_FROM_ORGANIZATION_CHANNELS =
            "DELETE FROM organization_channels " +
                    "WHERE channel_id = '%s' AND organization_id IN (%s)";

    private static final String DELETE_FROM_NOTIFICATION_TYPES =
            "DELETE FROM notification_types " +
                    "WHERE notification_type_id= '%s';";

    private static final String DELETE_FROM_GLOBAL_ACTIVE_CHANNELS =
            "DELETE FROM notification_type_channels " +
            "WHERE notification_type_id= '%s';";

    private static final String DELETE_FROM_USER_REQUIRED_ACTION =
            "delete from user_required_action " +
                    "where user_id = (select id from user_entity " +
                    "where username = '%s');";

    private static final String DELETE_FROM_SYSTEM_NOTIFICATIONS =
            "DELETE FROM system_notifications " +
                    "WHERE notification_header = '%s' AND notification_text = '%s'";

    private void findDriver() {
        //ищем драйвер
        try {
            Class.forName("org.postgresql.Driver");
        } catch (final ClassNotFoundException e) {
            log.error("PostgreSQL JDBC Driver is not found. Include it in your library path ", e);
        }
    }

    //для добавления (не)прочитанных нотификаций
    public Long fillDBNotificationsMass(final String userId, final String orgUnitId, final Boolean wasRead, final Boolean is_archive) {

        final String userFromId = getSelectUserIdFromUsersByLogin(teacherLogin);

        Allure.step("Создание одного wasRead = " + wasRead + " сообщения для userId = " + userId + " и orgUnitId = " + orgUnitId);

        findDriver();

        try (final Connection connection = DriverManager.getConnection(dbUrl, user, pass);
             final Statement statement = connection.createStatement()) {
            //добавляем непрочитанные уведомления
            statement.execute(String.format(INSERT_INTO_NOTIFICATIONS,
                    wasRead,
                    LocalDateTime.now(ZoneOffset.UTC),
                    userId,
                    userFromId,
                    orgUnitId
            ));

            final ResultSet rs = statement.executeQuery(String.format(
                    SELECT_NOTIFICATION_ID_FROM_NOTIFICATIONS_BY_USER_AND_ORG_UNIT, userId, orgUnitId));

            if (rs.next()) {
                return rs.getLong("notification_id");
            }
        } catch (final SQLException e) {
            log.error("Error: ", e);
        }

        return 0L;
    }

    //для добавления (не)прочитанных нотификаций с определенным userFromId
    public Long[] fillDataBaseWithUserFromId(
            final Integer count,
            final String userToId,
            final String userFromId,
            final String orgUnitId,
            final Boolean wasRead,
            final Boolean is_archive
    ) {
        Allure.step("Создание " + count + " сообщений wasRead = " + wasRead
                + " для userId = " + userToId + " и orgUnitId = " + orgUnitId);
        findDriver();

        try (
                final Connection connection = DriverManager.getConnection(dbUrl, user, pass);
                final Statement statement = connection.createStatement()
        ) {
            final Long[] results = new Long[count];

            for (int i = 0; i < count; i++) {
                statement.execute(String.format(INSERT_INTO_NOTIFICATIONS,
                        wasRead,
                        LocalDateTime.now(ZoneOffset.UTC),
                        userToId,
                        userFromId,
                        orgUnitId
                ));

                final ResultSet rs = statement.executeQuery(String.format(
                        SELECT_NOTIFICATION_ID_FROM_NOTIFICATIONS_BY_USER_AND_ORG_UNIT, userToId, orgUnitId));

                if (rs.next()) {
                    final Long selectResult = rs.getLong("notification_id");
                    results[i] = selectResult;
                }
            }

            return results;

        } catch (final SQLException e) {
            log.error("Error: ", e);
        }

        return null;
    }

    //для добавления большого количества (не)прочитанных нотификаций
    public Long[] fillDBNotificationsMass(final Integer count, final String userId, final String orgUnitId, final Boolean wasRead,
                                          final Boolean is_archive) {

        final String userFromId = getSelectUserIdFromUsersByLogin(teacherLogin);

        Allure.step("Создание " + count + " сообщений wasRead = " +
                wasRead + " для userId = " + userId + " и orgUnitId = " + orgUnitId);
        findDriver();

        try (final Connection connection = DriverManager.getConnection(dbUrl, user, pass);
             final Statement statement = connection.createStatement()) {
            final Long[] results = new Long[count];

            //добавляем непрочитанные уведомления
            for (int i = 0; i < count; i++) {
                statement.execute(String.format(INSERT_INTO_NOTIFICATIONS,
                        wasRead,
                        LocalDateTime.now(ZoneOffset.UTC),
                        userId,
                        userFromId,
                        orgUnitId
                ));

                final ResultSet rs = statement.executeQuery(String.format(
                        SELECT_NOTIFICATION_ID_FROM_NOTIFICATIONS_BY_USER_AND_ORG_UNIT, userId, orgUnitId));

                if (rs.next()) {
                    final Long selectResult = rs.getLong("notification_id");
                    results[i] = selectResult;
                }
            }

            return results;

        } catch (final SQLException e) {
            log.error("Error: ", e);
        }

        return null;
    }

    public Long[] fillDBNotificationsCustom(
            final Integer count,
            final Integer typeId,
            final Integer objectId,
            final String objectType,
            final Boolean wasRead,
            final String userId,
            final String userFromId,
            final String orgUnitId,
            final Boolean is_archive,
            final Boolean is_visible
    ) {
        Allure.step("Создание " + count + " сообщений wasRead = " + wasRead + " для userId = " + userId +
                " от userFromId = " + userFromId + " из orgUnitId = " + orgUnitId + ", objectType = " +
                objectType + ", objectId = " + objectId + ", typeId = " + typeId);
        findDriver();

        try (final Connection connection = DriverManager.getConnection(dbUrl, user, pass);
             final Statement statement = connection.createStatement()) {
            final Long[] results = new Long[count];

            //добавляем непрочитанные уведомления
            for (int i = 0; i < count; i++) {
                statement.execute(String.format(INSERT_INTO_NOTIFICATIONS_CUSTOM,
                        typeId,
                        objectId,
                        objectType,
                        wasRead,
                        LocalDateTime.now(ZoneOffset.UTC),
                        userId,
                        userFromId,
                        orgUnitId
                ));

                final ResultSet rs = statement.executeQuery(String.format(
                        SELECT_NOTIFICATION_ID_FROM_NOTIFICATIONS_BY_USER_AND_ORG_UNIT, userId, orgUnitId));

                if (rs.next()) {
                    final Long selectResult = rs.getLong("notification_id");
                    results[i] = selectResult;
                }
            }

            return results;

        } catch (final SQLException e) {
            log.error("Error: ", e);
        }

        return null;
    }

    public void fillDBNotificationTypeId(int notification_type_id, boolean is_active, boolean locked, String notification_type_description,
                                         int notification_type_group_id, String notification_type_name, String notification_type_scope) {

        Allure.step("Создание записи в таблице notification_types, где  notification_type_id=" + notification_type_id );

        findDriver();

        try (final Connection connection = DriverManager.getConnection(dbUrl, user, pass);
             final Statement statement = connection.createStatement()) {

            statement.execute(String.format(INSERT_INTO_NOTIFICATION_TYPES, notification_type_id, locked, notification_type_description, notification_type_group_id, notification_type_name));

        } catch (final SQLException e) {
            log.error("Error: ", e);
        }
    }

    public void cleanDBNotificationsByCreationTime(
            final LocalDateTime creationTimeBefore,
            final LocalDateTime creationTimeAfter
    ) {
        Allure.step("Удаление записей из таблицы notifications, где creation_time между "
                + String.valueOf(creationTimeBefore) + " и " + String.valueOf(creationTimeAfter));
        findDriver();
        try (final Connection connection = DriverManager.getConnection(dbUrl, user, pass);
             final Statement statement = connection.createStatement()) {

            statement.execute(String.format(
                    DELETE_FROM_NOTIFICATIONS_BY_CREATION_TIME,
                    creationTimeBefore,
                    creationTimeAfter
            ));

        } catch (final SQLException e) {
            log.error("Error: ", e);
        }
    }

    public Long fillTableSystemNotifications(
            final String schoolId,
            final String header,
            final String text,
            final String displayStart,
            final String displayEnd,
            final String authorId
    ) {
        Allure.step(
                "Создание записи в таблице system_notifications, где schoolId = " + schoolId + ", " +
                "header = " + header + ", text = " + text + ", displayStart = " +
                displayStart + ", displayEnd = " + displayEnd + ", authorId = " + authorId
        );

        findDriver();

        try (final Connection connection = DriverManager.getConnection(dbUrlGlobal, userGlobal, passGlobal);
             final Statement statement = connection.createStatement()
        ) {

                statement.execute(String.format(
                        INSERT_INTO_SYSTEM_NOTIFICATIONS,
                        schoolId == null ? null : "'" + schoolId + "'",
                        header,
                        text,
                        displayStart,
                        displayEnd,
                        authorId
                ));

            final ResultSet rs = statement.executeQuery(String.format(
                    SELECT_SYSTEM_NOTIFICATION_ID_FROM_SYSTEM_NOTIFICATIONS,
                    authorId,
                    header
            ));

            if (rs.next()) {
                return rs.getLong("system_notification_id");
            }

        } catch (final SQLException e) {
            log.error("Error: ", e);
        }
        return null;
    }

    public void cleanDBNotificationTypes(final int notification_type_id){

        Allure.step("Удаление записей из таблицы notification_type_id для notification_type_id = " + notification_type_id);
        findDriver();
        try (final Connection connection = DriverManager
                .getConnection(dbUrl, user, pass);
             final Statement statement = connection.createStatement()) {

            statement.execute(String.format(DELETE_FROM_NOTIFICATION_TYPES, notification_type_id));

        } catch (final SQLException e) {
            log.error("Error: ", e);
        }
    }

    public void cleanDBGlobalActiveChannels(final int notification_type_id){

        Allure.step("Удаление записей из таблицы notification_type_channels для notification_type_id = " + notification_type_id);
        findDriver();
        try (final Connection connection = DriverManager
                .getConnection(dbUrl, user, pass);
             final Statement statement = connection.createStatement()) {

            statement.execute(String.format(DELETE_FROM_GLOBAL_ACTIVE_CHANNELS, notification_type_id));

        } catch (final SQLException e) {
            log.error("Error: ", e);
        }
    }

    public Integer cleanTableOrganizationChannels(final Long channel_id, final Set <Object> orgUnitIds){
        Allure.step("Удаление записей из таблицы organization_channels для channel_id = " + channel_id
                + " и orgUnitIds = " + orgUnitIds.toString());

        findDriver();

        try (final Connection connection = DriverManager.getConnection(dbUrl, user, pass);
             final Statement statement = connection.createStatement()) {

            final StringBuilder builder = new StringBuilder();
            for (final Object orgUnitBuild : orgUnitIds) {
                builder.append("'").append(orgUnitBuild).append("',");
            }
            final String orgUnitIdsBuild = builder.deleteCharAt(builder.length() -1).toString();

            return statement.executeUpdate(String.format(
                    DELETE_FROM_ORGANIZATION_CHANNELS,
                    channel_id,
                    orgUnitIdsBuild
            ));

        } catch (final SQLException e) {
            log.error("Error: ", e);
        }
        return null;
    }

    public void cleanTableSystemNotificationsByHeaderAndText(final String notificationHeader, final String notificationText) {
        Allure.step("Удаление записей из таблицы system_notifications для notification_header = " + notificationHeader +
                    " и notification_text = " + notificationText);

        findDriver();

        try (final Connection connection = DriverManager.getConnection(dbUrlGlobal, userGlobal, passGlobal);
             final Statement statement = connection.createStatement()
        ) {

            statement.executeUpdate(String.format(
                    DELETE_FROM_SYSTEM_NOTIFICATIONS,
                    notificationHeader,
                    notificationText
            ));

        } catch (final SQLException e) {
            log.error("Error: ", e);
        }
    }

    public void cleanTableNotificationsByTitleAndMessage(final String title, final String message) {
        Allure.step("Удаление записей из таблицы notifications для title = " + title + " и message = " + message);

        findDriver();

        try (final Connection connection = DriverManager.getConnection(dbUrl, user, pass);
             final Statement statement = connection.createStatement()
        ) {

            statement.executeUpdate(String.format(
                    DELETE_FROM_NOTIFICATIONS_BY_TITLE_AND_MESSAGE,
                    title,
                    message
            ));

        } catch (final SQLException e) {
            log.error("Error: ", e);
        }
    }

    public void cleanDBNotifications(final String userId, final String orgUnitId) {

        Allure.step("Удаление сообщений для userId = " + userId + " и orgUnitId = " + orgUnitId);

        findDriver();

        try (final Connection connection = DriverManager.getConnection(dbUrl, user, pass);
             final Statement statement = connection.createStatement()) {
            final String delquery;

            if ("".equals(userId) && !"".equals(orgUnitId)) {
                delquery = String.format(DELETE_FROM_NOTIFICATIONS_USERTO_IS_NULL, orgUnitId);
            } else if (!"".equals(userId) && "".equals(orgUnitId)) {
                delquery = String.format(DELETE_FROM_NOTIFICATIONS_organization_id_IS_NULL, userId);
            } else {
                delquery = String.format(DELETE_FROM_NOTIFICATIONS_USERTO, userId, orgUnitId);
            }
            statement.execute(delquery);
        } catch (final SQLException e) {
            log.error("Error: ", e);
        }
    }

    public void cleanDBUserActiveChannels(final String user_id) {

        Allure.step("Удаление записей из таблицы user_active_channels для channel_name = " + user_id);
        findDriver();
        try (final Connection connection = DriverManager.getConnection(dbUrl, user, pass);
             final Statement statement = connection.createStatement()) {

            statement.execute(String.format(DELETE_FROM_USER_ACTIVE_CHANNELS, user_id));

        } catch (final SQLException e) {
            log.error("Error: ", e);
        }
    }

    public Integer selectCountSystemNotificationsById(final Long system_notification_id) {

        Allure.step("Подсчет количества записей в таблице system_notifications, " +
                    "где system_notification_id = " + system_notification_id);

        findDriver();

        try (final Connection connection = DriverManager.getConnection(dbUrlGlobal, userGlobal, passGlobal);
             final Statement statement = connection.createStatement()
        ) {

            final ResultSet rs = statement.executeQuery(String.format(
                    SELECT_COUNT_FROM_SYSTEM_NOTIFICATIONS_BY_ID,
                    system_notification_id
            ));

            if (rs.next()) {
                return rs.getInt("total");
            }

        } catch (final SQLException e) {
            log.error("Error: ", e);
        }
        return null;
    }

    public String selectNoTificationsReadIntervalFromDB() {

        Allure.step("Получение интервала из GLOBAL БД");

        findDriver();

        try (final Connection connection = DriverManager.getConnection(dbUrlGlobal, userGlobal, passGlobal);
             final Statement statement = connection.createStatement()) {
            final ResultSet rs = statement.executeQuery(SELECT_NOTIFICATION_READ_INTERVAL_FROM_CONFIGURATION);

            if (rs.next()) {
                return rs.getString("value");
            }
        } catch (final SQLException e) {
            log.error("Error: ", e);
        }

        return null;
    }

    public Integer selectMaxNoTificationTypeIDFromDB() {

        Allure.step("Получение количества записей в таблице notification_type_id ");

        findDriver();

        try (final Connection connection = DriverManager
                .getConnection(dbUrl, user, pass);
             final Statement statement = connection.createStatement()) {
            final ResultSet rs = statement.executeQuery(SELECT_MAX_FROM_NOTIFICATION_TYPE);

            if (rs.next()) {
                return rs.getInt("max");
            }
        } catch (final SQLException e) {
            log.error("Error: ", e);
        }

        return null;
    }

    //считаем количество сообщений для пользователя и школы, прочитанные или непрочитанные
    public Integer selectCountNotifications(final String userId, final String orgUnitId, final Boolean wasRead) {
        Allure.step("Подсчет количества wasRead = " + wasRead + " сообщений для userId = " + userId + " и orgUnitId = " + orgUnitId);
        findDriver();

        int countResult = 0;
        try (final Connection connection = DriverManager.getConnection(dbUrl, user, pass);
             final Statement statement = connection.createStatement()) {
            final ResultSet rs = statement.executeQuery(String.format(
                    SELECT_COUNT_FROM_NOTIFICATIONS_BY_USER_AND_ORG_UNIT,
                    userId,
                    orgUnitId,
                    wasRead
            ));

            if (rs.next()) {
                countResult = rs.getInt("total");
            }
        } catch (final SQLException e) {
            log.error("Error: ", e);
        }

        return countResult;
    }

    // подсчет кол-ва сообщений от UserFromId в базе Notifications
    public Integer selectCountNotificationsFromUser(final UUID userId) {
        Allure.step("Подсчет количества сообщений для userId = " + userId);
        findDriver();
        int countResult = 0;

        try (final Connection connection = DriverManager.getConnection(dbUrl, user, pass);
             final Statement statement = connection.createStatement()) {
            final ResultSet rs = statement.executeQuery(String.format(
                    SELECT_COUNT_FROM_NOTIFICATIONS_BY_USER_FROM,
                    userId
            ));
            if (rs.next()) {
                countResult = rs.getInt("total");
            }
        } catch (final SQLException e) {
            log.error("Error: ", e);
        }

        return countResult;
    }

    public int selectCountSystemNotificationsByHeaderAndText(final String notificationHeader, final String notificationText) {
        Allure.step("Подсчет количества нотификаций, где title = " + notificationHeader + " и message = " + notificationText);
        findDriver();

        int countResult = 0;
        try (final Connection connection = DriverManager.getConnection(dbUrlGlobal, userGlobal, passGlobal);
             final Statement statement = connection.createStatement()
        ) {
            final ResultSet rs = statement.executeQuery(String.format(
                    SELECT_COUNT_FROM_NOTIFICATIONS_BY_HEADER_AND_TEXT,
                    notificationHeader,
                    notificationText
            ));

            if (rs.next()) {
                countResult = rs.getInt("total");
            }
        } catch (final SQLException e) {
            log.error("Error: ", e);
        }

        return countResult;
    }

    public int selectCountNotificationTypeChannels() {
        Allure.step("Подсчет количества уникальных типов нотификаций в разрезе каналов");
        findDriver();

        int countResult = 0;
        try (
                final Connection connection = DriverManager.getConnection(dbUrl, user, pass);
                final Statement statement = connection.createStatement()
        ) {
            final ResultSet rs = statement.executeQuery(SELECT_COUNT_FROM_NOTIFICATION_TYPE_CHANNELS);
            if (rs.next()) {
                countResult = rs.getInt("total");
            }
        } catch (final SQLException e) {
            log.error("Error: ", e);
        }
        return countResult;
    }

    public Integer selectCountNotificationTypesByRole(final String role) {
        Allure.step("Подсчет количества типов нотификаций для роли = " + role);
        findDriver();
        int countResult = 0;

        try (
                final Connection connection = DriverManager.getConnection(dbUrl, user, pass);
                final Statement statement = connection.createStatement()
        ) {
            final ResultSet rs = statement.executeQuery(String.format(SELECT_COUNT_FROM_ROLE_NOTIFICATION_TYPES, role));
            if (rs.next()) {
                countResult = rs.getInt("total");
            }
        } catch (final SQLException e) {
            log.error("Error: ", e);
        }
        return countResult;
    }

    //считаем количество сообщений для пользователя и школы, прочитанные или непрочитанные
    public Boolean isNotificationRead(final Long notificationId) {
        Allure.step("Проверка, является ли сообщение с notificationId = " + notificationId + " прочитанным.");
        findDriver();

        try (final Connection connection = DriverManager.getConnection(dbUrl, user, pass);
             final Statement statement = connection.createStatement()) {
            final ResultSet rs = statement.executeQuery(String.format(SELECT_WAS_READ_FROM_NOTIFICATIONS_BY_NOTIFICATION_ID,
                    notificationId
            ));
            if (rs.next()) {
                return rs.getBoolean("was_read");
            }
        } catch (final SQLException e) {
            log.error("Error: ", e);
        }

        return null;
    }

    public String getSelectUserIdFromUsersByLogin (String login) {
        findDriver();

        try (final Connection connection = DriverManager.getConnection(dbUrlGlobal, userGlobal, passGlobal);
        final Statement statement = connection.createStatement()) {
            final ResultSet rs = statement.executeQuery(String.format(SELECT_USER_ID_FROM_USERS_BY_LOGIN, login));
            if (rs.next()) {
                return rs.getString("user_id");
            }
        } catch (final SQLException e) {
            log.error("Error: ", e);
        }
        return null;
    }

    public Boolean isChannelActive(final Long channelId) {
        Allure.step("Проверка, является ли канал с channelId = " + channelId + " активным.");
        findDriver();

        try (final Connection connection = DriverManager.getConnection(dbUrl, user, pass);
             final Statement statement = connection.createStatement()) {

            final ResultSet rs = statement.executeQuery(String.format(SELECT_IS_ACTIVE_FROM_SYSTEM_CHANNELS_BY_CHANNEL_ID, channelId));
            if (rs.next()) {
                return rs.getBoolean("is_active");
            }
        } catch (final SQLException e) {
            log.error("Error: ", e);
        }

        return null;
    }

    public Boolean isChannelActiveInDBOrgUnitActiveChannel(final Long channelId, final String orgUnitId) {
        Allure.step("Проверка, является ли канал с channelId = " + channelId
                + " активным для orgUnitId = " + orgUnitId);
        findDriver();

        try (final Connection connection = DriverManager.getConnection(dbUrl, user, pass);
             final Statement statement = connection.createStatement()) {

            final ResultSet rs = statement.executeQuery(String.format(
                    SELECT_IS_ACTIVE_FROM_ORGANIZATION_CHANNELS_BY_CHANNEL_ID,
                    channelId,
                    orgUnitId
            ));
            if (rs.next()) {
                return rs.getBoolean("is_active");
            }
        } catch (final SQLException e) {
            log.error("Error: ", e);
        }
        return null;
    }

    public Long deleteSecurityUserIdFromKCDB(String login) {

        Allure.step("Удаление записи из таблицы user_required_action БД Keycloak для снятия ограничения на смену сброшенного пароля");
        findDriver();
        try (final Connection connection = DriverManager
                .getConnection(dbUrlKeycloak, userKeycloak, passKeycloak);
             final Statement statement = connection.createStatement()) {

            Long rowDeleted = Long.valueOf(statement.executeUpdate(String.format(DELETE_FROM_USER_REQUIRED_ACTION, login)));

            return rowDeleted;

        } catch (final SQLException e) {
            log.error("Не удалось удалить запись с логином: " + login, e);
        }

        return 0L;
    }

    public void fillTableOrganizationChannels(final Long channelId, final String orgUnitId, final Boolean is_active) {
        Allure.step("Создание записи в таблице organization_channels для channel_id = " + channelId +
                "organization_id = " + orgUnitId + "is_active = " + is_active);
        findDriver();

        try (final Connection connection = DriverManager.getConnection(dbUrl, user, pass);
             final Statement statement = connection.createStatement()) {

            statement.execute(String.format(INSERT_INTO_ORGANIZATION_CHANNELS, channelId, orgUnitId, is_active));

        } catch (final SQLException e) {
            log.error("Error: ", e);
        }
    }
}